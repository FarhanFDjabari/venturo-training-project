class ApiConstants {
  ApiConstants._();

  // base url
  static const String baseUrl = "https://trainee.landa.id/javacode";

  // auth
  static const String login = "$baseUrl/auth/login";
  static const String session = "$baseUrl/auth/session";
  static const String logout = "$baseUrl/auth/logout";

  // user
  static const String userDetail = "$baseUrl/user/detail";
  static const String userUpdate = "$baseUrl/user/update";
  static const String updateProfilUser = "$baseUrl/user/profil";
  static const String updateKtpUser = "$baseUrl/user/ktp";

  // menu
  static const String menu = "$baseUrl/menu/all";
  static const String menuDetail = "$baseUrl/menu/detail";
  static const String menuByCategory = "$baseUrl/menu/kategori";

  // diskon
  static const String diskon = "$baseUrl/diskon/all";
  static const String diskonOfUser = "$baseUrl/diskon/user";
  static const String diskonDetail = "$baseUrl/diskon/detail";

  // order
  static const String orderAdd = "$baseUrl/order/add";
  static const String orderList = "$baseUrl/order/all";
  static const String orderListOfUser = "$baseUrl/order/user";
  static const String orderDetail = "$baseUrl/order/detail";
  static const String orderUserByStatus = "$baseUrl/order/status";
  static const String orderHistoryOfUser = "$baseUrl/order/history";
  static const String orderCancel = "$baseUrl/order/batal";

  // promo
  static const String promo = "$baseUrl/promo/all";
  static const String promoType = "$baseUrl/promo/type";
  static const String promoOfUser = "$baseUrl/promo/user";
  static const String promoDetail = "$baseUrl/promo/detail";

  // review
  static const String review = "$baseUrl/review";
  static const String reviewByUser = "$baseUrl/review";
  static const String reviewAdd = "$baseUrl/review/add";
  static const String reviewDetail = "$baseUrl/review/detail";
  static const String reviewAnswerAdd = "$baseUrl/review/answer/add";

  // voucher
  static const String voucher = "$baseUrl/voucher/all";
  static const String voucherOfUser = "$baseUrl/voucher/user";
  static const String voucherDetail = "$baseUrl/voucher/detail";

  // fcm
  static const String firebaseCloudMessaging =
      'https://fcm.googleapis.com/fcm/send';
}
