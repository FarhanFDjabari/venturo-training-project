import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:java_code_onboarding/config/themes/colors.dart';
import 'package:java_code_onboarding/shared/widgets/outlined_title_button.dart';
import 'package:java_code_onboarding/shared/widgets/primary_button_with_title.dart';

class ConfirmationDialog extends StatelessWidget {
  const ConfirmationDialog({Key? key, this.title}) : super(key: key);

  final String? title;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16.w),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          // Icon
          Row(
            children: [
              Icon(Icons.warning, color: AppColor.kBlueColor, size: 72.r),
              16.horizontalSpace,
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text(
                      'Warning'.tr,
                      style: Get.textTheme.titleMedium?.copyWith(
                        color: AppColor.kRedColor,
                      ),
                    ),
                    4.verticalSpacingRadius,
                    Text(
                      title ?? 'Are you sure?'.tr,
                      style: Get.textTheme.bodyMedium,
                    ),
                  ],
                ),
              ),
            ],
          ),
          14.verticalSpace,

          // Actions
          Row(
            children: [
              Expanded(
                child: OutlinedTitleButton(
                  onPressed: () => Get.back(result: false),
                  text: 'No'.tr,
                ),
              ),
              16.horizontalSpace,
              Expanded(
                child: PrimaryButtonWithTitle(
                  onPressed: () => Get.back(result: true),
                  title: 'Yes'.tr,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
