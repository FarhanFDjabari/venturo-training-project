import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:app_settings/app_settings.dart';
import 'package:java_code_onboarding/config/themes/colors.dart';

import '../../constant/core/asset_const.dart';

class NoConnectionView extends StatelessWidget {
  const NoConnectionView({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Text('Error'.tr, style: Get.textTheme.headlineSmall),
        15.verticalSpacingRadius,
        SvgPicture.asset(
          AssetConstants.iconNoInternet,
          width: 0.6.sw,
        ),
        5.verticalSpacingRadius,
        Text(
          'Not connected to internet'.tr,
          style: Get.textTheme.titleSmall,
          textAlign: TextAlign.center,
        ),
        15.verticalSpacingRadius,
        ElevatedButton(
          onPressed: () => AppSettings.openDeviceSettings(),
          style: ElevatedButton.styleFrom(
            backgroundColor: AppColor.kLightColor,
            elevation: 2,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(50.r),
            ),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              const Icon(Icons.settings, color: AppColor.kDarkColor),
              16.horizontalSpaceRadius,
              Text(
                'Open settings'.tr,
                style: Get.textTheme.bodySmall,
              ),
            ],
          ),
        ),
      ],
    );
  }
}
