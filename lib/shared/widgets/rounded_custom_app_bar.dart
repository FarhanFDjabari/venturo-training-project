import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:java_code_onboarding/config/themes/colors.dart';

class RoundedAppBar extends StatelessWidget implements PreferredSizeWidget {
  const RoundedAppBar({
    super.key,
    required this.title,
    this.iconAssetUrl,
    this.onBackButtonPressed,
    this.actions,
    this.titleWidget,
    this.enableBackButton = true,
    this.titleStyle,
  });

  final String title;
  final String? iconAssetUrl;
  final VoidCallback? onBackButtonPressed;
  final List<Widget>? actions;
  final Widget? titleWidget;
  final bool? enableBackButton;
  final TextStyle? titleStyle;

  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: Wrap(
        crossAxisAlignment: WrapCrossAlignment.center,
        children: [
          if (iconAssetUrl != null)
            SvgPicture.asset(
              iconAssetUrl!,
              width: 20.r,
              height: 20.r,
              colorFilter: const ColorFilter.mode(
                AppColor.kBlueColor,
                BlendMode.srcIn,
              ),
            ),
          if (iconAssetUrl != null) 10.horizontalSpaceRadius,
          Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(title, style: titleStyle ?? Get.textTheme.titleMedium),
              if (titleWidget != null) 10.verticalSpace,
              if (titleWidget != null) titleWidget!,
            ],
          ),
        ],
      ),
      backgroundColor: AppColor.kLightColor,
      elevation: 2,
      centerTitle: true,
      leading: enableBackButton == true
          ? IconButton(
              splashRadius: 30.r,
              icon: Icon(Icons.chevron_left, color: Colors.black, size: 36.r),
              onPressed:
                  onBackButtonPressed ?? () => Get.back(closeOverlays: true),
            )
          : null,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          bottom: Radius.circular(30.r),
        ),
      ),
      actions: actions,
    );
  }

  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);
}
