import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:java_code_onboarding/config/themes/colors.dart';

class HorizontalOrDivider extends StatelessWidget {
  const HorizontalOrDivider({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        const Expanded(child: Divider(color: AppColor.kDarkColor5)),
        15.horizontalSpaceRadius,
        Text(
          'or'.tr,
          style: Get.textTheme.bodySmall!.copyWith(
            color: AppColor.kDarkColor4,
          ),
        ),
        15.horizontalSpaceRadius,
        const Expanded(child: Divider(color: AppColor.kDarkColor5)),
      ],
    );
  }
}
