import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:java_code_onboarding/config/themes/colors.dart';

class PrimaryButtonWithTitleIcon extends StatelessWidget {
  final VoidCallback? onPressed;
  final Color? backgroundColor;
  final Color? titleColor;
  final Widget iconWidget;
  final String title;
  final String? additionalTitle;
  final bool isLoading;

  const PrimaryButtonWithTitleIcon({
    super.key,
    this.onPressed,
    this.backgroundColor,
    required this.iconWidget,
    required this.title,
    this.additionalTitle,
    this.titleColor,
    this.isLoading = false,
  });

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: onPressed,
      style: ElevatedButton.styleFrom(
        backgroundColor: backgroundColor ?? AppColor.kLightColor,
        minimumSize: Size(1.sw, 56.h),
        maximumSize: Size(1.sw, 56.h),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(50.r),
        ),
        elevation: 2,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          iconWidget,
          15.horizontalSpace,
          Text(
            title,
            textAlign: TextAlign.center,
            style: Get.textTheme.bodySmall?.copyWith(
              color: titleColor ?? AppColor.kDarkColor,
            ),
          ),
          Text(
            additionalTitle ?? '',
            style: Get.textTheme.bodySmall?.copyWith(
              fontWeight: FontWeight.w700,
              color: titleColor ?? AppColor.kDarkColor,
            ),
          ),
        ],
      ),
    );
  }
}
