import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:java_code_onboarding/config/themes/colors.dart';

class SectionHeader extends StatelessWidget {
  const SectionHeader({
    super.key,
    this.color,
    required this.iconAssetUrl,
    required this.title,
  });

  final String title;
  final String iconAssetUrl;
  final Color? color;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 25.w),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          SvgPicture.asset(
            iconAssetUrl,
            width: 20.w,
            colorFilter:
                ColorFilter.mode(color ?? AppColor.kBlueColor, BlendMode.srcIn),
          ),
          10.horizontalSpace,
          Text(title,
              style: Get.textTheme.titleMedium?.copyWith(
                color: color,
              )),
        ],
      ),
    );
  }
}
