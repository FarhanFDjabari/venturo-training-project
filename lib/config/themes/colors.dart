import 'package:flutter/material.dart';

class AppColor {
  AppColor._();

  static const Color kBlueColor = Color(0xFF009AAD);
  static const Color kBlueColor2 = Color.fromARGB(230, 0, 153, 173);
  static const Color kBlueColor3 = Color.fromARGB(64, 0, 154, 173);
  static const Color kDarkBlueColor = Color(0xFF00717F);
  static const Color kRedColor = Color(0xFFD81D1D);
  static const Color kLightColor = Color(0xFFFFFFFF);
  static const Color kLightColor2 = Color(0xF0F0F0F0);
  static const Color kLightColor3 = Color.fromARGB(255, 223, 223, 223);
  static const Color kLightColor4 = Color.fromARGB(255, 175, 175, 175);
  static const Color kLightColor5 = Color(0xFFF6F6F6);
  static const Color kLightColor6 = Color.fromARGB(125, 223, 223, 223);

  static const Color kDarkColor = Color(0xFF1E1E1E);
  static const Color kDarkColor2 = Color(0xFF2E2E2E);
  static const Color kDarkColor3 = Color(0xFF8F8F8F);
  static const Color kDarkColor4 = Color.fromARGB(64, 30, 30, 30);
  static const Color kDarkColor5 = Color.fromARGB(64, 0, 0, 0);
  static const Color kDarkColor6 = Color.fromARGB(125, 0, 0, 0);
  static const Color kDarkColor7 = Color(0xFFC2C2C2);
  static const Color kDarkColor8 = Color.fromARGB(64, 46, 46, 46);

  static const Color kYellowColor = Color(0xFFFFAC01);
  static const Color kOrangeColor = Color(0xFFFFC700);
  static const Color kDarkOrangeColor = Color(0xFFFF9900);

  static const Color kGreenColor = Color(0xFF009C48);

  static const Color kTransparent = Color(0x00FFFFFF);

  static const LinearGradient orangeGradient = LinearGradient(
    begin: Alignment.topLeft,
    end: Alignment.bottomRight,
    colors: [
      kOrangeColor,
      kDarkOrangeColor,
    ],
  );
}
