import 'package:get/get.dart';
import 'package:java_code_onboarding/config/routes/routes.dart';
import 'package:java_code_onboarding/modules/features/cart/view/ui/cart_view.dart';
import 'package:java_code_onboarding/modules/features/cart/view/ui/voucher_detail_view.dart';
import 'package:java_code_onboarding/modules/features/cart/view/ui/voucher_view.dart';
import 'package:java_code_onboarding/modules/features/dashboard/controllers/dashboard_binding.dart';
import 'package:java_code_onboarding/modules/features/dashboard/view/ui/dashboard_view.dart';
import 'package:java_code_onboarding/modules/features/login/controllers/login_binding.dart';
import 'package:java_code_onboarding/modules/features/login/view/ui/login_view.dart';
import 'package:java_code_onboarding/modules/features/menu/controllers/menu_binding.dart';
import 'package:java_code_onboarding/modules/features/menu/view/ui/menu_view.dart';
import 'package:java_code_onboarding/modules/features/order/controllers/detail_order_binding.dart';
import 'package:java_code_onboarding/modules/features/order/view/ui/detail_order_view.dart';
import 'package:java_code_onboarding/modules/features/promo/controllers/promo_binding.dart';
import 'package:java_code_onboarding/modules/features/promo/view/ui/promo_view.dart';
import 'package:java_code_onboarding/modules/features/review/controllers/add_review_binding.dart';
import 'package:java_code_onboarding/modules/features/review/controllers/reply_review_binding.dart';
import 'package:java_code_onboarding/modules/features/review/controllers/review_binding.dart';
import 'package:java_code_onboarding/modules/features/review/view/ui/add_review_view.dart';
import 'package:java_code_onboarding/modules/features/review/view/ui/reply_review_view.dart';
import 'package:java_code_onboarding/modules/features/review/view/ui/review_view.dart';
import 'package:java_code_onboarding/modules/features/splash/controllers/splash_binding.dart';
import 'package:java_code_onboarding/modules/features/splash/view/ui/splash_view.dart';

class AppPages {
  AppPages._();

  static final pages = <GetPage>[
    // authentication
    GetPage(
      name: AppRoutes.splashView,
      page: () => const SplashView(),
      binding: SplashBinding(),
    ),
    GetPage(
      name: AppRoutes.loginView,
      page: () => LoginView(),
      binding: LoginBinding(),
    ),

    // dashboard
    GetPage(
      name: AppRoutes.dashboardView,
      page: () => const DashboardView(),
      binding: DashboardBinding(),
    ),

    // promo
    GetPage(
      name: AppRoutes.promoDetailView,
      page: () => const PromoView(),
      binding: PromoBinding(),
    ),

    // menu
    GetPage(
      name: AppRoutes.menuDetailView,
      page: () => const MenuView(),
      binding: MenuBinding(),
    ),

    // cart
    GetPage(
      name: AppRoutes.cartView,
      page: () => const CartView(),
    ),
    GetPage(
      name: AppRoutes.voucherView,
      page: () => const VoucherView(),
    ),
    GetPage(
      name: AppRoutes.voucherDetailView,
      page: () => const VoucherDetailView(),
    ),

    // order
    GetPage(
      name: AppRoutes.detailOrderView,
      page: () => const DetailOrderView(),
      binding: DetailOrderBinding(),
    ),

    // review
    GetPage(
      name: AppRoutes.reviewView,
      page: () => const ReviewView(),
      binding: ReviewBinding(),
    ),
    GetPage(
      name: AppRoutes.addReviewView,
      page: () => const AddReviewView(),
      binding: AddReviewBinding(),
    ),
    GetPage(
      name: AppRoutes.replyReviewView,
      page: () => const ReplyReviewView(),
      binding: ReplyReviewBinding(),
    ),
  ];
}
