class AppRoutes {
  AppRoutes._();

  // authentication
  static const splashView = '/';
  static const loginView = '/login';

  // home
  static const dashboardView = '/dashboard';

  // promo
  static const promoView = '/promo';
  static const promoDetailView = '/promo/:promoId';

  // menu
  static const menuView = '/menu';
  static const menuDetailView = '/menu/:menuId';

  // cart
  static const cartView = '/cart';
  static const voucherView = '/voucher';
  static const voucherDetailView = '/voucher/:voucherId';

  // order
  static const orderView = '/order';
  static const detailOrderView = '/order/:orderId';

  // review
  static const reviewView = '/review';
  static const replyReviewView = '/review/reply/:reviewId';
  static const addReviewView = '/review/add';
}
