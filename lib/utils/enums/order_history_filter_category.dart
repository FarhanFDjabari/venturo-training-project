// ignore_for_file: constant_identifier_names

enum OrderHistoryFilterCategory {
  All('all'),
  Canceled('canceled'),
  Completed('completed');

  const OrderHistoryFilterCategory(this.name);
  final String name;
}
