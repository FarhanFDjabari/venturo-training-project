// string extension
extension StringExt on String {
  /*
    Format string to capital case
    ex: 'hello world' => 'Hello world'
   */
  String get toCapitalCase =>
      length > 0 ? '${this[0].toUpperCase()}${substring(1).toLowerCase()}' : '';

  /*
    Format string to title case
    ex: 'hello world' => 'Hello World'
  */
  String get toTitleCase => replaceAll(RegExp(' +'), ' ')
      .split(' ')
      .map((e) => e.toCapitalCase)
      .join(' ');

  /*
    Format string to obscured string
    ex: 'hello world' => '***** *****'
   */
  String get toObscured => '*' * length;
}
