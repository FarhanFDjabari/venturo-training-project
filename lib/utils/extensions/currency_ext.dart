import 'package:intl/intl.dart';

// currency format extension
extension CurrencyExt on num {
  // static object for currency format
  static final NumberFormat _currencyFormat =
      NumberFormat.currency(locale: 'id_ID', symbol: 'Rp. ', decimalDigits: 0);

  /* 
    Format int number to K
    ex: 1000 => 1K
  */
  String get toShortK => '${(this / 1000).round()}K';

  /* 
    Format int number to Rp
    ex: 1000000 => Rp. 1.000.000
  */
  String get toRupiah => _currencyFormat.format(this);
}
