import 'package:intl/intl.dart';

// date and time format extension
extension DateExt on DateTime {
  // static object for date and time format
  static final DateFormat _dateTimeFormat = DateFormat('yyyy-MM-dd HH:mm:ss');
  static final DateFormat _dateFormat = DateFormat('yyy-MM-dd');

  // format date to date and time formatted string
  String get toDateTimeString => _dateTimeFormat.format(this);

  // format date to date formatted string
  String get toDateString => _dateFormat.format(this);
}
