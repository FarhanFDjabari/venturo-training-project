// iterable extension
extension IndexedIterable<E> on Iterable<E> {
  // map with index
  Iterable<T> mapIndexed<T>(T Function(E e, int i) f) {
    var i = 0;
    return map((e) => f(e, i++));
  }
}
