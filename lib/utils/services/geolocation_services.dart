import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:java_code_onboarding/config/localization/localization.dart';
import 'package:java_code_onboarding/constant/common/constants.dart';

class GeoLocationServices {
  GeoLocationServices._();

  static Stream<ServiceStatus> serviceStatusStream =
      Geolocator.getServiceStatusStream();

  static Future<LocationResult> getCurrentPosition() async {
    // check if location service is enabled
    bool isServiceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!isServiceEnabled) {
      return LocationResult.error(message: 'Location service not enabled'.tr);
    }

    // check if location permission is granted
    LocationPermission permission = await Geolocator.checkPermission();
    // if permission is denied, ask for permission
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
    }

    if (permission == LocationPermission.deniedForever) {
      return LocationResult.error(
          message: 'Location permission permanently denied'.tr);
    }

    // if permission is granted, get current position
    late Position position;
    try {
      position = await Geolocator.getCurrentPosition();
    } catch (error, stack) {
      FirebaseCrashlytics.instance.recordError(error, stack);
      return LocationResult.error(message: 'Failed to get location'.tr);
    }

    // get address from position
    double distanceInMeters = Geolocator.distanceBetween(
      position.latitude,
      position.longitude,
      AppConstants.locationLatitude,
      AppConstants.locationLongitude,
    );

    // check if user is within max distance from the office
    if (distanceInMeters > AppConstants.maximumDistance) {
      return LocationResult.error(message: 'Distance not close'.tr);
    }

    // obtain location info
    List<Placemark> placemarks = await placemarkFromCoordinates(
      position.latitude,
      position.longitude,
      localeIdentifier: Localization.currentLocale.toString(),
    );

    // check if location info is not available then return error
    if (placemarks.isEmpty) {
      return LocationResult.error(message: 'Unknown location'.tr);
    }

    // if location info is available then return success
    return LocationResult.success(
      position: position,
      address: [
        placemarks.first.name,
        placemarks.first.subLocality,
        placemarks.first.locality,
        placemarks.first.administrativeArea,
        placemarks.first.postalCode,
        placemarks.first.country,
      ].where((element) => element != null).join(', '),
    );
  }
}

class LocationResult {
  final bool success;
  final Position? position;
  final String? address;
  final String? message;

  LocationResult({
    required this.success,
    this.position,
    this.address,
    this.message,
  });

  factory LocationResult.success(
      {required Position position, required String address}) {
    return LocationResult(
      success: true,
      position: position,
      address: address,
    );
  }

  factory LocationResult.error({required String message}) {
    return LocationResult(
      success: false,
      message: message,
    );
  }
}
