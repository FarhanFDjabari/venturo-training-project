import 'dart:convert';

import 'package:java_code_onboarding/modules/models/user.dart';
import 'package:shared_preferences/shared_preferences.dart';

// shared preferences local db service
class LocalDBServices {
  LocalDBServices._();

  // save current user data to local db
  static Future<void> setUser(User user) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    await prefs.setString('user', json.encode(user.toMap()));
  }

  // save current fcm token to local db
  static Future<void> setFCMToken(String fcmToken) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    await prefs.setString('fcm_token', fcmToken);
  }

  // get current user data from local db
  static Future<User?> getCurrentUser() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    String? user = prefs.getString('user');

    if (user != null) {
      return User.fromJson(json.decode(user));
    }

    return null;
  }

  // get current fcm token from local db
  static Future<String?> getFCMToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    String? fcmToken = prefs.getString('fcm_token');

    return fcmToken;
  }

  // remove current user data from local db
  static Future<void> removeCurrentUser() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    await prefs.remove('user');
  }

  // save user token to local db
  static Future<void> setUserToken(String token) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    await prefs.setString('token', token);
  }

  // get user token from local db
  static Future<String?> getUserToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    String? token = prefs.getString('token');

    return token;
  }

  // remove user token from local db
  static Future<void> removeUserToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    await prefs.remove('token');
  }

  // remove fcm token from local db
  static Future<void> removeFCMToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    await prefs.remove('fcm_token');
  }

  // save language name to local db
  static Future<void> setLanguage(String languageName) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    await prefs.setString('language', languageName);
  }

  // get language name from local db
  static Future<String?> getLanguage() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    String? languageName = prefs.getString('language');

    return languageName;
  }

  // remove language name from local db
  static Future<void> removeLanguage() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    await prefs.remove('language');
  }
}
