import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:java_code_onboarding/constant/core/api_const.dart';
import 'package:java_code_onboarding/utils/services/interceptor/log_interceptor.dart';

class ApiServices {
  ApiServices._();

  static Dio dioCall({
    int timeout = 20000,
    String? token,
    String? authorization,
  }) {
    var headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
    };

    if (token != null) {
      headers['token'] = token;
    }

    if (authorization != null) {
      headers['Authorization'] = authorization;
    }

    final dio = Dio(
      BaseOptions(
        connectTimeout: Duration(milliseconds: timeout),
        headers: headers,
        baseUrl: ApiConstants.baseUrl,
      ),
    );

    if (kProfileMode || kDebugMode) {
      dio.interceptors.add(APILogInterceptor());
    }

    return dio;
  }
}
