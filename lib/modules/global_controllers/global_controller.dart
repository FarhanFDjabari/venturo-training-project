import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:java_code_onboarding/config/routes/routes.dart';
import 'package:java_code_onboarding/utils/enums/connection_status.dart';

import '../../shared/widgets/no_connection_view.dart';

class GlobalController extends GetxController {
  static GlobalController get to => Get.find<GlobalController>();

  // connectivity instance
  final Connectivity _connectivity = Connectivity();

  // connection status
  Rx<ConnectionStatus> connectionStatus = ConnectionStatus.Unknown.obs;

  @override
  void onInit() {
    super.onInit();

    // check connection state
    checkConnectionState();

    // listen to connection changes
    _connectivity.onConnectivityChanged.listen(_updateConnectivityState);
  }

  void checkConnectionState() {
    _connectivity.checkConnectivity().then(_updateConnectivityState);
  }

  void _updateConnectivityState(ConnectivityResult result) {
    switch (result) {
      case ConnectivityResult.mobile:
        connectionStatus(ConnectionStatus.Online);
        if (Get.isDialogOpen == true &&
            Get.currentRoute != AppRoutes.dashboardView) {
          Get.back();
        }
        break;
      case ConnectivityResult.wifi:
        connectionStatus(ConnectionStatus.Online);
        if (Get.isDialogOpen == true &&
            Get.currentRoute != AppRoutes.dashboardView) {
          Get.back();
        }
        break;
      case ConnectivityResult.none:
        connectionStatus(ConnectionStatus.Offline);
        if (connectionStatus.value == ConnectionStatus.Offline &&
            Get.currentRoute != AppRoutes.splashView) {
          showConnectionErrorAlert();
        }
        break;
      default:
        connectionStatus(ConnectionStatus.Offline);
        if (connectionStatus.value == ConnectionStatus.Offline &&
            Get.currentRoute != AppRoutes.splashView) {
          showConnectionErrorAlert();
        }
    }
  }

  Future<void> showConnectionErrorAlert() async {
    await Get.defaultDialog(
      title: '',
      titleStyle: const TextStyle(fontSize: 0),
      barrierDismissible: false,
      content: const NoConnectionView(),
    );
  }
}
