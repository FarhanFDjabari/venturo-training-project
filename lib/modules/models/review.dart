import 'package:equatable/equatable.dart';

class Review extends Equatable {
  final int idReview;
  final String nama;
  final double score;
  final String type;
  final String review;
  final String? image;

  const Review({
    required this.idReview,
    required this.nama,
    required this.score,
    required this.type,
    required this.review,
    required this.image,
  });

  @override
  List<Object?> get props => [idReview];

  /// From json
  factory Review.fromJson(Map<String, dynamic> json) {
    return Review(
      idReview: json['id_review'] as int,
      nama: json['nama'] as String,
      score: json['score'] as double,
      type: json['type'] as String,
      review: json['review'] as String,
      image: json['image'] as String?,
    );
  }

  static Review dummy = const Review(
    idReview: 1,
    nama: 'dev noersy',
    score: 4,
    type: 'Fasilitas',
    review: 'Mohon Menjaga kebersihan, kemarin meja masih kotor',
    image: 'https://picsum.photos/300/200',
  );
}

class ListReviewRes {
  final int statusCode;
  final String? message;
  final List<Review>? data;

  const ListReviewRes({
    required this.statusCode,
    this.message,
    this.data,
  });

  /// From json
  factory ListReviewRes.fromJson(Map<String, dynamic> json) {
    return ListReviewRes(
      statusCode: json['status_code'] as int,
      message: json['message'] as String?,
      data: json['status_code'] == 200
          ? json['data'].map<Review>((e) => Review.fromJson(e)).toList()
          : null,
    );
  }
}
