// ignore_for_file: must_be_immutable

import 'package:equatable/equatable.dart';
import 'package:java_code_onboarding/constant/common/constants.dart';
import 'package:java_code_onboarding/modules/models/discount.dart';
import 'package:java_code_onboarding/modules/models/menu.dart';
import 'package:java_code_onboarding/modules/models/user.dart';
import 'package:java_code_onboarding/modules/models/voucher.dart';

class CartItem extends Equatable {
  Menu menu;
  int quantity;
  String note;
  MenuVariant? level;
  List<MenuVariant>? toppings;

  CartItem({
    required this.menu,
    required this.quantity,
    required this.note,
    required this.level,
    required this.toppings,
  });

  // check if the cart item is food
  bool get isFood => menu.kategori == AppConstants.foodCategory;

  // check if the cart item is drink
  bool get isDrink => menu.kategori == AppConstants.drinkCategory;

  // get the total price of the levels in cart item
  int get totalLevelPrice {
    if (level == null) {
      return 0;
    } else {
      return level!.harga;
    }
  }

  // get the total price of the toppings in cart item
  int get totalToppingsPrice {
    if (toppings == null) {
      return 0;
    } else {
      return toppings!.fold<int>(0, (total, topping) => total + topping.harga);
    }
  }

  // get the subtotal price of the cart item
  int get price {
    return menu.harga + totalLevelPrice + totalToppingsPrice;
  }

  // get the total price of the cart item
  int get totalPrice {
    return price * quantity;
  }

  @override
  List<Object?> get props => [menu.idMenu];
}

class CartReq {
  final User user;
  final List<CartItem> cart;
  final List<Discount>? discounts;
  final Voucher? voucher;
  final int discountPrice;
  final int totalPrice;

  CartReq({
    required this.user,
    required this.cart,
    this.discounts,
    this.voucher,
    required this.discountPrice,
    required this.totalPrice,
  });

  Map<String, dynamic> toMap() {
    return {
      'order': {
        'id_user': user.idUser,
        'id_voucher': voucher?.idVoucher,
        'id_diskon': discounts?.isEmpty ?? true
            ? null
            : discounts?.map((e) => e.idDiskon).toList(),
        'diskon': voucher == null ? 1 : 0,
        'potongan': discountPrice,
        'total_bayar': totalPrice,
      },
      'menu': cart
          .map((e) => {
                'id_menu': e.menu.idMenu,
                'harga': e.price,
                'level': e.level?.idDetail,
                'topping': e.toppings?.isEmpty ?? true
                    ? null
                    : e.toppings?.map((e) => e.idDetail).toList(),
                'jumlah': e.quantity,
              })
          .toList(),
    };
  }
}
