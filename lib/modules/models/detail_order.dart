import 'package:equatable/equatable.dart';
import 'package:java_code_onboarding/constant/common/constants.dart';

class DetailOrder extends Equatable {
  final int idMenu;
  final String kategori;
  final String topping;
  final String nama;
  final String? foto;
  final int jumlah;
  final String harga;
  final int total;
  final String catatan;

  const DetailOrder({
    required this.idMenu,
    required this.kategori,
    required this.topping,
    required this.nama,
    required this.foto,
    required this.jumlah,
    required this.harga,
    required this.total,
    required this.catatan,
  });

  /// Apakah menu ini adalah makanan
  bool get isFood => kategori == AppConstants.foodCategory;

  /// Apakah menu ini adalah minuman
  bool get isDrink => kategori == AppConstants.drinkCategory;

  /// From json
  factory DetailOrder.fromJson(Map<String, dynamic> json) {
    return DetailOrder(
      idMenu: json['id_menu'] as int,
      kategori: json['kategori'] as String,
      topping: json['topping'] as String,
      nama: json['nama'] as String,
      foto: json['foto'] as String?,
      jumlah: json['jumlah'] as int,
      harga: json['harga'] as String,
      total: json['total'] as int,
      catatan: json['catatan'] as String,
    );
  }

  @override
  List<Object?> get props => [];
}
