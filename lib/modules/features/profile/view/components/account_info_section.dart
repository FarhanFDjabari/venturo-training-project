import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:java_code_onboarding/config/themes/colors.dart';
import 'package:java_code_onboarding/modules/features/profile/controllers/profile_controller.dart';
import 'package:java_code_onboarding/shared/widgets/tile_option.dart';
import 'package:java_code_onboarding/utils/extensions/string_ext.dart';

class AccountInfoSection extends StatelessWidget {
  const AccountInfoSection({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16.w),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.only(left: 20.w),
            child: Text(
              'Account info'.tr,
              style: Get.textTheme.titleMedium!
                  .copyWith(color: AppColor.kBlueColor),
            ),
          ),
          14.verticalSpacingRadius,
          Container(
            padding: EdgeInsets.symmetric(horizontal: 20.w, vertical: 30.h),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(30.r),
              color: AppColor.kLightColor2,
            ),
            child: Column(
              children: [
                Obx(
                  () => TileOption(
                    title: 'Name'.tr,
                    message: ProfileController.to.user.value.nama,
                    onTap: ProfileController.to.updateProfileName,
                  ),
                ),
                Divider(color: AppColor.kDarkColor4, height: 0.5.h),
                Obx(
                  () => TileOption(
                    title: 'Birth date'.tr,
                    message: ProfileController.to.user.value.tglLahir != null
                        ? ProfileController.to.user.value.tglLahir!
                        : '-',
                    onTap: ProfileController.to.updateBirthDate,
                  ),
                ),
                Divider(color: AppColor.kDarkColor4, height: 0.5.h),
                Obx(
                  () => TileOption(
                    title: 'Phone number'.tr,
                    message: ProfileController.to.user.value.telepon ?? '-',
                    onTap: ProfileController.to.updatePhoneNumber,
                  ),
                ),
                Divider(color: AppColor.kDarkColor4, height: 0.5.h),
                Obx(
                  () => TileOption(
                    title: 'Email'.tr,
                    message: ProfileController.to.user.value.email,
                    onTap: ProfileController.to.updateEmail,
                  ),
                ),
                Divider(color: AppColor.kDarkColor4, height: 0.5.h),
                Obx(
                  () => TileOption(
                    title: 'Change PIN'.tr,
                    message: ProfileController.to.user.value.pin.toObscured,
                    onTap: ProfileController.to.updatePin,
                  ),
                ),
                Divider(color: AppColor.kDarkColor4, height: 0.5.h),
                Obx(
                  () => TileOption(
                    title: 'Change language'.tr,
                    message: ProfileController.to.currentLang.value,
                    onTap: ProfileController.to.updateLanguage,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
