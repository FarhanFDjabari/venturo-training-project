import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:java_code_onboarding/config/routes/routes.dart';
import 'package:java_code_onboarding/config/themes/colors.dart';
import 'package:java_code_onboarding/constant/core/asset_const.dart';
import 'package:java_code_onboarding/shared/widgets/primary_button_with_title.dart';

class ReviewSection extends StatelessWidget {
  const ReviewSection({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16.w),
      child: Material(
        borderRadius: BorderRadius.circular(30.r),
        clipBehavior: Clip.antiAlias,
        child: InkWell(
          onTap: () {},
          child: Ink(
            padding: EdgeInsets.symmetric(horizontal: 20.r, vertical: 16.r),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(30.r),
              color: AppColor.kLightColor2,
            ),
            child: Row(
              children: [
                SvgPicture.asset(AssetConstants.iconReview),
                8.horizontalSpace,
                Text('Review'.tr, style: Get.textTheme.titleSmall),
                const Spacer(),
                PrimaryButtonWithTitle.compact(
                  width: 134.w,
                  height: 35.h,
                  title: 'Review now'.tr,
                  borderColor: AppColor.kLightColor,
                  onPressed: () {
                    Get.toNamed(AppRoutes.reviewView);
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
