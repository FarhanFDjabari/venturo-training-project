import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:java_code_onboarding/config/themes/colors.dart';
import 'package:java_code_onboarding/constant/common/constants.dart';
import 'package:java_code_onboarding/modules/features/profile/controllers/profile_controller.dart';
import 'package:java_code_onboarding/shared/widgets/tile_option.dart';

class DeviceInfoSection extends StatelessWidget {
  const DeviceInfoSection({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16.w),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.only(left: 20.w),
            child: Text(
              'More info'.tr,
              style: Get.textTheme.titleMedium!
                  .copyWith(color: AppColor.kBlueColor),
            ),
          ),
          14.verticalSpace,
          Container(
            padding: EdgeInsets.symmetric(horizontal: 20.w, vertical: 30.h),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(30.r),
              color: AppColor.kLightColor2,
            ),
            child: Column(
              children: [
                Obx(
                  () => TileOption(
                    title: 'Device info'.tr,
                    message: ProfileController.to.deviceInfo.value,
                  ),
                ),
                TileOption(
                  title: 'Version'.tr,
                  message: AppConstants.appVersion,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
