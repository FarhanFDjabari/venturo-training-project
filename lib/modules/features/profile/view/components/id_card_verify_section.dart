import 'package:flutter/material.dart';
import 'package:flutter_conditional_rendering/flutter_conditional_rendering.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:java_code_onboarding/config/themes/colors.dart';
import 'package:java_code_onboarding/constant/core/asset_const.dart';
import 'package:java_code_onboarding/modules/features/profile/controllers/profile_controller.dart';

class IdCardVerifySection extends StatelessWidget {
  const IdCardVerifySection({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Conditional.single(
        context: context,
        conditionBuilder: (context) =>
            ProfileController.to.user.value.ktp != null,
        widgetBuilder: (context) => Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(Icons.check, color: AppColor.kGreenColor, size: 20.r),
            8.horizontalSpace,
            Text(
              'You have verified your ID card'.tr,
              style: Get.textTheme.labelMedium!
                  .copyWith(color: AppColor.kBlueColor),
            ),
          ],
        ),
        fallbackBuilder: (context) => Center(
          child: InkWell(
            onTap: ProfileController.to.updateKtp,
            borderRadius: BorderRadius.circular(5.r),
            child: Padding(
              padding: EdgeInsets.symmetric(
                horizontal: 10,
                vertical: 5.r,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: [
                  SvgPicture.asset(
                    AssetConstants.iconKtp,
                    width: 18.r,
                    height: 18.r,
                    colorFilter: const ColorFilter.mode(
                      AppColor.kBlueColor,
                      BlendMode.srcIn,
                    ),
                  ),
                  8.horizontalSpace,
                  Text(
                    'Verify your ID card now!'.tr,
                    style: Get.textTheme.labelMedium!
                        .copyWith(color: AppColor.kBlueColor),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
