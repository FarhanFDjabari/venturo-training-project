import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_conditional_rendering/conditional.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:java_code_onboarding/config/themes/colors.dart';
import 'package:java_code_onboarding/constant/core/asset_const.dart';
import 'package:java_code_onboarding/modules/features/profile/controllers/profile_controller.dart';

class ProfileImage extends StatelessWidget {
  const ProfileImage({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
        width: 170.r,
        height: 170.r,
        clipBehavior: Clip.antiAlias,
        decoration: const BoxDecoration(shape: BoxShape.circle),
        child: Stack(
          children: [
            Obx(
              () => Conditional.single(
                context: context,
                conditionBuilder: (context) =>
                    ProfileController.to.user.value.foto != null,
                widgetBuilder: (context) => CachedNetworkImage(
                  imageUrl: ProfileController.to.user.value.foto!,
                  width: 170.r,
                  height: 170.r,
                  fit: BoxFit.cover,
                ),
                fallbackBuilder: (context) => Image.asset(
                  AssetConstants.bgProfile,
                  width: 170.r,
                  height: 170.r,
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Material(
                color: AppColor.kBlueColor,
                child: InkWell(
                  onTap: ProfileController.to.updatePhotoProfile,
                  child: Container(
                    width: double.infinity,
                    padding: EdgeInsets.only(top: 10.h, bottom: 15.h),
                    child: Text(
                      'Change'.tr,
                      style: Get.textTheme.labelMedium!
                          .copyWith(color: Colors.white),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ));
  }
}
