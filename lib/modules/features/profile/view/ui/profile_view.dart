import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:java_code_onboarding/config/themes/colors.dart';
import 'package:java_code_onboarding/constant/core/asset_const.dart';
import 'package:java_code_onboarding/modules/features/profile/controllers/profile_controller.dart';
import 'package:java_code_onboarding/modules/features/profile/view/components/account_info_section.dart';
import 'package:java_code_onboarding/modules/features/profile/view/components/device_info_section.dart';
import 'package:java_code_onboarding/modules/features/profile/view/components/id_card_verify_section.dart';
import 'package:java_code_onboarding/modules/features/profile/view/components/profile_image.dart';
import 'package:java_code_onboarding/modules/features/profile/view/components/review_section.dart';
import 'package:java_code_onboarding/shared/widgets/primary_button_with_title.dart';
import 'package:java_code_onboarding/shared/widgets/rounded_custom_app_bar.dart';

class ProfileView extends StatelessWidget {
  const ProfileView({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: RoundedAppBar(
        title: 'Profile'.tr,
        enableBackButton: false,
        titleStyle: Get.textTheme.headlineMedium?.copyWith(
          color: AppColor.kBlueColor,
        ),
        titleWidget: ColoredBox(
          color: AppColor.kBlueColor,
          child: SizedBox(
            width: 65.w,
            height: 2.h,
          ),
        ),
      ),
      body: Container(
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage(AssetConstants.bgPattern2),
            fit: BoxFit.fitHeight,
            alignment: Alignment.center,
          ),
        ),
        child: CustomScrollView(
          physics: const ClampingScrollPhysics(),
          slivers: [
            SliverToBoxAdapter(child: 33.verticalSpace),
            const SliverToBoxAdapter(
              child: Align(
                alignment: Alignment.center,
                child: ProfileImage(),
              ),
            ),
            SliverToBoxAdapter(child: 10.verticalSpace),
            const SliverToBoxAdapter(child: IdCardVerifySection()),
            SliverToBoxAdapter(child: 10.verticalSpace),
            const SliverToBoxAdapter(
              child: AccountInfoSection(),
            ),
            SliverToBoxAdapter(child: 18.verticalSpace),
            const SliverToBoxAdapter(
              child: ReviewSection(),
            ),
            SliverToBoxAdapter(child: 27.verticalSpace),
            const SliverToBoxAdapter(
              child: DeviceInfoSection(),
            ),
            SliverToBoxAdapter(child: 32.verticalSpace),
            SliverPadding(
              padding: EdgeInsets.symmetric(horizontal: 102.w),
              sliver: SliverToBoxAdapter(
                child: PrimaryButtonWithTitle(
                  title: 'Log out'.tr,
                  height: 45.h,
                  onPressed: ProfileController.to.logout,
                ),
              ),
            ),
            SliverToBoxAdapter(child: 32.verticalSpace),
          ],
        ),
      ),
    );
  }
}
