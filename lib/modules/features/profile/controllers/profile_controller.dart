import 'dart:convert';

import 'package:device_info_plus/device_info_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:java_code_onboarding/config/localization/localization.dart';
import 'package:java_code_onboarding/config/routes/routes.dart';
import 'package:java_code_onboarding/config/themes/colors.dart';
import 'package:java_code_onboarding/modules/features/cart/view/components/pin_dialog.dart';
import 'package:java_code_onboarding/modules/features/profile/repositories/profile_repository.dart';
import 'package:java_code_onboarding/modules/features/profile/view/components/email_bottom_sheet.dart';
import 'package:java_code_onboarding/modules/features/profile/view/components/language_bottom_sheet.dart';
import 'package:java_code_onboarding/modules/features/profile/view/components/name_bottom_sheet.dart';
import 'package:java_code_onboarding/modules/features/profile/view/components/phone_bottom_sheet.dart';
import 'package:java_code_onboarding/modules/models/user.dart';
import 'package:java_code_onboarding/shared/widgets/confirmation_dialog.dart';
import 'package:java_code_onboarding/shared/widgets/image_picker_dialog.dart';
import 'package:java_code_onboarding/utils/extensions/date_ext.dart';
import 'package:java_code_onboarding/utils/services/local_db_services.dart';
import 'package:image_cropper/image_cropper.dart';

class ProfileController extends GetxController {
  static ProfileController get to => Get.find<ProfileController>();

  Rx<User> user = Rx<User>(User.dummy);
  RxString currentLang = Localization.currentLanguage.obs;
  RxString deviceInfo = ''.obs;

  @override
  void onInit() {
    super.onInit();

    getInitialData();
  }

  void getInitialData() async {
    final currentUser = await LocalDBServices.getCurrentUser();
    user(currentUser);

    final deviceInfoData = await DeviceInfoPlugin().androidInfo;
    deviceInfo('${deviceInfoData.manufacturer} ${deviceInfoData.model}');

    getUserData();
  }

  Future<void> getUserData() async {
    final result = await ProfileRepository.getProfile();

    if (result.statusCode == 200 && result.user != null) {
      user(User.dummy);
      user(result.user);
      await LocalDBServices.setUser(result.user!);
    }
  }

  Future<void> logout() async {
    final isLogout = await Get.defaultDialog(
      title: '',
      titleStyle: const TextStyle(fontSize: 0),
      content: ConfirmationDialog(
        title: 'Are you sure want to logout?'.tr,
      ),
    );

    if (isLogout == true) {
      await LocalDBServices.removeUserToken();
      await LocalDBServices.removeCurrentUser();
      await Get.offAllNamed(AppRoutes.loginView);
    }
  }

  Future<void> updatePhotoProfile() async {
    //show photo picker dialog
    ImageSource? imgSource = await Get.defaultDialog(
      title: '',
      titleStyle: const TextStyle(fontSize: 0),
      content: const ImagePickerDialog(),
    );

    if (imgSource == null) return;

    final image = await ImagePicker().pickImage(
      source: imgSource,
      maxWidth: 300,
      maxHeight: 300,
      imageQuality: 60,
    );

    if (image == null) return;

    final CroppedFile? croppedFile = await ImageCropper().cropImage(
      sourcePath: image.path,
      aspectRatioPresets: [CropAspectRatioPreset.square],
      uiSettings: [
        AndroidUiSettings(
          toolbarTitle: 'Cropper'.tr,
          toolbarColor: AppColor.kBlueColor,
          toolbarWidgetColor: AppColor.kLightColor,
          initAspectRatio: CropAspectRatioPreset.square,
          lockAspectRatio: true,
        ),
      ],
    );

    if (croppedFile == null) return;

    final base64Image = base64.encode(await croppedFile.readAsBytes());

    final result = await ProfileRepository.updatePhotoProfile(base64Image);

    if (result.statusCode == 200 && result.user != null) {
      user(User.dummy);
      user(result.user);
      await LocalDBServices.setUser(result.user!);
    }
  }

  Future<void> updateKtp() async {
    ImageSource? imgSource = await Get.defaultDialog(
      title: '',
      titleStyle: const TextStyle(fontSize: 0),
      content: const ImagePickerDialog(),
    );

    if (imgSource == null) return;

    final image = await ImagePicker().pickImage(
      source: imgSource,
      maxWidth: 1500,
      maxHeight: 1500,
      imageQuality: 90,
    );

    if (image == null) return;

    final base64Image = base64.encode(await image.readAsBytes());

    final result = await ProfileRepository.updateKtp(base64Image);

    if (result.statusCode == 200 && result.user != null) {
      user(User.dummy);
      user(result.user);
      await LocalDBServices.setUser(result.user!);
    }
  }

  Future<void> updateUser({
    String? nama,
    DateTime? tglLahir,
    String? telepon,
    String? email,
    String? pin,
  }) async {
    final reqData = <String, String>{};

    if (nama != null) reqData["nama"] = nama;
    if (tglLahir != null) reqData["tgl_lahir"] = tglLahir.toDateString;
    if (telepon != null) reqData["telepon"] = telepon;
    if (email != null) reqData["email"] = email;
    if (pin != null) reqData["pin"] = pin;

    final result = await ProfileRepository.updateProfile(reqData);

    if (result.statusCode == 200 && result.user != null) {
      user(User.dummy);
      user(result.user);
      await LocalDBServices.setUser(result.user!);
    }
  }

  Future<void> updateProfileName() async {
    String? nameInput = await Get.bottomSheet(
      NameBottomSheet(nama: user.value.nama),
      backgroundColor: AppColor.kLightColor,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(30.r),
        ),
      ),
      isScrollControlled: true,
    );

    if (nameInput != null && nameInput.isNotEmpty) {
      await updateUser(nama: nameInput);
    }
  }

  Future<void> updateBirthDate() async {
    DateTime? birthDate = await showDatePicker(
      context: Get.context!,
      initialDate: DateTime(DateTime.now().year - 21),
      firstDate: DateTime(DateTime.now().year - 100),
      lastDate: DateTime.now(),
    );

    if (birthDate != null) {
      await updateUser(tglLahir: birthDate);
    }
  }

  Future<void> updatePhoneNumber() async {
    String? phoneNumber = await Get.bottomSheet(
      PhoneBottomSheet(telepon: user.value.telepon ?? ''),
      backgroundColor: AppColor.kLightColor,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(30.r),
        ),
      ),
      isScrollControlled: true,
    );

    if (phoneNumber != null && phoneNumber.isNotEmpty) {
      await updateUser(telepon: phoneNumber);
    }
  }

  Future<void> updateEmail() async {
    String? email = await Get.bottomSheet(
      EmailBottomSheet(email: user.value.email),
      backgroundColor: AppColor.kLightColor,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(30.r),
        ),
      ),
      isScrollControlled: true,
    );

    if (email != null && email.isNotEmpty) {
      await updateUser(email: email);
    }
  }

  Future<void> updatePin() async {
    String? pin = await Get.bottomSheet(
      PinDialog(pin: user.value.pin),
      backgroundColor: AppColor.kLightColor,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(30.r),
        ),
      ),
      isScrollControlled: true,
    );

    if (pin != null && pin.isNotEmpty) {
      await updateUser(pin: pin);
    }
  }

  Future<void> updateLanguage() async {
    String? language = await Get.bottomSheet(
      const LanguageBottomSheet(),
      backgroundColor: AppColor.kLightColor,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(30.r),
        ),
      ),
      isScrollControlled: true,
    );

    if (language != null) {
      Localization.changeLocale(language);
      await LocalDBServices.setLanguage(language);
      currentLang(language);
    }
  }
}
