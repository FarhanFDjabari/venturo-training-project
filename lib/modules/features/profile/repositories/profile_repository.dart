import 'package:dio/dio.dart';
import 'package:java_code_onboarding/constant/core/api_const.dart';
import 'package:java_code_onboarding/modules/models/user.dart';
import 'package:java_code_onboarding/utils/services/api_services.dart';
import 'package:java_code_onboarding/utils/services/local_db_services.dart';

class ProfileRepository {
  ProfileRepository._();

  static Future<UserRes> getProfile() async {
    try {
      final userToken = await LocalDBServices.getUserToken();
      final userData = await LocalDBServices.getCurrentUser();
      final dio = ApiServices.dioCall(token: userToken);

      final response =
          await dio.get('${ApiConstants.userDetail}/${userData?.idUser}');

      return UserRes.fromJson(response.data);
    } on DioError {
      return const UserRes(statusCode: 500);
    }
  }

  static Future<UserRes> updateProfile(Map<String, String> data) async {
    try {
      final userToken = await LocalDBServices.getUserToken();
      final userData = await LocalDBServices.getCurrentUser() as User;
      final dio = ApiServices.dioCall(token: userToken);
      final response = await dio
          .post('${ApiConstants.userUpdate}/${userData.idUser}', data: data);

      return UserRes.fromJson(response.data);
    } on DioError {
      return const UserRes(statusCode: 500);
    }
  }

  static Future<UserRes> updatePhotoProfile(String photo) async {
    try {
      final userToken = await LocalDBServices.getUserToken();
      final userData = await LocalDBServices.getCurrentUser();
      final dio = ApiServices.dioCall(token: userToken);
      final response = await dio.post(
        '${ApiConstants.updateProfilUser}/${userData?.idUser}',
        data: {'image': photo},
      );

      return UserRes.fromJson(response.data);
    } on DioError {
      return const UserRes(statusCode: 500);
    }
  }

  static Future<UserRes> updateKtp(String ktpPhoto) async {
    try {
      final userToken = await LocalDBServices.getUserToken();
      final userData = await LocalDBServices.getCurrentUser();
      final dio = ApiServices.dioCall(token: userToken);
      final response = await dio.post(
        '${ApiConstants.updateKtpUser}/${userData?.idUser}',
        data: {'image': ktpPhoto},
      );

      return UserRes.fromJson(response.data);
    } on DioError {
      return const UserRes(statusCode: 500);
    }
  }
}
