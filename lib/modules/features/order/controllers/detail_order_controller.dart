import 'dart:async';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:java_code_onboarding/modules/features/order/controllers/order_controller.dart';
import 'package:java_code_onboarding/modules/features/order/repositories/order_repository.dart';
import 'package:java_code_onboarding/modules/models/detail_order.dart';
import 'package:java_code_onboarding/modules/models/order.dart';
import 'package:java_code_onboarding/shared/customs/error_snackbar.dart';
import 'package:java_code_onboarding/shared/customs/success_snackbar.dart';
import 'package:java_code_onboarding/shared/widgets/confirmation_dialog.dart';
import 'package:java_code_onboarding/utils/enums/order_detail_state.dart';

class DetailOrderController extends GetxController {
  static DetailOrderController get to => Get.find<DetailOrderController>();

  // order data
  Rx<OrderDetailState> orderDetailState = OrderDetailState.Loading.obs;
  Rxn<Order> order = Rxn<Order>();
  Timer? timer;

  @override
  void onInit() {
    super.onInit();

    final orderId = int.parse(Get.parameters['orderId'] as String);

    getOrderDetail(orderId).then((value) {
      timer = Timer.periodic(
        const Duration(seconds: 10),
        (_) => getOrderDetail(orderId, isPeriodic: true),
      );
    });
  }

  @override
  void onClose() {
    timer?.cancel();
    super.onClose();
  }

  Future<void> getOrderDetail(int orderId, {bool isPeriodic = false}) async {
    if (!isPeriodic) {
      orderDetailState(OrderDetailState.Loading);
    }
    final result = await OrderRepository.getOrderDetail(orderId);

    if (result.statusCode == 200) {
      orderDetailState(OrderDetailState.Success);
      order(result.data);
    } else {
      orderDetailState(OrderDetailState.Error);
    }
  }

  Future<void> cancelOrder() async {
    final isCancel = await Get.defaultDialog(
      title: '',
      titleStyle: const TextStyle(fontSize: 0),
      content:
          ConfirmationDialog(title: 'Are you sure want to cancel order?'.tr),
    );

    if (isCancel == true) {
      final result =
          await OrderRepository.cancelOrder(order.value?.idOrder ?? 0);

      if (result == 200) {
        await getOrderDetail(order.value?.idOrder ?? 0);

        Get.showSnackbar(SuccessSnackBar(
          title: 'Success!'.tr,
          message: 'Your order has been canceled'.tr,
        ));

        OrderController.to.getOngoingOrders();
        OrderController.to.getOrderHistories();

        Get.back(closeOverlays: true);
      } else {
        Get.showSnackbar(ErrorSnackBar(
          title: 'Something went wrong'.tr,
          message: 'Unknown error'.tr,
        ));
      }
    }
  }

  List<DetailOrder> get foodItems =>
      order.value?.menu.where((element) => element.isFood).toList() ?? [];

  List<DetailOrder> get drinkItems =>
      order.value?.menu.where((element) => element.isDrink).toList() ?? [];
}
