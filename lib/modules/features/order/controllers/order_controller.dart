import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:java_code_onboarding/config/routes/routes.dart';
import 'package:java_code_onboarding/modules/features/cart/controllers/cart_controller.dart';
import 'package:java_code_onboarding/modules/features/home/controllers/home_controller.dart';
import 'package:java_code_onboarding/modules/features/order/repositories/order_repository.dart';
import 'package:java_code_onboarding/modules/models/cart_item.dart';
import 'package:java_code_onboarding/modules/models/order.dart';
import 'package:java_code_onboarding/utils/enums/ongoing_order_state.dart';
import 'package:java_code_onboarding/utils/enums/order_history_filter_category.dart';
import 'package:java_code_onboarding/utils/enums/order_history_state.dart';
import 'package:java_code_onboarding/utils/extensions/currency_ext.dart';

class OrderController extends GetxController {
  static OrderController get to => Get.find<OrderController>();

  @override
  void onInit() {
    super.onInit();

    getOngoingOrders();
    getOrderHistories();
  }

  RxList<Order> onGoingOrders = RxList<Order>();
  RxList<Order> historyOrders = RxList<Order>();
  Rx<OnGoingOrderState> onGoingOrderState = OnGoingOrderState.Loading.obs;
  Rx<OrderHistoryState> orderHistoryState = OrderHistoryState.Loading.obs;

  Rx<String> selectedCategory = OrderHistoryFilterCategory.All.name.obs;

  Map<String, String> get dateFilterStatus => {
        OrderHistoryFilterCategory.All.name: 'All status'.tr,
        OrderHistoryFilterCategory.Completed.name: 'Completed'.tr,
        OrderHistoryFilterCategory.Canceled.name: 'Canceled'.tr,
      };

  Rx<DateTimeRange> selectedDateRange = DateTimeRange(
    start: DateTime.now().subtract(const Duration(days: 30)),
    end: DateTime.now(),
  ).obs;

  Future<void> getOngoingOrders() async {
    onGoingOrderState(OnGoingOrderState.Loading);

    final result = await OrderRepository.getOngoingOrders();

    if (result.statusCode == 200) {
      final data =
          result.data?.where((element) => element.status != 4).toList();

      if (data?.isNotEmpty == true) {
        onGoingOrderState(OnGoingOrderState.Success);
        onGoingOrders(data?.reversed.toList());
      } else {
        onGoingOrderState(OnGoingOrderState.Empty);
      }
    } else if (result.statusCode == 204) {
      onGoingOrderState(OnGoingOrderState.Empty);
    } else {
      onGoingOrderState(OnGoingOrderState.Error);
    }
  }

  Future<void> getOrderHistories() async {
    orderHistoryState(OrderHistoryState.Loading);

    final result = await OrderRepository.getOrderHistory();

    if (result.statusCode == 200) {
      final data = result.data;

      if (data?.isNotEmpty == true) {
        orderHistoryState(OrderHistoryState.Success);
        historyOrders(data?.reversed.toList());
      } else {
        orderHistoryState(OrderHistoryState.Empty);
      }
    } else if (result.statusCode == 204) {
      orderHistoryState(OrderHistoryState.Empty);
    } else {
      orderHistoryState(OrderHistoryState.Error);
    }
  }

  void setDateFilter({String? category, DateTimeRange? range}) {
    selectedCategory(category);
    selectedDateRange(range);
  }

  List<Order> get filteredHistoryOrder {
    final historyOrderList = historyOrders.toList();

    if (selectedCategory.value == OrderHistoryFilterCategory.Canceled.name) {
      historyOrderList.removeWhere((element) => element.status != 4);
    } else if (selectedCategory.value ==
        OrderHistoryFilterCategory.Completed.name) {
      historyOrderList.removeWhere((element) => element.status != 3);
    }

    historyOrderList.removeWhere((element) =>
        element.tanggal.isBefore(selectedDateRange.value.start) ||
        element.tanggal.isAfter(selectedDateRange.value.end));

    historyOrderList.sort((a, b) => b.tanggal.compareTo(a.tanggal));

    return historyOrderList;
  }

  void onRepeatOrder(Order order) {
    // check if menu is still available, add to cart if available
    for (final menu in order.menu) {
      final menuData = HomeController.to.listMenu
          .firstWhereOrNull((element) => element.idMenu == menu.idMenu);

      if (menuData != null) {
        CartController.to.addToCart(
          CartItem(
              menu: menuData,
              quantity: menu.jumlah,
              note: '',
              level: null,
              toppings: null),
        );
      }
    }

    // to cart view
    Get.toNamed(AppRoutes.cartView);
  }

  String get totalHistoryOrder {
    final total = filteredHistoryOrder
        .where((e) => e.status == 3)
        .fold(0, (previousValue, element) => previousValue + element.total);

    return total.toRupiah;
  }
}
