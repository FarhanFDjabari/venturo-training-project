import 'package:dio/dio.dart';
import 'package:java_code_onboarding/constant/core/api_const.dart';
import 'package:java_code_onboarding/modules/models/order.dart';
import 'package:java_code_onboarding/utils/services/api_services.dart';
import 'package:java_code_onboarding/utils/services/local_db_services.dart';

class OrderRepository {
  OrderRepository._();

  // get ongoing orders
  static Future<ListOrderRes> getOngoingOrders() async {
    try {
      final userToken = await LocalDBServices.getUserToken();
      final user = await LocalDBServices.getCurrentUser();
      final dio = ApiServices.dioCall(token: userToken);
      final response =
          await dio.get('${ApiConstants.orderListOfUser}/${user?.idUser}');

      return ListOrderRes.fromJson(response.data);
    } on DioError {
      return const ListOrderRes(statusCode: 500);
    }
  }

  // get order history
  static Future<ListOrderRes> getOrderHistory() async {
    try {
      final userToken = await LocalDBServices.getUserToken();
      final user = await LocalDBServices.getCurrentUser();
      final dio = ApiServices.dioCall(token: userToken);
      final response =
          await dio.post('${ApiConstants.orderHistoryOfUser}/${user?.idUser}');
      if (response.data['status_code'] == 200) {
        response.data['data'] = response.data['data']['listData'];
      }

      return ListOrderRes.fromJson(response.data);
    } on DioError {
      return const ListOrderRes(statusCode: 500);
    }
  }

  // get order detail
  static Future<OrderRes> getOrderDetail(int idOrder) async {
    try {
      final userToken = await LocalDBServices.getUserToken();
      final dio = ApiServices.dioCall(token: userToken);
      final response = await dio.get('${ApiConstants.orderDetail}/$idOrder');

      return OrderRes.fromJson(response.data);
    } on DioError {
      return const OrderRes(statusCode: 500);
    }
  }

  // cancel order
  static Future<int> cancelOrder(int idOrder) async {
    try {
      final userToken = await LocalDBServices.getUserToken();
      final dio = ApiServices.dioCall(token: userToken);
      final response = await dio.post('${ApiConstants.orderCancel}/$idOrder');

      return response.data['status_code'];
    } on DioError {
      return 500;
    }
  }
}
