import 'package:flutter/material.dart';

import 'package:flutter_conditional_rendering/flutter_conditional_rendering.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:java_code_onboarding/config/themes/colors.dart';
import 'package:java_code_onboarding/constant/core/asset_const.dart';
import 'package:java_code_onboarding/modules/features/order/controllers/detail_order_controller.dart';
import 'package:java_code_onboarding/modules/features/order/view/components/order_list_sliver.dart';
import 'package:java_code_onboarding/modules/features/order/view/components/order_tracker.dart';
import 'package:java_code_onboarding/shared/widgets/rect_shimmer.dart';
import 'package:java_code_onboarding/shared/widgets/rounded_custom_app_bar.dart';
import 'package:java_code_onboarding/shared/widgets/section_header.dart';
import 'package:java_code_onboarding/shared/widgets/server_error_list_view.dart';
import 'package:java_code_onboarding/shared/widgets/tile_option.dart';
import 'package:java_code_onboarding/utils/enums/order_detail_state.dart';
import 'package:java_code_onboarding/utils/extensions/currency_ext.dart';

class DetailOrderView extends StatelessWidget {
  const DetailOrderView({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: RoundedAppBar(
        title: 'Order'.tr,
        iconAssetUrl: AssetConstants.iconOrder,
        actions: [
          Obx(
            () => Conditional.single(
              context: context,
              conditionBuilder: (context) =>
                  DetailOrderController.to.order.value?.status == 0,
              widgetBuilder: (context) => Padding(
                padding: EdgeInsets.symmetric(vertical: 16.h, horizontal: 10.w),
                child: TextButton(
                  onPressed: DetailOrderController.to.cancelOrder,
                  style: TextButton.styleFrom(
                    padding: EdgeInsets.zero,
                    visualDensity: VisualDensity.compact,
                  ),
                  child: Text(
                    'Cancel'.tr,
                    style: Get.textTheme.labelLarge
                        ?.copyWith(color: AppColor.kRedColor),
                  ),
                ),
              ),
              fallbackBuilder: (context) => const SizedBox(),
            ),
          ),
        ],
      ),
      body: Obx(
        () => ConditionalSwitch.single(
          context: context,
          valueBuilder: (context) =>
              DetailOrderController.to.orderDetailState.value,
          caseBuilders: {
            OrderDetailState.Loading: (context) => ListView.separated(
                  padding: EdgeInsets.all(25.r),
                  separatorBuilder: (_, i) => 18.verticalSpacingRadius,
                  itemCount: 3,
                  itemBuilder: (_, i) => AspectRatio(
                    aspectRatio: 378 / 89,
                    child: RectShimmer(radius: 10.r),
                  ),
                ),
            OrderDetailState.Error: (context) => const ServerErrorListView(),
          },
          fallbackBuilder: (context) => CustomScrollView(
            physics: const ClampingScrollPhysics(),
            slivers: [
              SliverToBoxAdapter(child: 28.verticalSpace),
              if (DetailOrderController.to.foodItems.isNotEmpty) ...[
                SliverToBoxAdapter(
                  child: SectionHeader(
                    iconAssetUrl: AssetConstants.iconFood,
                    title: 'Food'.tr,
                    color: AppColor.kBlueColor,
                  ),
                ),
                SliverPadding(
                  padding:
                      EdgeInsets.symmetric(horizontal: 25.w, vertical: 8.h),
                  sliver: OrderListSliver(
                    orders: DetailOrderController.to.foodItems,
                  ),
                )
              ],
              SliverToBoxAdapter(child: 17.verticalSpace),
              if (DetailOrderController.to.drinkItems.isNotEmpty) ...[
                SliverToBoxAdapter(
                  child: SectionHeader(
                    iconAssetUrl: AssetConstants.iconFood,
                    title: 'Drink'.tr,
                    color: AppColor.kBlueColor,
                  ),
                ),
                SliverPadding(
                  padding:
                      EdgeInsets.symmetric(horizontal: 25.w, vertical: 8.h),
                  sliver: OrderListSliver(
                    orders: DetailOrderController.to.drinkItems,
                  ),
                )
              ],
            ],
          ),
        ),
      ),
      bottomNavigationBar: Obx(
        () => Conditional.single(
          context: context,
          conditionBuilder: (context) =>
              DetailOrderController.to.orderDetailState.value ==
              OrderDetailState.Success,
          widgetBuilder: (context) => Container(
            decoration: BoxDecoration(
              color: AppColor.kLightColor2,
              borderRadius: BorderRadius.vertical(
                top: Radius.circular(30.r),
              ),
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Container(
                  padding:
                      EdgeInsets.symmetric(vertical: 25.h, horizontal: 22.w),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      // Total order tile
                      TileOption(
                        title: 'Total orders'.tr,
                        subtitle:
                            '(${DetailOrderController.to.order.value?.menu.length} Menu):',
                        message: DetailOrderController
                            .to.order.value!.total.toRupiah,
                        titleStyle: Get.textTheme.headlineSmall,
                        messageStyle: Get.textTheme.labelLarge!
                            .copyWith(color: AppColor.kBlueColor),
                      ),
                      Divider(color: AppColor.kDarkColor5, height: 2.h),

                      // Discount tile
                      Conditional.single(
                        context: context,
                        conditionBuilder: (context) =>
                            DetailOrderController.to.order.value!.diskon == 1 &&
                            DetailOrderController.to.order.value!.potongan > 0,
                        widgetBuilder: (context) => TileOption(
                          icon: AssetConstants.iconDiscount,
                          iconSize: 24.r,
                          title: 'Discount'.tr,
                          message: DetailOrderController
                              .to.order.value!.potongan.toRupiah,
                          titleStyle: Get.textTheme.headlineSmall,
                          messageStyle: Get.textTheme.bodySmall
                              ?.copyWith(color: AppColor.kRedColor),
                        ),
                        fallbackBuilder: (context) => const SizedBox(),
                      ),
                      Divider(color: AppColor.kDarkColor5, height: 2.h),

                      // Vouchers tile
                      Conditional.single(
                        context: context,
                        conditionBuilder: (context) =>
                            DetailOrderController.to.order.value!.idVoucher !=
                            0,
                        widgetBuilder: (context) => TileOption(
                          icon: AssetConstants.iconVoucher,
                          iconSize: 24.r,
                          title: 'voucher'.tr,
                          message: DetailOrderController
                              .to.order.value!.potongan.toRupiah,
                          messageSubtitle:
                              DetailOrderController.to.order.value?.namaVoucher,
                          titleStyle: Get.textTheme.headlineSmall,
                          messageStyle: Get.textTheme.bodySmall
                              ?.copyWith(color: AppColor.kRedColor),
                        ),
                        fallbackBuilder: (context) => const SizedBox(),
                      ),
                      Divider(color: AppColor.kDarkColor5, height: 2.h),

                      // Payment options tile
                      TileOption(
                        icon: AssetConstants.iconPayment,
                        iconSize: 24.r,
                        title: 'Payment'.tr,
                        message: 'Pay Later',
                        titleStyle: Get.textTheme.headlineSmall,
                        messageStyle: Get.textTheme.bodySmall,
                      ),

                      Divider(color: AppColor.kDarkColor5, height: 2.h),

                      // total payment
                      TileOption(
                        iconSize: 24.r,
                        title: 'Total payment'.tr,
                        message: DetailOrderController
                            .to.order.value!.totalBayar.toRupiah,
                        titleStyle: Get.textTheme.headlineSmall,
                        messageStyle: Get.textTheme.headlineSmall!
                            .copyWith(color: AppColor.kBlueColor),
                      ),
                      Divider(color: AppColor.kDarkColor5, height: 2.h),
                      24.verticalSpace,

                      // order status track
                      const OrderTracker(),
                    ],
                  ),
                ),
              ],
            ),
          ),
          fallbackBuilder: (context) => const SizedBox(),
        ),
      ),
    );
  }
}
