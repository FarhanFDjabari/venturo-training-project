import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:java_code_onboarding/config/themes/colors.dart';
import 'package:java_code_onboarding/modules/features/order/view/ui/on_going_order_tabview.dart';
import 'package:java_code_onboarding/modules/features/order/view/ui/order_history_tabview.dart';

class OrderView extends StatelessWidget {
  const OrderView({super.key});

  @override
  Widget build(BuildContext context) {
    return const DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: OrderTopBar(),
        body: TabBarView(
          children: [
            OnGoingOrderTabView(),
            OrderHistoryTabView(),
          ],
        ),
      ),
    );
  }
}

class OrderTopBar extends StatelessWidget implements PreferredSizeWidget {
  const OrderTopBar({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: AppColor.kLightColor,
        borderRadius: BorderRadius.vertical(
          bottom: Radius.circular(30.r),
        ),
        boxShadow: const [
          BoxShadow(
            color: AppColor.kDarkColor5,
            blurRadius: 10,
            spreadRadius: -1,
            offset: Offset(0, 2),
          ),
        ],
      ),
      padding: EdgeInsets.symmetric(horizontal: 20.w),
      child: TabBar(
        tabs: [
          Tab(text: 'On going'.tr),
          Tab(text: 'History'.tr),
        ],
        indicatorColor: AppColor.kBlueColor,
        indicatorWeight: 3.h,
        labelColor: AppColor.kBlueColor,
        unselectedLabelColor: AppColor.kDarkColor,
        labelStyle: Get.textTheme.bodyLarge?.copyWith(
          fontWeight: FontWeight.w600,
        ),
        indicatorPadding: EdgeInsets.symmetric(horizontal: 70.w),
      ),
    );
  }

  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);
}
