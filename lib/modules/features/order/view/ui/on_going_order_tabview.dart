import 'package:flutter/material.dart';
import 'package:flutter_conditional_rendering/flutter_conditional_rendering.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:java_code_onboarding/config/routes/routes.dart';
import 'package:java_code_onboarding/modules/features/order/controllers/order_controller.dart';
import 'package:java_code_onboarding/modules/features/order/view/components/order_item_card.dart';
import 'package:java_code_onboarding/shared/widgets/order_data_empty.dart';
import 'package:java_code_onboarding/shared/widgets/rect_shimmer.dart';
import 'package:java_code_onboarding/shared/widgets/server_error_list_view.dart';
import 'package:java_code_onboarding/utils/enums/ongoing_order_state.dart';

class OnGoingOrderTabView extends StatelessWidget {
  const OnGoingOrderTabView({super.key});

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh: () async => OrderController.to.getOngoingOrders(),
      child: Obx(
        () => ConditionalSwitch.single(
          context: context,
          valueBuilder: (context) => OrderController.to.onGoingOrderState.value,
          caseBuilders: {
            OnGoingOrderState.Loading: (context) => ListView.separated(
                  padding: EdgeInsets.symmetric(
                    horizontal: 25.w,
                    vertical: 25.h,
                  ),
                  separatorBuilder: (_, i) => 16.verticalSpace,
                  itemCount: 5,
                  itemBuilder: (_, i) => AspectRatio(
                    aspectRatio: 378 / 138,
                    child: RectShimmer(radius: 10.r),
                  ),
                ),
            OnGoingOrderState.Empty: (context) => OrderDataEmpty(
                  title: 'Already Ordered? Track the order here.'.tr,
                ),
            OnGoingOrderState.Error: (context) => const ServerErrorListView(),
          },
          fallbackBuilder: (context) => ListView.separated(
            padding: EdgeInsets.all(25.r),
            itemBuilder: (context, index) => OrderItemCard(
              order: OrderController.to.onGoingOrders[index],
              onTap: () {
                Get.toNamed(
                  '${AppRoutes.orderView}/${OrderController.to.onGoingOrders[index].idOrder}',
                );
              },
              onOrderAgain: () {},
            ),
            separatorBuilder: (context, index) => 16.verticalSpace,
            itemCount: OrderController.to.onGoingOrders.length,
          ),
        ),
      ),
    );
  }
}
