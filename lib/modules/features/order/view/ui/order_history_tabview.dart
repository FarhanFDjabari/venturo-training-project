import 'package:flutter/material.dart';
import 'package:flutter_conditional_rendering/flutter_conditional_rendering.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:java_code_onboarding/config/routes/routes.dart';
import 'package:java_code_onboarding/config/themes/colors.dart';
import 'package:java_code_onboarding/modules/features/order/controllers/order_controller.dart';
import 'package:java_code_onboarding/modules/features/order/view/components/date_picker.dart';
import 'package:java_code_onboarding/modules/features/order/view/components/dropdown_status.dart';
import 'package:java_code_onboarding/modules/features/order/view/components/order_item_card.dart';
import 'package:java_code_onboarding/shared/widgets/empty_data_view.dart';
import 'package:java_code_onboarding/shared/widgets/order_data_empty.dart';
import 'package:java_code_onboarding/shared/widgets/rect_shimmer.dart';
import 'package:java_code_onboarding/shared/widgets/server_error_list_view.dart';
import 'package:java_code_onboarding/utils/enums/order_history_state.dart';

class OrderHistoryTabView extends StatelessWidget {
  const OrderHistoryTabView({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: RefreshIndicator(
        onRefresh: OrderController.to.getOrderHistories,
        child: Obx(
          () => ConditionalSwitch.single(
            context: context,
            valueBuilder: (context) =>
                OrderController.to.orderHistoryState.value,
            caseBuilders: {
              OrderHistoryState.Loading: (context) => ListView.separated(
                    padding: EdgeInsets.symmetric(
                      horizontal: 25.w,
                      vertical: 25.h,
                    ),
                    separatorBuilder: (_, i) => 16.verticalSpace,
                    itemCount: 5,
                    itemBuilder: (_, i) => AspectRatio(
                      aspectRatio: 378 / 138,
                      child: RectShimmer(radius: 10.r),
                    ),
                  ),
              OrderHistoryState.Empty: (context) => OrderDataEmpty(
                    title: 'Start placing orders.'.tr,
                    subtitle:
                        'The food you ordered will appear here so you can find your favorite menu again!'
                            .tr,
                  ),
              OrderHistoryState.Error: (context) => const ServerErrorListView(),
            },
            fallbackBuilder: (context) => CustomScrollView(
              slivers: [
                SliverToBoxAdapter(
                  child: Padding(
                    padding:
                        EdgeInsets.symmetric(horizontal: 25.w, vertical: 25.h),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(
                          child: DropdownStatus(
                            items: OrderController.to.dateFilterStatus,
                            selectedItem:
                                OrderController.to.selectedCategory.value,
                            onChanged: (value) => OrderController.to
                                .setDateFilter(category: value),
                          ),
                        ),
                        22.horizontalSpaceRadius,
                        Expanded(
                          child: DatePicker(
                            selectedDate:
                                OrderController.to.selectedDateRange.value,
                            onChanged: (value) =>
                                OrderController.to.setDateFilter(range: value),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Conditional.single(
                  context: context,
                  conditionBuilder: (context) =>
                      OrderController.to.filteredHistoryOrder.isNotEmpty,
                  widgetBuilder: (context) => SliverPadding(
                    padding: EdgeInsets.symmetric(horizontal: 25.w),
                    sliver: SliverList(
                      delegate: SliverChildBuilderDelegate(
                        (context, index) => Padding(
                          padding: EdgeInsets.only(bottom: 16.r),
                          child: OrderItemCard(
                            order:
                                OrderController.to.filteredHistoryOrder[index],
                            onOrderAgain: () => OrderController.to
                                .onRepeatOrder(OrderController
                                    .to.filteredHistoryOrder[index]),
                            onTap: () => Get.toNamed(
                              '${AppRoutes.orderView}/${OrderController.to.filteredHistoryOrder[index].idOrder}',
                            ),
                          ),
                        ),
                        childCount:
                            OrderController.to.filteredHistoryOrder.length,
                      ),
                    ),
                  ),
                  fallbackBuilder: (context) => const SliverToBoxAdapter(
                    child: EmptyDataView(),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
      bottomNavigationBar: Container(
        width: 1.sw,
        height: 56.h,
        padding: EdgeInsets.symmetric(horizontal: 22.w, vertical: 15.h),
        decoration: BoxDecoration(
            color: AppColor.kLightColor,
            borderRadius: BorderRadius.vertical(
              top: Radius.circular(30.r),
            )),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              'Total orders'.tr,
              style: Get.textTheme.headlineSmall?.copyWith(
                fontWeight: FontWeight.w700,
              ),
            ),
            5.horizontalSpace,
            Obx(() => Text(
                  OrderController.to.totalHistoryOrder,
                  style: Get.textTheme.headlineSmall?.copyWith(
                    fontWeight: FontWeight.w700,
                    color: AppColor.kBlueColor,
                  ),
                )),
          ],
        ),
      ),
    );
  }
}
