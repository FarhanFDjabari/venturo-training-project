import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:java_code_onboarding/config/themes/colors.dart';
import 'package:java_code_onboarding/constant/common/constants.dart';
import 'package:java_code_onboarding/constant/core/asset_const.dart';
import 'package:java_code_onboarding/modules/features/home/view/components/quantity_counter.dart';
import 'package:java_code_onboarding/modules/models/detail_order.dart';
import 'package:java_code_onboarding/utils/extensions/currency_ext.dart';

class DetailOrderCard extends StatelessWidget {
  final DetailOrder detailOrder;

  const DetailOrderCard(this.detailOrder, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Ink(
      padding: EdgeInsets.all(7.r),
      decoration: BoxDecoration(
        color: AppColor.kLightColor2,
        borderRadius: BorderRadius.circular(10.r),
        boxShadow: const [
          BoxShadow(
            color: AppColor.kDarkColor8,
            offset: Offset(0, 2),
            blurRadius: 8,
            spreadRadius: -1,
          ),
        ],
      ),
      child: Row(
        children: [
          /* Image */
          Container(
            height: 90.h,
            width: 90.w,
            margin: EdgeInsets.only(right: 12.w),
            padding: EdgeInsets.all(5.r),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10.r),
              color: AppColor.kLightColor,
            ),
            child: CachedNetworkImage(
              imageUrl: detailOrder.foto ?? AppConstants.defaultMenuPhoto,
              fit: BoxFit.contain,
              errorWidget: (context, _, __) => CachedNetworkImage(
                imageUrl: AppConstants.defaultMenuPhoto,
                fit: BoxFit.contain,
              ),
            ),
          ),
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  detailOrder.nama,
                  style: Get.textTheme.titleMedium,
                  overflow: TextOverflow.ellipsis,
                  maxLines: 1,
                ),
                Text(
                  int.parse(detailOrder.harga).toRupiah,
                  style: Get.textTheme.bodyMedium!.copyWith(
                      color: AppColor.kBlueColor, fontWeight: FontWeight.bold),
                ),
                5.verticalSpacingRadius,
                Row(
                  children: [
                    SvgPicture.asset(AssetConstants.iconEdit, height: 12.h),
                    7.horizontalSpaceRadius,
                    Expanded(
                      child: TextFormField(
                        initialValue: detailOrder.catatan,
                        style: Get.textTheme.labelMedium,
                        enabled: false,
                        decoration: InputDecoration.collapsed(
                          hintText: 'No notes'.tr,
                          border: InputBorder.none,
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          Container(
            height: 75.r,
            padding: EdgeInsets.only(left: 12.w, right: 5.w),
            child: QuantityCounter(quantity: detailOrder.jumlah),
          ),
        ],
      ),
    );
  }
}
