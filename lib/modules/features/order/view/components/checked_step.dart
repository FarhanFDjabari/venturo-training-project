import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:java_code_onboarding/config/themes/colors.dart';

class CheckedStep extends StatelessWidget {
  const CheckedStep({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.zero,
      decoration: const BoxDecoration(
        color: AppColor.kBlueColor,
        shape: BoxShape.circle,
        boxShadow: [
          BoxShadow(
            offset: Offset(0, 2),
            blurRadius: 8,
            spreadRadius: -1,
            color: AppColor.kDarkColor5,
          ),
        ],
      ),
      child: Icon(
        Icons.check,
        color: AppColor.kLightColor,
        size: 30.r,
      ),
    );
  }
}
