import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:java_code_onboarding/modules/features/order/view/components/detail_order_card.dart';
import 'package:java_code_onboarding/modules/models/detail_order.dart';

class OrderListSliver extends StatelessWidget {
  const OrderListSliver({
    super.key,
    required this.orders,
  });

  final List<DetailOrder> orders;

  @override
  Widget build(BuildContext context) {
    return SliverFixedExtentList(
      delegate: SliverChildBuilderDelegate(
        (context, index) {
          return Padding(
            padding: EdgeInsets.symmetric(vertical: 8.5.h),
            child: DetailOrderCard(
              orders[index],
            ),
          );
        },
        childCount: orders.length,
      ),
      itemExtent: 112.h,
    );
  }
}
