import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_conditional_rendering/flutter_conditional_rendering.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:java_code_onboarding/config/localization/localization.dart';
import 'package:java_code_onboarding/config/themes/colors.dart';
import 'package:java_code_onboarding/constant/common/constants.dart';
import 'package:java_code_onboarding/modules/models/order.dart';
import 'package:java_code_onboarding/shared/widgets/outlined_title_button.dart';
import 'package:java_code_onboarding/shared/widgets/primary_button_with_title.dart';
import 'package:java_code_onboarding/utils/extensions/currency_ext.dart';

class OrderItemCard extends StatelessWidget {
  const OrderItemCard({
    super.key,
    required this.order,
    this.onTap,
    this.onOrderAgain,
    this.onGiveReview,
  });

  final Order order;
  final VoidCallback? onTap;
  final VoidCallback? onOrderAgain;
  final ValueChanged<int>? onGiveReview;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      borderRadius: BorderRadius.circular(10.r),
      child: Ink(
        padding: EdgeInsets.all(7.r),
        decoration: BoxDecoration(
          color: AppColor.kLightColor5,
          borderRadius: BorderRadius.circular(10.r),
          boxShadow: const [
            BoxShadow(
              offset: Offset(0, 2),
              blurRadius: 8,
              spreadRadius: -1,
              color: AppColor.kDarkColor6,
            ),
          ],
        ),
        child: IntrinsicHeight(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Flexible(
                flex: 3,
                child: Hero(
                  tag: 'order-${order.idOrder}',
                  child: Container(
                    width: 124.w,
                    constraints: BoxConstraints(
                      minHeight: 124.h,
                      maxWidth: 124.w,
                    ),
                    padding: EdgeInsets.all(10.r),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10.r),
                      color: AppColor.kLightColor3,
                    ),
                    child: CachedNetworkImage(
                      imageUrl: order.menu.isNotEmpty
                          ? order.menu.first.foto ??
                              AppConstants.defaultMenuPhoto
                          : AppConstants.defaultMenuPhoto,
                      fit: BoxFit.contain,
                      height: 75.h,
                      width: 75.w,
                      errorWidget: (context, _, __) => CachedNetworkImage(
                        imageUrl: AppConstants.defaultMenuPhoto,
                        fit: BoxFit.contain,
                        height: 75.h,
                        width: 75.w,
                      ),
                    ),
                  ),
                ),
              ),
              12.horizontalSpace,
              Flexible(
                flex: 8,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    5.verticalSpace,

                    // order status
                    Row(
                      children: [
                        ConditionalSwitch.single<int>(
                          context: context,
                          valueBuilder: (context) => order.status,
                          caseBuilders: {
                            0: (context) => Icon(
                                  Icons.access_time,
                                  color: AppColor.kYellowColor,
                                  size: 16.r,
                                ),
                            1: (context) => Icon(
                                  Icons.access_time,
                                  color: AppColor.kYellowColor,
                                  size: 16.r,
                                ),
                            2: (context) => Icon(
                                  Icons.access_time,
                                  color: AppColor.kYellowColor,
                                  size: 16.r,
                                ),
                            3: (context) => Icon(
                                  Icons.check,
                                  color: AppColor.kGreenColor,
                                  size: 16.r,
                                ),
                            4: (context) => Icon(
                                  Icons.close,
                                  color: AppColor.kRedColor,
                                  size: 16.r,
                                ),
                          },
                          fallbackBuilder: (context) => const SizedBox(),
                        ),
                        5.horizontalSpaceRadius,
                        Expanded(
                          child: ConditionalSwitch.single<int>(
                            context: context,
                            valueBuilder: (context) => order.status,
                            caseBuilders: {
                              0: (context) => Text(
                                    'In queue'.tr,
                                    style: Get.textTheme.labelMedium!
                                        .copyWith(color: AppColor.kYellowColor),
                                  ),
                              1: (context) => Text(
                                    'Preparing'.tr,
                                    style: Get.textTheme.labelMedium!
                                        .copyWith(color: AppColor.kYellowColor),
                                  ),
                              2: (context) => Text(
                                    'Ready'.tr,
                                    style: Get.textTheme.labelMedium!
                                        .copyWith(color: AppColor.kYellowColor),
                                  ),
                              3: (context) => Text(
                                    'Completed'.tr,
                                    style: Get.textTheme.labelMedium!
                                        .copyWith(color: AppColor.kGreenColor),
                                  ),
                              4: (context) => Text(
                                    'Canceled'.tr,
                                    style: Get.textTheme.labelMedium!
                                        .copyWith(color: AppColor.kRedColor),
                                  ),
                            },
                            fallbackBuilder: (context) => const SizedBox(),
                          ),
                        ),
                        Text(
                          DateFormat(
                            'dd MMMM yyyy',
                            Localization.currentLocale.languageCode,
                          ).format(order.tanggal),
                          style: Get.textTheme.labelMedium!
                              .copyWith(color: AppColor.kLightColor4),
                        ),
                      ],
                    ),
                    14.verticalSpace,

                    // Menu title
                    Text(
                      order.menu.map((e) => e.nama).join(', '),
                      style: Get.textTheme.bodyMedium,
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      textAlign: TextAlign.left,
                    ),
                    5.verticalSpace,

                    // Price
                    Row(
                      children: [
                        Text(
                          order.totalBayar.toRupiah,
                          style: Get.textTheme.labelLarge!
                              .copyWith(color: AppColor.kBlueColor),
                        ),
                        5.horizontalSpace,
                        Text(
                          '(${order.menu.length} Menu)',
                          style: Get.textTheme.labelLarge!
                              .copyWith(color: AppColor.kLightColor4),
                        ),
                      ],
                    ),

                    // Action Button
                    Conditional.single(
                      context: context,
                      conditionBuilder: (context) =>
                          order.status == 3 || order.status == 4,
                      widgetBuilder: (context) => Wrap(
                        spacing: 15.r,
                        children: [
                          if (order.status == 3)
                            Padding(
                              padding: EdgeInsets.only(top: 10.r, bottom: 5.r),
                              child: OutlinedTitleButton.compact(
                                text: 'Give review'.tr,
                                onPressed: () =>
                                    onGiveReview?.call(order.idOrder),
                              ),
                            ),
                          Padding(
                            padding: EdgeInsets.only(
                                top: 10.r,
                                bottom: 5.r,
                                right: order.status == 3 ? 0.r : 0.3.sw),
                            child: PrimaryButtonWithTitle.compact(
                              title: 'Order again'.tr,
                              onPressed: onOrderAgain,
                            ),
                          ),
                        ],
                      ),
                      fallbackBuilder: (context) => const SizedBox(),
                    ),
                  ],
                ),
              ),
              5.horizontalSpace,
            ],
          ),
        ),
      ),
    );
  }
}
