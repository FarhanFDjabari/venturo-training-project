import 'package:get/get.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:java_code_onboarding/config/routes/routes.dart';
import 'package:java_code_onboarding/modules/features/login/repositories/login_repository.dart';
import 'package:java_code_onboarding/modules/models/user.dart';
import 'package:java_code_onboarding/utils/services/local_db_services.dart';

import '../../../../shared/customs/error_snackbar.dart';

class LoginController extends GetxController {
  static LoginController get to => Get.find<LoginController>();

  final isObscure = true.obs;
  final isLoading = false.obs;

  Future<void> loginWithEmailPassword(String email, String password) async {
    isLoading(true);
    UserRes userRes = await LoginRepository.loginUser(email, password);

    isLoading(false);
    if (userRes.statusCode == 200) {
      // save auth data to local db
      if (userRes.user != null) {
        await LocalDBServices.setUser(userRes.user!);
      }
      await LocalDBServices.setUserToken(userRes.token ?? '');

      // Go to dashboard page
      Get.offAllNamed(AppRoutes.dashboardView);
    } else {
      Get.showSnackbar(ErrorSnackBar(
        title: 'Error'.tr,
        message: '${userRes.errors?.first ?? userRes.message}',
      ));
    }
  }

  Future<void> loginWithGoogle() async {
    // google sign in instance
    final GoogleSignIn googleSignIn = GoogleSignIn();

    await googleSignIn.signOut();

    // obtain auth data
    final GoogleSignInAccount? googleUser = await googleSignIn.signIn();

    if (googleUser == null) return;

    UserRes userRes = await LoginRepository.loginUserGoogle(
      googleUser.displayName ?? '',
      googleUser.email,
    );

    if (userRes.statusCode >= 200 && userRes.statusCode < 300) {
      // save auth data to local db
      if (userRes.user != null) {
        await LocalDBServices.setUser(userRes.user!);
      }
      await LocalDBServices.setUserToken(userRes.token ?? '');

      // Go to dashboard page
      Get.offAllNamed(AppRoutes.dashboardView);
    } else {
      Get.showSnackbar(ErrorSnackBar(
        title: 'Error'.tr,
        message: '${userRes.errors?.first}',
      ));
    }
  }
}
