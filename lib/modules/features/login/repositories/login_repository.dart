import 'package:dio/dio.dart';
import 'package:get/get.dart';
import 'package:java_code_onboarding/constant/core/api_const.dart';
import 'package:java_code_onboarding/modules/models/user.dart';
import 'package:java_code_onboarding/utils/services/api_services.dart';

class LoginRepository {
  LoginRepository._();

  static final Dio _dio = ApiServices.dioCall();

  static Future<UserRes> loginUser(String email, String password) async {
    try {
      final response = await _dio.post(ApiConstants.login, data: {
        'email': email,
        'password': password,
      });

      return UserRes.fromLoginJson(response.data);
    } on DioError {
      return UserRes(statusCode: 500, message: 'Server error'.tr);
    }
  }

  static Future<UserRes> loginUserGoogle(String name, String email) async {
    try {
      var response = await _dio.post(ApiConstants.login, data: {
        'is_google': 'is_google',
        'nama': name,
        'email': email,
      });

      return UserRes.fromLoginJson(response.data);
    } on DioError {
      return UserRes(statusCode: 500, message: 'Server error'.tr);
    }
  }
}
