import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:java_code_onboarding/config/themes/colors.dart';
import 'package:java_code_onboarding/constant/core/asset_const.dart';
import 'package:java_code_onboarding/modules/features/login/controllers/login_controller.dart';
import 'package:java_code_onboarding/shared/widgets/horizontal_divider.dart';

import '../components/email_text_form_field.dart';
import '../components/password_text_form_field.dart';
import '../../../../../shared/widgets/primary_button_with_title.dart';
import '../../../../../shared/widgets/primary_button_with_title_icon.dart';

class LoginView extends StatelessWidget {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  LoginView({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 46.r),
            child: Form(
              key: _formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  80.verticalSpacingRadius,
                  Align(
                    alignment: Alignment.center,
                    child: Hero(
                      tag: AssetConstants.iconJavaCode,
                      child: Image.asset(
                        AssetConstants.iconJavaCode,
                        width: 270.w,
                        height: 91.h,
                      ),
                    ),
                  ),
                  121.verticalSpacingRadius,
                  Text(
                    'Continue to sign in'.tr,
                    style: Get.textTheme.headlineMedium,
                  ),
                  40.verticalSpacingRadius,
                  Text(
                    'Email'.tr,
                    style: Get.textTheme.bodySmall,
                  ),
                  5.verticalSpacingRadius,
                  EmailTextFormField(
                    emailController: _emailController,
                    validator: (value) {
                      if (!GetUtils.isEmail(value ?? '')) {
                        return 'Email is not valid'.tr;
                      }
                      return null;
                    },
                  ),
                  40.verticalSpacingRadius,
                  Text(
                    'Password'.tr,
                    style: Get.textTheme.bodySmall,
                  ),
                  5.verticalSpacingRadius,
                  Obx(
                    () => PasswordTextFormField(
                      passwordController: _passwordController,
                      isObscure: LoginController.to.isObscure.value,
                      onSuffixIconTapped: () {
                        LoginController.to.isObscure.toggle();
                      },
                      validator: (value) {
                        if (value?.isEmpty == true) {
                          return 'Password is required'.tr;
                        }
                        return null;
                      },
                    ),
                  ),
                  40.verticalSpacingRadius,
                  Obx(() => PrimaryButtonWithTitle(
                        onPressed: () {
                          if (_formKey.currentState?.validate() == true &&
                              LoginController.to.isLoading.isFalse) {
                            // login logic
                            LoginController.to.loginWithEmailPassword(
                                _emailController.text.trim(),
                                _passwordController.text.trim());
                          }
                        },
                        title: 'Login'.tr,
                        isLoading: LoginController.to.isLoading.value,
                      )),
                  77.verticalSpacingRadius,
                  const HorizontalOrDivider(),
                  9.verticalSpacingRadius,
                  Obx(() => PrimaryButtonWithTitleIcon(
                        onPressed: LoginController.to.isLoading.isFalse
                            ? () {
                                LoginController.to.loginWithGoogle();
                              }
                            : null,
                        iconWidget: SvgPicture.asset(
                          AssetConstants.iconGoogle,
                        ),
                        title: 'Login with'.tr,
                        additionalTitle: ' Google',
                      )),
                  17.verticalSpacingRadius,
                  Obx(() => PrimaryButtonWithTitleIcon(
                        onPressed:
                            LoginController.to.isLoading.isFalse ? () {} : null,
                        iconWidget: SvgPicture.asset(
                          AssetConstants.iconApple,
                        ),
                        title: 'Login with'.tr,
                        additionalTitle: ' Apple',
                        backgroundColor: AppColor.kDarkColor,
                        titleColor: AppColor.kLightColor,
                      )),
                  84.verticalSpacingRadius,
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
