import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:java_code_onboarding/utils/extensions/string_ext.dart';

import '../../../../../config/themes/colors.dart';

class PasswordTextFormField extends StatelessWidget {
  const PasswordTextFormField({
    super.key,
    required TextEditingController passwordController,
    this.isObscure = true,
    this.onSuffixIconTapped,
    this.validator,
  }) : _passwordController = passwordController;

  final TextEditingController _passwordController;
  final bool isObscure;
  final VoidCallback? onSuffixIconTapped;
  final String? Function(String?)? validator;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      cursorColor: AppColor.kBlueColor,
      controller: _passwordController,
      obscureText: isObscure,
      keyboardType: TextInputType.visiblePassword,
      textInputAction: TextInputAction.done,
      style: Get.textTheme.bodySmall,
      decoration: InputDecoration(
        hintText: 'Password'.toObscured,
        hintStyle: Get.textTheme.bodySmall?.copyWith(
          color: AppColor.kDarkColor4,
        ),
        isDense: true,
        enabledBorder: const UnderlineInputBorder(
          borderSide: BorderSide(
            color: AppColor.kBlueColor,
          ),
        ),
        focusedBorder: const UnderlineInputBorder(
          borderSide: BorderSide(
            color: AppColor.kBlueColor,
          ),
        ),
        suffixIcon: InkWell(
          onTap: onSuffixIconTapped,
          child: Icon(isObscure
              ? Icons.visibility_off_outlined
              : Icons.visibility_outlined),
        ),
        suffixIconConstraints: BoxConstraints(
          minWidth: 24.w,
        ),
      ),
      validator: validator,
    );
  }
}
