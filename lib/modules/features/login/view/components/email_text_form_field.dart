import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../../../../../config/themes/colors.dart';

class EmailTextFormField extends StatelessWidget {
  const EmailTextFormField({
    super.key,
    required TextEditingController emailController,
    this.validator,
  }) : _emailController = emailController;

  final TextEditingController _emailController;
  final String? Function(String?)? validator;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      cursorColor: AppColor.kBlueColor,
      controller: _emailController,
      keyboardType: TextInputType.emailAddress,
      textInputAction: TextInputAction.next,
      style: Get.textTheme.bodySmall,
      decoration: InputDecoration(
        hintText: 'lorem.ipsum@gmail.com',
        isDense: true,
        hintStyle: Get.textTheme.bodySmall?.copyWith(
          color: AppColor.kDarkColor4,
        ),
        enabledBorder: const UnderlineInputBorder(
          borderSide: BorderSide(
            color: AppColor.kBlueColor,
          ),
        ),
        focusedBorder: const UnderlineInputBorder(
          borderSide: BorderSide(
            color: AppColor.kBlueColor,
          ),
        ),
      ),
      validator: validator,
    );
  }
}
