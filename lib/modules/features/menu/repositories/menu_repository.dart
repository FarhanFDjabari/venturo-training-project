import 'package:dio/dio.dart';
import 'package:get/get.dart';
import 'package:java_code_onboarding/constant/core/api_const.dart';
import 'package:java_code_onboarding/modules/models/menu.dart';
import 'package:java_code_onboarding/utils/services/api_services.dart';
import 'package:java_code_onboarding/utils/services/local_db_services.dart';

class MenuRepository {
  MenuRepository._();

  // get menu detail by id
  static Future<MenuRes> getMenuDetail(int id) async {
    try {
      final userToken = await LocalDBServices.getUserToken();
      final dio = ApiServices.dioCall(token: userToken);
      final response = await dio.get('${ApiConstants.menuDetail}/$id');

      return MenuRes.fromJson(response.data);
    } on DioError {
      return MenuRes(statusCode: 500, message: 'Server error'.tr);
    }
  }
}
