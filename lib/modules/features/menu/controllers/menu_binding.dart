import 'package:get/get.dart';
import 'package:java_code_onboarding/modules/features/menu/controllers/detail_menu_controller.dart';

class MenuBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => DetailMenuController());
  }
}
