import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:java_code_onboarding/config/routes/routes.dart';
import 'package:java_code_onboarding/config/themes/colors.dart';
import 'package:java_code_onboarding/modules/features/cart/controllers/cart_controller.dart';
import 'package:java_code_onboarding/shared/widgets/confirmation_dialog.dart';
import 'package:java_code_onboarding/modules/features/menu/repositories/menu_repository.dart';
import 'package:java_code_onboarding/modules/features/menu/view/components/level_bottom_sheet.dart';
import 'package:java_code_onboarding/modules/features/menu/view/components/note_bottom_sheet.dart';
import 'package:java_code_onboarding/modules/features/menu/view/components/topping_bottom_sheet.dart';
import 'package:java_code_onboarding/modules/models/cart_item.dart';
import 'package:java_code_onboarding/modules/models/menu.dart';
import 'package:java_code_onboarding/utils/enums/detail_menu_level_state.dart';
import 'package:java_code_onboarding/utils/enums/detail_menu_topping_state.dart';
import 'package:java_code_onboarding/utils/enums/menu_detail_state.dart';

class DetailMenuController extends GetxController {
  static DetailMenuController get to => Get.find<DetailMenuController>();

  // menu state
  Rx<MenuDetailState> menuDetailState = MenuDetailState.Loading.obs;
  Rx<DetailMenuLevelState> detailMenuLevelState =
      DetailMenuLevelState.Loading.obs;
  Rx<DetailMenuToppingState> detailMenuToppingState =
      DetailMenuToppingState.Loading.obs;

  // menu data
  RxList<MenuVariant> levels = <MenuVariant>[].obs;
  RxList<MenuVariant> toppings = <MenuVariant>[].obs;
  RxString errorMessage = ''.obs;
  Rxn<Menu> menu = Rxn<Menu>();
  RxBool isAlreadyExist = false.obs;

  // order data
  RxInt quantity = 0.obs;
  Rxn<MenuVariant> selectedLevel = Rxn<MenuVariant>();
  RxList<MenuVariant> selectedToppings = <MenuVariant>[].obs;
  RxString note = ''.obs;

  @override
  void onInit() async {
    super.onInit();

    await getMenuDetail(int.parse(Get.parameters['menuId'] as String));

    await updateDataFromCart();
  }

  Future<void> getMenuDetail(int menuId) async {
    menuDetailState(MenuDetailState.Loading);

    final result = await MenuRepository.getMenuDetail(menuId);
    menuDetailState(MenuDetailState.Success);

    if (result.statusCode >= 200 && result.statusCode < 300) {
      detailMenuLevelState(DetailMenuLevelState.Success);

      if (result.level.isNotEmpty) {
        detailMenuLevelState(DetailMenuLevelState.Success);
        levels(result.level);
        selectedLevel(levels.first);
      } else {
        detailMenuLevelState(DetailMenuLevelState.Empty);
      }

      if (result.topping.isNotEmpty) {
        detailMenuToppingState(DetailMenuToppingState.Success);
        toppings(result.level);
      } else {
        detailMenuToppingState(DetailMenuToppingState.Empty);
      }

      menu(result.data);
    } else {
      detailMenuLevelState(DetailMenuLevelState.Error);
      detailMenuToppingState(DetailMenuToppingState.Error);
      menuDetailState(MenuDetailState.Error);
      errorMessage(result.errors?.first);
    }
  }

  Future<void> updateDataFromCart() async {
    // check if menu already exist in cart
    final cartDetail = CartController.to.cart.firstWhereOrNull(
        (element) => element.menu.idMenu == menu.value?.idMenu);

    // if exist, update data
    if (cartDetail != null) {
      isAlreadyExist(true);
      selectedLevel(cartDetail.level);
      note(cartDetail.note);
      selectedToppings(cartDetail.toppings);
      quantity(cartDetail.quantity);
    }
  }

  void onAddQty() {
    quantity.value++;
  }

  void onRemoveQty() async {
    if (quantity > 0) {
      if (quantity.value == 1 && isAlreadyExist.value == true) {
        // show confirm dialog to delete from cart
        final result = await Get.defaultDialog(
          title: '',
          titleStyle: const TextStyle(fontSize: 0),
          content: ConfirmationDialog(
            title: 'Are you sure want to delete this menu from cart?'.tr,
          ),
        );

        if (result == true) {
          deleteFromCart();
          quantity.value--;
          isAlreadyExist(false);
        }
      } else {
        quantity.value--;
      }
    }
  }

  void showNoteBottomSheet() {
    Get.bottomSheet(
      NoteBottomSheet(),
      backgroundColor: AppColor.kLightColor,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(30.r),
        ),
      ),
      isScrollControlled: true,
    );
  }

  void showLevelBottomSheet() {
    Get.bottomSheet(
      const LevelBottomSheet(),
      backgroundColor: AppColor.kLightColor,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(30.r),
        ),
      ),
      isScrollControlled: true,
    );
  }

  void showToppingsBottomSheet() {
    Get.bottomSheet(
      const ToppingBottomSheet(),
      backgroundColor: AppColor.kLightColor,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(30.r),
        ),
      ),
      isScrollControlled: true,
    );
  }

  void setNote(String value) {
    note(value.trim());
    if (Get.isBottomSheetOpen == true) Get.back();
  }

  void setLevel(MenuVariant value) {
    selectedLevel(value);
    if (Get.isBottomSheetOpen == true) Get.back();
  }

  void toggleToppings(MenuVariant value) {
    if (selectedToppings.contains(value)) {
      // already selected, remove
      selectedToppings.remove(value);
    } else {
      // not selected, add
      selectedToppings.add(value);
    }

    // alphabetically sort list
    selectedToppings.sort((a, b) => a.keterangan.compareTo(b.keterangan));
  }

  String get selectedToppingText => selectedToppings.isNotEmpty
      ? selectedToppings.map((e) => e.keterangan).join(', ')
      : 'Choose topping'.tr;

  String get selectedLevelText => selectedLevel.value?.keterangan ?? '-';

  CartItem? get cartItem => menu.value != null
      ? CartItem(
          menu: menu.value!,
          quantity: quantity.value,
          note: note.value,
          level: selectedLevel.value,
          toppings: toppings.isEmpty ? null : selectedToppings.toList())
      : null;

  // add menu to cart
  void addToCart() {
    if (menuDetailState.value == MenuDetailState.Success &&
        (selectedLevel.value != null || levels.isEmpty)) {
      if (cartItem != null) {
        CartController.to.addToCart(cartItem!);
        Get.offNamedUntil(
          AppRoutes.cartView,
          ModalRoute.withName(AppRoutes.dashboardView),
        );
      }
    }
  }

  // delete cart item from cart
  void deleteFromCart() {
    if (cartItem != null) {
      CartController.to.removeFromCart(cartItem!);
    }
  }
}
