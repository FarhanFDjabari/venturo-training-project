import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_conditional_rendering/conditional.dart';
import 'package:flutter_conditional_rendering/conditional_switch.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:java_code_onboarding/config/themes/colors.dart';
import 'package:java_code_onboarding/constant/common/constants.dart';
import 'package:java_code_onboarding/constant/core/asset_const.dart';
import 'package:java_code_onboarding/modules/features/home/view/components/quantity_counter.dart';
import 'package:java_code_onboarding/modules/features/menu/controllers/detail_menu_controller.dart';
import 'package:java_code_onboarding/shared/widgets/primary_button_with_title.dart';
import 'package:java_code_onboarding/shared/widgets/rect_shimmer.dart';
import 'package:java_code_onboarding/shared/widgets/rounded_custom_app_bar.dart';
import 'package:java_code_onboarding/shared/widgets/tile_option.dart';
import 'package:java_code_onboarding/utils/enums/detail_menu_level_state.dart';
import 'package:java_code_onboarding/utils/enums/menu_detail_state.dart';
import 'package:java_code_onboarding/utils/extensions/currency_ext.dart';

class MenuView extends StatelessWidget {
  const MenuView({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.kLightColor2,
      appBar: RoundedAppBar(
        title: 'Menu detail'.tr,
      ),
      body: CustomScrollView(
        slivers: [
          SliverToBoxAdapter(
            child: Container(
              height: 231.h,
              alignment: Alignment.center,
              padding: EdgeInsets.symmetric(horizontal: 97.w, vertical: 25.h),
              child: Obx(() => Conditional.single(
                    context: context,
                    conditionBuilder: (context) =>
                        DetailMenuController.to.menuDetailState.value ==
                        MenuDetailState.Success,
                    widgetBuilder: (context) => Hero(
                      tag: 'menu-${DetailMenuController.to.menu.value!.idMenu}',
                      child: CachedNetworkImage(
                        imageUrl: DetailMenuController.to.menu.value!.foto,
                        height: 181.h,
                        width: 234.w,
                        fit: BoxFit.contain,
                        errorWidget: (context, _, __) => CachedNetworkImage(
                          imageUrl: AppConstants.defaultMenuPhoto,
                          fit: BoxFit.contain,
                        ),
                      ),
                    ),
                    fallbackBuilder: (context) => RectShimmer(
                      height: 181.h,
                      width: 234.h,
                      radius: 30.r,
                    ),
                  )),
            ),
          ),
          SliverFillRemaining(
            hasScrollBody: false,
            fillOverscroll: true,
            child: Container(
              padding: EdgeInsets.fromLTRB(25.w, 45.h, 25.w, 20.h),
              decoration: BoxDecoration(
                color: AppColor.kLightColor,
                borderRadius: BorderRadius.vertical(top: Radius.circular(30.r)),
              ),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(
                        child: Obx(() => Conditional.single(
                              context: context,
                              conditionBuilder: (context) =>
                                  DetailMenuController
                                      .to.menuDetailState.value ==
                                  MenuDetailState.Success,
                              widgetBuilder: (context) => Text(
                                DetailMenuController.to.menu.value?.nama ?? '',
                                style: Get.textTheme.titleMedium!
                                    .copyWith(color: AppColor.kBlueColor),
                                overflow: TextOverflow.ellipsis,
                                maxLines: 3,
                              ),
                              fallbackBuilder: (context) => RectShimmer(
                                width: 168.w,
                                height: 21.h,
                              ),
                            )),
                      ),
                      10.horizontalSpace,
                      Obx(
                        () => DetailMenuController.to.menuDetailState.value ==
                                MenuDetailState.Success
                            ? QuantityCounter(
                                quantity:
                                    DetailMenuController.to.quantity.value,
                                onIncrement: DetailMenuController.to.onAddQty,
                                onDecrement: DetailMenuController
                                            .to.isAlreadyExist.value ||
                                        DetailMenuController.to.quantity.value >
                                            0
                                    ? DetailMenuController.to.onRemoveQty
                                    : null,
                              )
                            : const SizedBox(),
                      ),
                    ],
                  ),
                  14.verticalSpace,
                  Obx(() => Conditional.single(
                        context: context,
                        conditionBuilder: (context) =>
                            DetailMenuController.to.menuDetailState.value ==
                            MenuDetailState.Success,
                        widgetBuilder: (context) => Text(
                          DetailMenuController.to.menu.value?.deskripsi ?? '',
                          style: Get.textTheme.labelMedium,
                          textAlign: TextAlign.left,
                        ),
                        fallbackBuilder: (context) => RectShimmer(
                          width: 295.w,
                          height: 45.h,
                        ),
                      )),
                  39.verticalSpace,
                  Divider(color: AppColor.kDarkColor8, height: 2.h),
                  Obx(
                    () => TileOption(
                      title: 'Price'.tr,
                      message:
                          DetailMenuController.to.cartItem?.price.toRupiah ??
                              '-',
                      icon: AssetConstants.iconPrice,
                      messageStyle: Get.textTheme.headlineSmall
                          ?.copyWith(color: AppColor.kBlueColor),
                    ),
                  ),
                  Obx(
                    () => ConditionalSwitch.single(
                      context: context,
                      valueBuilder: (context) =>
                          DetailMenuController.to.detailMenuLevelState.value,
                      caseBuilders: {
                        DetailMenuLevelState.Success: (context) => Wrap(
                              children: [
                                Divider(
                                  color: AppColor.kDarkColor8,
                                  height: 2.h,
                                ),
                                TileOption(
                                  icon: AssetConstants.iconLevel,
                                  title: 'Level'.tr,
                                  message:
                                      DetailMenuController.to.selectedLevelText,
                                  onTap: DetailMenuController
                                      .to.showLevelBottomSheet,
                                ),
                              ],
                            ),
                        DetailMenuLevelState.Loading: (context) => Wrap(
                              children: [
                                Divider(
                                  color: AppColor.kDarkColor8,
                                  height: 2.h,
                                ),
                                Padding(
                                  padding: EdgeInsets.symmetric(vertical: 8.h),
                                  child: RectShimmer(height: 32.h),
                                ),
                              ],
                            ),
                      },
                      fallbackBuilder: (context) => const SizedBox(),
                    ),
                  ),
                  Obx(
                    () => ConditionalSwitch.single(
                      context: context,
                      valueBuilder: (context) =>
                          DetailMenuController.to.detailMenuToppingState.value,
                      caseBuilders: {
                        DetailMenuLevelState.Success: (context) => Wrap(
                              children: [
                                Divider(
                                  color: AppColor.kDarkColor8,
                                  height: 2.h,
                                ),
                                TileOption(
                                  icon: AssetConstants.iconLevel,
                                  title: 'Topping'.tr,
                                  message: DetailMenuController
                                      .to.selectedToppingText,
                                  onTap: DetailMenuController
                                      .to.showToppingsBottomSheet,
                                ),
                              ],
                            ),
                        DetailMenuLevelState.Loading: (context) => Wrap(
                              children: [
                                Divider(
                                  color: AppColor.kDarkColor8,
                                  height: 2.h,
                                ),
                                Padding(
                                  padding: EdgeInsets.symmetric(vertical: 8.h),
                                  child: RectShimmer(height: 32.h),
                                ),
                              ],
                            ),
                      },
                      fallbackBuilder: (context) => const SizedBox(),
                    ),
                  ),
                  Divider(color: AppColor.kDarkColor8, height: 2.h),
                  Obx(
                    () => TileOption(
                      icon: AssetConstants.iconEdit,
                      title: 'Note'.tr,
                      message: DetailMenuController.to.note.isNotEmpty
                          ? DetailMenuController.to.note.value
                          : 'Add note'.tr,
                      onTap: DetailMenuController.to.showNoteBottomSheet,
                    ),
                  ),
                  Divider(
                    color: AppColor.kDarkColor8,
                    height: 2.h,
                  ),
                  40.verticalSpace,
                  Obx(
                    () => ConditionalSwitch.single(
                      context: context,
                      valueBuilder: (context) =>
                          DetailMenuController.to.menuDetailState.value,
                      caseBuilders: {
                        MenuDetailState.Loading: (context) => RectShimmer(
                              height: 50.r,
                              radius: 30.r,
                            ),
                        MenuDetailState.Success: (context) => SizedBox(
                              width: double.infinity,
                              child: Conditional.single(
                                context: context,
                                conditionBuilder: (context) =>
                                    DetailMenuController.to.quantity > 0,
                                widgetBuilder: (context) =>
                                    PrimaryButtonWithTitle(
                                  title: DetailMenuController
                                          .to.isAlreadyExist.value
                                      ? 'Update to order'.tr
                                      : 'Add to order'.tr,
                                  onPressed: DetailMenuController.to.addToCart,
                                ),
                                fallbackBuilder: (context) => const SizedBox(),
                              ),
                            ),
                      },
                      fallbackBuilder: (context) => const SizedBox(),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
