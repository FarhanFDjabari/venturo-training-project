import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:java_code_onboarding/config/themes/colors.dart';
import 'package:java_code_onboarding/modules/features/menu/controllers/detail_menu_controller.dart';

import 'holder_bottom_sheet.dart';

class NoteBottomSheet extends StatelessWidget {
  final TextEditingController noteController =
      TextEditingController(text: DetailMenuController.to.note.value);

  NoteBottomSheet({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 17.w, vertical: 19.h),
      child: Wrap(
        children: [
          const HolderBottomSheet(),
          13.verticalSpacingRadius,
          Text('Create note'.tr, style: Get.textTheme.headlineSmall),
          13.verticalSpacingRadius,
          Row(
            children: [
              Expanded(
                child: TextField(
                  controller: noteController,
                  style: Get.textTheme.bodySmall,
                  decoration: InputDecoration(
                    hintText: 'Add note'.tr,
                    hintStyle: Get.textTheme.bodySmall,
                    enabledBorder: const UnderlineInputBorder(
                      borderSide:
                          BorderSide(color: AppColor.kBlueColor, width: 2),
                    ),
                    focusedBorder: const UnderlineInputBorder(
                      borderSide:
                          BorderSide(color: AppColor.kBlueColor, width: 2),
                    ),
                    isDense: true,
                  ),
                  maxLength: 100,
                  autofocus: true,
                ),
              ),
              6.horizontalSpace,
              InkWell(
                onTap: () {
                  DetailMenuController.to.setNote(noteController.text);
                },
                radius: 20.r,
                child: const Icon(
                  Icons.check_circle,
                  color: AppColor.kBlueColor,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
