import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:java_code_onboarding/config/routes/routes.dart';
import 'package:java_code_onboarding/modules/features/dashboard/view/components/get_location_view.dart';
import 'package:java_code_onboarding/utils/enums/location_controller_state.dart';
import 'package:java_code_onboarding/utils/services/geolocation_services.dart';

class DashboardController extends GetxController {
  static DashboardController get to => Get.find<DashboardController>();

  // tab index for bottom nav bar
  final RxInt tabIndex = 0.obs;

  // location data
  Rx<LocationControllerState> locationState =
      LocationControllerState.Loading.obs;
  RxString message = ''.obs;
  Rxn<Position> positionStream = Rxn<Position>();
  RxnString addressStream = RxnString();

  @override
  void onReady() {
    super.onReady();

    getLocation();
    GeoLocationServices.serviceStatusStream.listen((status) => getLocation());
  }

  // get user location
  Future<void> getLocation() async {
    if (Get.isDialogOpen == false) {
      Get.dialog(
        const GetLocationView(),
        barrierDismissible: false,
      );
    }

    try {
      // get current position
      locationState(LocationControllerState.Loading);
      final result = await GeoLocationServices.getCurrentPosition();

      if (result.success) {
        // get address info if user location is within max distance
        positionStream(result.position);
        addressStream(result.address);
        locationState(LocationControllerState.Success);

        await Future.delayed(const Duration(seconds: 1));
        Get.until(ModalRoute.withName(AppRoutes.dashboardView));
      } else {
        // show error message
        message(result.message);
        locationState(LocationControllerState.Error);
      }
    } catch (e) {
      // show error message
      message('Server error'.tr);
      locationState(LocationControllerState.Error);
    }
  }
}
