import 'package:get/get.dart';
import 'package:java_code_onboarding/modules/features/cart/controllers/cart_controller.dart';
import 'package:java_code_onboarding/modules/features/dashboard/controllers/dashboard_controller.dart';
import 'package:java_code_onboarding/modules/features/home/controllers/home_controller.dart';
import 'package:java_code_onboarding/modules/features/order/controllers/order_controller.dart';
import 'package:java_code_onboarding/modules/features/profile/controllers/profile_controller.dart';

class DashboardBinding extends Bindings {
  @override
  void dependencies() {
    Get.put<DashboardController>(DashboardController());
    Get.put<CartController>(CartController());
    Get.lazyPut<HomeController>(() => HomeController());
    Get.lazyPut<OrderController>(() => OrderController());
    Get.lazyPut<ProfileController>(() => ProfileController());
  }
}
