import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:java_code_onboarding/modules/features/dashboard/controllers/dashboard_controller.dart';
import 'package:java_code_onboarding/modules/features/dashboard/view/components/rounded_bottom_nav_bar.dart';
import 'package:java_code_onboarding/modules/features/home/view/ui/home_view.dart';
import 'package:java_code_onboarding/modules/features/order/view/ui/order_view.dart';
import 'package:java_code_onboarding/modules/features/profile/view/ui/profile_view.dart';

class DashboardView extends StatelessWidget {
  const DashboardView({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
          child: Obx(
        () => IndexedStack(
          index: DashboardController.to.tabIndex.value,
          children: [
            HomeView(),
            const OrderView(),
            const ProfileView(),
          ],
        ),
      )),
      bottomNavigationBar: const RoundedBottomNavBar(),
    );
  }
}
