import 'package:app_settings/app_settings.dart';
import 'package:flutter/material.dart';
import 'package:flutter_conditional_rendering/conditional_switch.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:java_code_onboarding/config/themes/colors.dart';
import 'package:java_code_onboarding/constant/core/asset_const.dart';
import 'package:java_code_onboarding/modules/features/dashboard/controllers/dashboard_controller.dart';
import 'package:java_code_onboarding/utils/enums/location_controller_state.dart';

class GetLocationView extends StatelessWidget {
  const GetLocationView({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: WillPopScope(
        onWillPop: () async => false,
        child: Container(
          width: 1.sw,
          height: 1.sh,
          alignment: Alignment.center,
          decoration: const BoxDecoration(
            color: AppColor.kLightColor,
            image: DecorationImage(
              image: AssetImage(AssetConstants.bgPattern2),
              fit: BoxFit.cover,
            ),
          ),
          padding: EdgeInsets.all(32.r),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                'Searching location...'.tr,
                style: Get.textTheme.titleLarge?.copyWith(
                  fontWeight: FontWeight.w400,
                  color: AppColor.kDarkColor6,
                ),
              ),
              49.verticalSpacingRadius,
              Stack(
                children: [
                  Image.asset(AssetConstants.iconLocation, width: 190.r),
                  Positioned(
                    top: 5.h,
                    left: 0,
                    right: 0,
                    child: Icon(Icons.location_pin, size: 60.r),
                  ),
                ],
              ),
              49.verticalSpacingRadius,
              Obx(
                () => ConditionalSwitch.single(
                  context: context,
                  valueBuilder: (context) =>
                      DashboardController.to.locationState.value,
                  caseBuilders: {
                    LocationControllerState.Success: (context) => Text(
                          DashboardController.to.addressStream.value ?? '',
                          textAlign: TextAlign.center,
                          style: Get.textTheme.titleLarge,
                        ),
                    LocationControllerState.Error: (context) => Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Text(
                              DashboardController.to.message.value,
                              textAlign: TextAlign.center,
                              style: Get.textTheme.titleLarge,
                            ),
                            24.verticalSpace,
                            ElevatedButton(
                              onPressed: () =>
                                  AppSettings.openLocationSettings(),
                              style: ElevatedButton.styleFrom(
                                backgroundColor: AppColor.kLightColor,
                                elevation: 2,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(50.r),
                                ),
                              ),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  const Icon(Icons.settings,
                                      color: AppColor.kDarkColor),
                                  16.horizontalSpaceRadius,
                                  Text(
                                    'Open settings'.tr,
                                    style: Get.textTheme.bodySmall,
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                  },
                  fallbackBuilder: (context) =>
                      const CircularProgressIndicator(),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
