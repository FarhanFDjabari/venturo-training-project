import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:java_code_onboarding/config/themes/colors.dart';
import 'package:java_code_onboarding/constant/core/asset_const.dart';
import 'package:badges/badges.dart' as badge;
import 'package:java_code_onboarding/modules/features/dashboard/controllers/dashboard_controller.dart';
import 'package:java_code_onboarding/modules/features/order/controllers/order_controller.dart';

class RoundedBottomNavBar extends StatelessWidget {
  const RoundedBottomNavBar({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => ClipRRect(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(30.r),
          topRight: Radius.circular(30.r),
        ),
        child: BottomNavigationBar(
          currentIndex: DashboardController.to.tabIndex.value,
          onTap: (value) => DashboardController.to.tabIndex(value),
          backgroundColor: AppColor.kDarkColor2,
          selectedLabelStyle: Get.textTheme.labelSmall?.copyWith(
            color: AppColor.kLightColor,
            fontWeight: FontWeight.w700,
            letterSpacing: 0,
          ),
          unselectedLabelStyle: Get.textTheme.labelSmall?.copyWith(
            color: AppColor.kDarkColor7,
            fontWeight: FontWeight.w400,
            letterSpacing: 0,
          ),
          unselectedItemColor: AppColor.kDarkColor7,
          selectedItemColor: AppColor.kLightColor,
          items: [
            BottomNavigationBarItem(
              icon: Padding(
                padding: EdgeInsets.only(bottom: 8.h),
                child: SvgPicture.asset(
                  AssetConstants.iconHome,
                  colorFilter: const ColorFilter.mode(
                      AppColor.kDarkColor7, BlendMode.srcIn),
                  height: 27.r,
                  width: 27.r,
                ),
              ),
              activeIcon: Padding(
                padding: EdgeInsets.only(bottom: 8.h),
                child: SvgPicture.asset(
                  AssetConstants.iconHome,
                  colorFilter: const ColorFilter.mode(
                      AppColor.kLightColor, BlendMode.srcIn),
                  height: 27.r,
                  width: 27.r,
                ),
              ),
              label: 'Home'.tr,
              tooltip: 'Home'.tr,
            ),
            BottomNavigationBarItem(
              icon: badge.Badge(
                showBadge: OrderController.to.onGoingOrders.isNotEmpty,
                badgeStyle: const badge.BadgeStyle(
                    badgeColor: AppColor.kBlueColor,
                    borderSide: BorderSide(
                      color: AppColor.kLightColor,
                    )),
                position: badge.BadgePosition.topEnd(
                  end: -20.w,
                ),
                badgeContent: Text(
                  OrderController.to.onGoingOrders.length > 99
                      ? '99+'
                      : OrderController.to.onGoingOrders.length.toString(),
                  style: Get.textTheme.labelMedium!.copyWith(
                    color: AppColor.kLightColor,
                    fontWeight: FontWeight.w700,
                    letterSpacing: 0.w,
                  ),
                ),
                child: Padding(
                  padding: EdgeInsets.only(bottom: 8.h),
                  child: SvgPicture.asset(
                    AssetConstants.iconOrder,
                    colorFilter: const ColorFilter.mode(
                        AppColor.kDarkColor7, BlendMode.srcIn),
                    height: 27.r,
                    width: 27.r,
                  ),
                ),
              ),
              activeIcon: badge.Badge(
                showBadge: OrderController.to.onGoingOrders.isNotEmpty,
                badgeStyle: const badge.BadgeStyle(
                    badgeColor: AppColor.kBlueColor,
                    borderSide: BorderSide(
                      color: AppColor.kLightColor,
                    )),
                position: badge.BadgePosition.topEnd(
                  end: -20.w,
                ),
                badgeContent: Text(
                  OrderController.to.onGoingOrders.length > 99
                      ? '99+'
                      : OrderController.to.onGoingOrders.length.toString(),
                  style: Get.textTheme.labelMedium!.copyWith(
                    color: AppColor.kLightColor,
                    fontWeight: FontWeight.w700,
                  ),
                ),
                child: Padding(
                  padding: EdgeInsets.only(bottom: 8.h),
                  child: SvgPicture.asset(
                    AssetConstants.iconOrder,
                    colorFilter: const ColorFilter.mode(
                        AppColor.kLightColor, BlendMode.srcIn),
                    height: 27.r,
                    width: 27.r,
                  ),
                ),
              ),
              tooltip: 'Order'.tr,
              label: 'Order'.tr,
            ),
            BottomNavigationBarItem(
              icon: Padding(
                padding: EdgeInsets.only(bottom: 8.h),
                child: SvgPicture.asset(
                  AssetConstants.iconProfile,
                  colorFilter: const ColorFilter.mode(
                      AppColor.kDarkColor7, BlendMode.srcIn),
                  height: 27.r,
                  width: 27.r,
                ),
              ),
              activeIcon: Padding(
                padding: EdgeInsets.only(bottom: 8.h),
                child: SvgPicture.asset(
                  AssetConstants.iconProfile,
                  colorFilter: const ColorFilter.mode(
                      AppColor.kLightColor, BlendMode.srcIn),
                  height: 27.r,
                  width: 27.r,
                ),
              ),
              tooltip: 'Profile'.tr,
              label: 'Profile'.tr,
            ),
          ],
        ),
      ),
    );
  }
}
