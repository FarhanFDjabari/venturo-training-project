import 'package:get/get.dart';

class AddReviewController extends GetxController {
  static AddReviewController get to => Get.find<AddReviewController>();

  final List<String> reviewTypes = [
    'Harga',
    'Rasa',
    'Penyajian makanan',
    'Pelayanan',
    'Fasilitas'
  ];
}
