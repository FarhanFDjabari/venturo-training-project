import 'package:get/get.dart';
import 'package:java_code_onboarding/modules/features/review/repositories/review_repository.dart';
import 'package:java_code_onboarding/modules/models/review.dart';
import 'package:java_code_onboarding/utils/enums/review_state.dart';

class ReviewController extends GetxController {
  static ReviewController get to => Get.find<ReviewController>();

  RxString message = RxString('');
  RxList<Review> listReview = RxList<Review>();
  Rx<ReviewState> reviewState = ReviewState.Loading.obs;

  @override
  void onInit() {
    super.onInit();

    getReviews();
  }

  Future<void> getReviews() async {
    reviewState(ReviewState.Loading);
    final result = await ReviewRepository.getListReviews();

    if (result.statusCode == 200) {
      reviewState(ReviewState.Success);
      listReview(result.data);
    } else if (result.statusCode == 204 || result.statusCode == 422) {
      reviewState(ReviewState.Empty);
    } else {
      reviewState(ReviewState.Error);
      message(result.message);
    }
  }
}
