import 'package:get/get.dart';
import 'package:java_code_onboarding/modules/features/review/controllers/add_review_controller.dart';

class AddReviewBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(AddReviewController());
  }
}
