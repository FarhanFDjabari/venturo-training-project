import 'package:dio/dio.dart';
import 'package:java_code_onboarding/constant/core/api_const.dart';
import 'package:java_code_onboarding/modules/models/review.dart';
import 'package:java_code_onboarding/utils/services/api_services.dart';
import 'package:java_code_onboarding/utils/services/local_db_services.dart';

class ReviewRepository {
  ReviewRepository._();

  static Future<ListReviewRes> getListReviews() async {
    try {
      final userToken = await LocalDBServices.getUserToken();
      final userData = await LocalDBServices.getCurrentUser();
      final dio = ApiServices.dioCall(token: userToken);
      final response =
          await dio.get('${ApiConstants.review}/${userData?.idUser}');

      return ListReviewRes.fromJson(response.data);
    } on DioError {
      return const ListReviewRes(statusCode: 500);
    }
  }
}
