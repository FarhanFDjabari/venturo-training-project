import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:java_code_onboarding/config/themes/colors.dart';
import 'package:java_code_onboarding/constant/core/asset_const.dart';
import 'package:java_code_onboarding/modules/features/review/controllers/add_review_controller.dart';
import 'package:java_code_onboarding/modules/features/review/view/components/add_image_button.dart';
import 'package:java_code_onboarding/modules/features/review/view/components/review_type_chip.dart';
import 'package:java_code_onboarding/shared/widgets/primary_button_with_title.dart';
import 'package:java_code_onboarding/shared/widgets/rounded_custom_app_bar.dart';
import 'package:java_code_onboarding/utils/functions/texts.dart';

class AddReviewView extends StatelessWidget {
  const AddReviewView({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: RoundedAppBar(
        title: 'Review'.tr,
        titleStyle: Get.textTheme.headlineMedium,
        titleWidget: ColoredBox(
          color: AppColor.kBlueColor,
          child: SizedBox(
            width: 65.w,
            height: 2.h,
          ),
        ),
      ),
      body: Container(
        height: 1.sh,
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage(AssetConstants.bgPattern2),
            fit: BoxFit.fitHeight,
            alignment: Alignment.center,
          ),
        ),
        child: ListView(
          padding: EdgeInsets.symmetric(vertical: 21.h, horizontal: 16.w),
          children: [
            // Rating bar
            Container(
              padding: EdgeInsets.symmetric(vertical: 15.h, horizontal: 20.w),
              decoration: BoxDecoration(
                color: AppColor.kLightColor2,
                borderRadius: BorderRadius.circular(20.r),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Give your review!'.tr,
                    style: Get.textTheme.titleSmall,
                  ),
                  13.verticalSpacingRadius,
                  Row(
                    children: [
                      Expanded(
                        child: RatingBar(
                          initialRating: 0,
                          itemPadding: EdgeInsets.all(9.r),
                          itemSize: 22.r,
                          ratingWidget: RatingWidget(
                            full: ShaderMask(
                              child: SvgPicture.asset(
                                AssetConstants.iconStar,
                                width: 12.r,
                                height: 12.r,
                              ),
                              shaderCallback: (rect) {
                                return AppColor.orangeGradient
                                    .createShader(rect);
                              },
                            ),
                            half: SvgPicture.asset(
                              AssetConstants.iconStarEmpty,
                              width: 12.r,
                              height: 12.r,
                            ),
                            empty: SvgPicture.asset(
                              AssetConstants.iconStarEmpty,
                              width: 12.r,
                              height: 12.r,
                            ),
                          ),
                          onRatingUpdate: (rating) {},
                        ),
                      ),
                      Text(
                        scoreToString(0),
                        style: Get.textTheme.labelMedium,
                      ),
                    ],
                  ),
                ],
              ),
            ),
            16.verticalSpacingRadius,

            // Review form
            Container(
              padding: EdgeInsets.symmetric(vertical: 15.h, horizontal: 20.w),
              decoration: BoxDecoration(
                color: AppColor.kLightColor2,
                borderRadius: BorderRadius.circular(20.r),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'What can be upgraded?'.tr,
                    style: Get.textTheme.titleSmall,
                  ),
                  13.verticalSpacingRadius,
                  Wrap(
                    spacing: 10.r,
                    runSpacing: 10.r,
                    children: AddReviewController.to.reviewTypes
                        .map<Widget>(
                          (e) => ReviewTypeChip(
                            type: e,
                            isSelected: false,
                            onTap: () {},
                          ),
                        )
                        .toList(),
                  ),
                  22.verticalSpacingRadius,
                  Divider(
                    color: AppColor.kDarkColor2.withOpacity(0.25),
                    height: 1.r,
                  ),
                  18.verticalSpacingRadius,
                  Text(
                    'Write a review'.tr,
                    style: Get.textTheme.titleSmall,
                  ),
                  5.verticalSpacingRadius,
                  Form(
                    child: Container(
                      padding: EdgeInsets.symmetric(
                        vertical: 8.r,
                        horizontal: 13.r,
                      ),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10.r),
                        border: Border.all(
                          color: AppColor.kDarkColor2.withOpacity(0.25),
                          width: 0.5.r,
                        ),
                      ),
                      child: TextFormField(
                        controller: TextEditingController(),
                        maxLines: 5,
                        style: Get.textTheme.labelMedium,
                        decoration: InputDecoration.collapsed(
                          hintText: 'Write your review here...'.tr,
                        ),
                      ),
                    ),
                  ),
                  21.verticalSpace,
                  Row(
                    children: [
                      Expanded(
                        child: PrimaryButtonWithTitle(
                          onPressed: () {},
                          title: 'Submit review'.tr,
                        ),
                      ),
                      14.horizontalSpaceRadius,
                      AddImageButton(
                        onPressed: () {},
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
