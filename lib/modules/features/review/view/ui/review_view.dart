import 'package:flutter/material.dart';
import 'package:flutter_conditional_rendering/flutter_conditional_rendering.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:java_code_onboarding/config/routes/routes.dart';
import 'package:java_code_onboarding/config/themes/colors.dart';
import 'package:java_code_onboarding/constant/core/asset_const.dart';
import 'package:java_code_onboarding/modules/features/review/controllers/review_controller.dart';
import 'package:java_code_onboarding/modules/features/review/view/components/review_card.dart';
import 'package:java_code_onboarding/shared/widgets/empty_data_list_view.dart';
import 'package:java_code_onboarding/shared/widgets/rect_shimmer.dart';
import 'package:java_code_onboarding/shared/widgets/rounded_custom_app_bar.dart';
import 'package:java_code_onboarding/shared/widgets/server_error_list_view.dart';
import 'package:java_code_onboarding/utils/enums/review_state.dart';

class ReviewView extends StatelessWidget {
  const ReviewView({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: RoundedAppBar(
        title: 'List review'.tr,
        titleStyle: Get.textTheme.headlineMedium,
        titleWidget: ColoredBox(
          color: AppColor.kBlueColor,
          child: SizedBox(
            width: 65.w,
            height: 2.h,
          ),
        ),
      ),
      body: Container(
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage(AssetConstants.bgPattern2),
            fit: BoxFit.fitHeight,
            alignment: Alignment.center,
          ),
        ),
        child: Obx(
          () => ConditionalSwitch.single<ReviewState>(
            context: context,
            valueBuilder: (context) => ReviewController.to.reviewState.value,
            caseBuilders: {
              ReviewState.Loading: (context) => ListView.separated(
                    padding:
                        EdgeInsets.symmetric(vertical: 21.r, horizontal: 25.r),
                    separatorBuilder: (context, index) =>
                        16.verticalSpacingRadius,
                    itemCount: 4,
                    itemBuilder: (context, index) => RectShimmer(
                      height: 62.r,
                      radius: 10.r,
                    ),
                  ),
              ReviewState.Empty: (context) => const EmptyDataListView(),
              ReviewState.Error: (context) => const ServerErrorListView(),
            },
            fallbackBuilder: (context) => ListView.separated(
              padding: EdgeInsets.symmetric(vertical: 21.h, horizontal: 25.w),
              separatorBuilder: (context, index) => 16.verticalSpace,
              itemCount: ReviewController.to.listReview.length,
              itemBuilder: (context, index) => ReviewCard(
                review: ReviewController.to.listReview[index],
                onTap: () {
                  Get.toNamed(
                    "${AppRoutes.reviewView}/reply/${ReviewController.to.listReview[index].idReview}",
                  );
                },
              ),
            ),
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Get.toNamed(
            AppRoutes.addReviewView,
          );
        },
        backgroundColor: AppColor.kBlueColor,
        foregroundColor: AppColor.kLightColor,
        child: const Icon(Icons.add),
      ),
    );
  }
}
