import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:java_code_onboarding/config/themes/colors.dart';
import 'package:java_code_onboarding/constant/core/asset_const.dart';
import 'package:java_code_onboarding/modules/features/review/view/components/date_chat.dart';
import 'package:java_code_onboarding/modules/features/review/view/components/left_chat.dart';
import 'package:java_code_onboarding/modules/features/review/view/components/reply_text_input.dart';
import 'package:java_code_onboarding/modules/features/review/view/components/right_chat.dart';
import 'package:java_code_onboarding/shared/widgets/rounded_custom_app_bar.dart';

class ReplyReviewView extends StatelessWidget {
  const ReplyReviewView({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.kLightColor2,
      appBar: RoundedAppBar(
        title: 'Reply review'.tr,
        titleStyle: Get.textTheme.headlineMedium,
        titleWidget: ColoredBox(
          color: AppColor.kBlueColor,
          child: SizedBox(
            width: 65.w,
            height: 2.h,
          ),
        ),
      ),
      body: Column(
        children: [
          Expanded(
            child: Container(
              width: 1.sw,
              height: 1.sh,
              margin: EdgeInsets.symmetric(horizontal: 16.w, vertical: 21.h),
              padding: EdgeInsets.symmetric(horizontal: 14.w),
              decoration: BoxDecoration(
                color: AppColor.kLightColor,
                borderRadius: BorderRadius.circular(30.r),
                image: const DecorationImage(
                  image: AssetImage(AssetConstants.bgPattern2),
                  fit: BoxFit.fitHeight,
                  alignment: Alignment.center,
                ),
              ),
              child: ListView.separated(
                padding: EdgeInsets.symmetric(vertical: 28.h, horizontal: 14.w),
                separatorBuilder: (context, index) => 28.verticalSpace,
                reverse: true,
                itemCount: 10,
                itemBuilder: (context, index) {
                  if (index == 9) {
                    return DateChat(
                      date: DateTime.now(),
                    );
                  } else if (index % 2 == 0) {
                    return LeftChat(
                      message:
                          'Mohon menjaga kebersihan, kemarin meja masih kotor',
                      date: DateTime.now().subtract(Duration(minutes: index)),
                    );
                  } else {
                    return RightChat(
                      message:
                          'Mohon maaf atas masalah tsb, crew kami akan melakukan evaluasi kembali perihal kebersihan fasilitas cafe',
                      date: DateTime.now().subtract(Duration(minutes: index)),
                    );
                  }
                },
              ),
            ),
          ),
          const ReplyTextInput(),
        ],
      ),
    );
  }
}
