import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:java_code_onboarding/config/themes/colors.dart';
import 'package:java_code_onboarding/constant/core/asset_const.dart';
import 'package:java_code_onboarding/modules/models/review.dart';

class ReviewCard extends StatelessWidget {
  final Review review;
  final void Function()? onTap;

  const ReviewCard({
    Key? key,
    required this.review,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      borderRadius: BorderRadius.circular(10.r),
      clipBehavior: Clip.antiAlias,
      child: InkWell(
        onTap: onTap,
        child: Ink(
          padding: EdgeInsets.symmetric(vertical: 13.r, horizontal: 20.r),
          decoration: BoxDecoration(
            color: AppColor.kLightColor2,
            boxShadow: [
              BoxShadow(
                color: Colors.black.withOpacity(0.15),
                blurRadius: 5.r, // changes position of shadow
              ),
            ],
          ),
          child: Row(
            children: [
              Expanded(
                child: Column(
                  children: [
                    Row(
                      children: [
                        SvgPicture.asset(
                          AssetConstants.iconDate,
                          width: 13.r,
                          height: 13.r,
                        ),
                        7.horizontalSpace,

                        /// Review type
                        Flexible(
                          child: Text(
                            review.type,
                            style: Get.textTheme.titleSmall!.copyWith(
                              color: AppColor.kBlueColor,
                            ),
                            maxLines: 1,
                          ),
                        ),
                        11.horizontalSpace,

                        RatingBar(
                          initialRating: review.score,
                          tapOnlyMode: true,
                          itemSize: 12.r,
                          allowHalfRating: false,
                          itemPadding: EdgeInsets.symmetric(horizontal: 5.r),
                          ignoreGestures: true,
                          ratingWidget: RatingWidget(
                            full: ShaderMask(
                              child: SvgPicture.asset(
                                AssetConstants.iconStar,
                                width: 12.r,
                                height: 12.r,
                              ),
                              shaderCallback: (rect) {
                                return AppColor.orangeGradient
                                    .createShader(rect);
                              },
                            ),
                            half: SvgPicture.asset(
                              AssetConstants.iconStarEmpty,
                              width: 12.r,
                              height: 12.r,
                            ),
                            empty: SvgPicture.asset(
                              AssetConstants.iconStarEmpty,
                              width: 12.r,
                              height: 12.r,
                            ),
                          ),
                          onRatingUpdate: (value) {},
                        ),
                      ],
                    ),
                    Text(
                      review.review,
                      style: Get.textTheme.labelMedium!.copyWith(
                        color: AppColor.kDarkColor3,
                      ),
                      overflow: TextOverflow.ellipsis,
                      maxLines: 1,
                    ),
                  ],
                ),
              ),
              20.horizontalSpace,
              IconButton(
                onPressed: () {},
                splashRadius: 30.r,
                visualDensity: VisualDensity.compact,
                padding: EdgeInsets.zero,
                icon: SvgPicture.asset(
                  AssetConstants.iconReply,
                  width: 22.r,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
