import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:java_code_onboarding/config/themes/colors.dart';
import 'package:java_code_onboarding/constant/core/asset_const.dart';

class ReplyTextInput extends StatelessWidget {
  const ReplyTextInput({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 80.r,
      padding: EdgeInsets.symmetric(vertical: 12.h, horizontal: 20.w),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.vertical(top: Radius.circular(30.r)),
        color: AppColor.kLightColor,
        boxShadow: [
          BoxShadow(
            color: AppColor.kDarkColor5,
            blurRadius: 7.r,
            spreadRadius: -1.r,
          ),
        ],
      ),
      child: Row(
        children: [
          Expanded(
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 10.h, horizontal: 20.w),
              decoration: BoxDecoration(
                color: AppColor.kLightColor,
                borderRadius: BorderRadius.circular(30.r),
                border: Border.all(
                  width: 0.5.r,
                  color: AppColor.kDarkColor6,
                ),
              ),
              child: Row(
                children: [
                  Expanded(
                    child: TextField(
                      style: Get.textTheme.bodyMedium,
                      decoration: InputDecoration.collapsed(
                        hintText: 'Type a message...'.tr,
                        hintStyle: Get.textTheme.bodyMedium!.copyWith(
                          color: AppColor.kLightColor3,
                        ),
                      ),
                    ),
                  ),
                  16.horizontalSpaceRadius,
                  Material(
                    color: AppColor.kTransparent,
                    child: InkWell(
                      onTap: () {},
                      customBorder: const CircleBorder(),
                      child: SvgPicture.asset(
                        AssetConstants.iconAddImage,
                        height: 22.r,
                        colorFilter: const ColorFilter.mode(
                            AppColor.kLightColor4, BlendMode.srcIn),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          10.horizontalSpace,
          Material(
            color: Colors.transparent,
            child: InkWell(
              onTap: () {},
              customBorder: const CircleBorder(),
              child: SvgPicture.asset(AssetConstants.iconSend, height: 22.r),
            ),
          ),
        ],
      ),
    );
  }
}
