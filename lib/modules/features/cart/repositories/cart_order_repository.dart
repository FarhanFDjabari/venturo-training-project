import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:java_code_onboarding/constant/core/api_const.dart';
import 'package:java_code_onboarding/modules/models/cart_item.dart';
import 'package:java_code_onboarding/utils/services/api_services.dart';
import 'package:java_code_onboarding/utils/services/local_db_services.dart';

class CartOrderRepository {
  CartOrderRepository._();

  static Future<Response?> addOrder(CartReq cart) async {
    try {
      final userToken = await LocalDBServices.getUserToken();
      final dio = ApiServices.dioCall(token: userToken);
      final response = await dio.post(
        ApiConstants.orderAdd,
        data: json.encode(cart.toMap()),
      );

      return response;
    } on DioError {
      return null;
    }
  }
}
