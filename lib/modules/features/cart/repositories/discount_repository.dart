import 'package:dio/dio.dart';
import 'package:get/get.dart';
import 'package:java_code_onboarding/constant/core/api_const.dart';
import 'package:java_code_onboarding/modules/models/discount.dart';
import 'package:java_code_onboarding/utils/services/api_services.dart';
import 'package:java_code_onboarding/utils/services/local_db_services.dart';

class DiscountRepository {
  DiscountRepository._();

  static Future<ListDiscountRes> getAllDiscount() async {
    try {
      final userToken = await LocalDBServices.getUserToken();
      final user = await LocalDBServices.getCurrentUser();
      final dio = ApiServices.dioCall(token: userToken);
      final response =
          await dio.get('${ApiConstants.diskonOfUser}/${user?.idUser}');

      return ListDiscountRes.fromJson(response.data);
    } on DioError {
      return ListDiscountRes(statusCode: 500, message: 'Server error'.tr);
    }
  }
}
