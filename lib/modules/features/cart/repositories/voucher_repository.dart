import 'package:dio/dio.dart';
import 'package:get/get.dart';
import 'package:java_code_onboarding/constant/core/api_const.dart';
import 'package:java_code_onboarding/modules/models/voucher.dart';
import 'package:java_code_onboarding/utils/services/api_services.dart';
import 'package:java_code_onboarding/utils/services/local_db_services.dart';

class VoucherRepository {
  VoucherRepository._();

  static Future<ListVoucherRes> getAllVoucher() async {
    try {
      final userToken = await LocalDBServices.getUserToken();
      final user = await LocalDBServices.getCurrentUser();
      final dio = ApiServices.dioCall(token: userToken);
      final response =
          await dio.get('${ApiConstants.voucherOfUser}/${user?.idUser}');

      return ListVoucherRes.fromJson(response.data);
    } on DioError {
      return ListVoucherRes(statusCode: 500, message: 'Server error'.tr);
    }
  }
}
