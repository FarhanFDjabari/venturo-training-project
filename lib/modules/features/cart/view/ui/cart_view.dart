import 'package:flutter/material.dart';
import 'package:flutter_conditional_rendering/conditional.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:java_code_onboarding/config/themes/colors.dart';
import 'package:java_code_onboarding/constant/core/asset_const.dart';
import 'package:java_code_onboarding/modules/features/cart/controllers/cart_controller.dart';
import 'package:java_code_onboarding/modules/features/cart/view/components/cart_list_sliver.dart';
import 'package:java_code_onboarding/modules/features/cart/view/components/cart_order_bottom_bar.dart';

import 'package:java_code_onboarding/shared/widgets/rounded_custom_app_bar.dart';
import 'package:java_code_onboarding/shared/widgets/section_header.dart';
import 'package:java_code_onboarding/shared/widgets/tile_option.dart';
import 'package:java_code_onboarding/utils/extensions/currency_ext.dart';

class CartView extends StatelessWidget {
  const CartView({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: RoundedAppBar(
        title: 'Order'.tr,
        iconAssetUrl: AssetConstants.iconOrder,
      ),
      body: Obx(
        () => Conditional.single(
          context: context,
          conditionBuilder: (context) => CartController.to.cart.isNotEmpty,
          widgetBuilder: (context) => CustomScrollView(
            physics: const ClampingScrollPhysics(),
            slivers: [
              SliverToBoxAdapter(child: 28.verticalSpace),
              if (CartController.to.foodItems.isNotEmpty) ...[
                SliverToBoxAdapter(
                  child: SectionHeader(
                    iconAssetUrl: AssetConstants.iconFood,
                    title: 'Food'.tr,
                    color: AppColor.kBlueColor,
                  ),
                ),
                SliverPadding(
                  padding:
                      EdgeInsets.symmetric(horizontal: 25.w, vertical: 8.h),
                  sliver: CartListSliver(
                    carts: CartController.to.foodItems,
                  ),
                )
              ],
              SliverToBoxAdapter(child: 17.verticalSpace),
              if (CartController.to.drinkItems.isNotEmpty) ...[
                SliverToBoxAdapter(
                  child: SectionHeader(
                    iconAssetUrl: AssetConstants.iconFood,
                    title: 'Drink'.tr,
                    color: AppColor.kBlueColor,
                  ),
                ),
                SliverPadding(
                  padding:
                      EdgeInsets.symmetric(horizontal: 25.w, vertical: 8.h),
                  sliver: CartListSliver(
                    carts: CartController.to.drinkItems,
                  ),
                )
              ],
            ],
          ),
          fallbackBuilder: (context) => SizedBox(
            width: 1.sw,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SvgPicture.asset(
                  AssetConstants.iconEmptyCart,
                  width: 0.8.sw,
                ),
                Text(
                  'Empty cart'.tr,
                  style: Get.textTheme.bodyLarge,
                  textAlign: TextAlign.center,
                ),
              ],
            ),
          ),
        ),
      ),
      bottomNavigationBar: Obx(
        () => Conditional.single(
          context: context,
          conditionBuilder: (context) => CartController.to.cart.isNotEmpty,
          widgetBuilder: (context) => Container(
            decoration: BoxDecoration(
              color: AppColor.kLightColor2,
              borderRadius: BorderRadius.vertical(
                top: Radius.circular(30.r),
              ),
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Container(
                  padding:
                      EdgeInsets.symmetric(vertical: 25.h, horizontal: 22.w),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      // Total order tile
                      TileOption(
                        title: 'Total orders'.tr,
                        subtitle: '(${CartController.to.cart.length} Menu):',
                        message: CartController.to.totalPrice.toRupiah,
                        titleStyle: Get.textTheme.headlineSmall,
                        messageStyle: Get.textTheme.labelLarge!
                            .copyWith(color: AppColor.kBlueColor),
                      ),
                      Divider(color: AppColor.kDarkColor5, height: 2.h),

                      // Discount tile
                      Conditional.single(
                        context: context,
                        conditionBuilder: (context) =>
                            CartController.to.discountPrice == 0,
                        widgetBuilder: (context) => TileOption(
                          icon: AssetConstants.iconDiscount,
                          iconSize: 24.r,
                          title: 'Discount'.tr,
                          message:
                              CartController.to.selectedVoucher.value == null
                                  ? 'No discount'.tr
                                  : 'Discount can not be combined'.tr,
                          titleStyle: Get.textTheme.headlineSmall,
                          messageStyle: Get.textTheme.bodySmall,
                        ),
                        fallbackBuilder: (context) => TileOption(
                          icon: AssetConstants.iconDiscount,
                          iconSize: 24.r,
                          title: 'Discount'.tr,
                          message: CartController.to.discountPrice.toRupiah,
                          titleStyle: Get.textTheme.headlineSmall,
                          messageStyle: Get.textTheme.bodySmall!
                              .copyWith(color: AppColor.kRedColor),
                          onTap: CartController.to.showDiscountDialog,
                        ),
                      ),
                      Divider(color: AppColor.kDarkColor5, height: 2.h),

                      // Vouchers tile
                      Conditional.single(
                        context: context,
                        conditionBuilder: (context) =>
                            CartController.to.selectedVoucher.value == null,
                        widgetBuilder: (context) => TileOption(
                          icon: AssetConstants.iconVoucher,
                          iconSize: 24.r,
                          title: 'voucher'.tr,
                          message: 'Choose voucher'.tr,
                          titleStyle: Get.textTheme.headlineSmall,
                          messageStyle: Get.textTheme.bodySmall,
                          onTap: CartController.to.showVoucherDialog,
                        ),
                        fallbackBuilder: (context) => TileOption(
                          icon: AssetConstants.iconVoucher,
                          iconSize: 24.r,
                          title: 'voucher'.tr,
                          message: CartController.to.voucherAmount.toRupiah,
                          messageSubtitle:
                              CartController.to.selectedVoucher.value!.nama,
                          titleStyle: Get.textTheme.headlineSmall,
                          messageStyle: Get.textTheme.bodySmall!
                              .copyWith(color: AppColor.kRedColor),
                          onTap: CartController.to.showVoucherDialog,
                        ),
                      ),
                      Divider(color: AppColor.kDarkColor5, height: 2.h),

                      // Payment options tile
                      TileOption(
                        icon: AssetConstants.iconPayment,
                        iconSize: 24.r,
                        title: 'Payment'.tr,
                        message: 'Pay Later',
                        titleStyle: Get.textTheme.headlineSmall,
                        messageStyle: Get.textTheme.bodySmall,
                      ),
                    ],
                  ),
                ),
                CartOrderBottomBar(
                  totalPrice: CartController.to.grandTotalPrice.toRupiah,
                  onOrderButtonPressed: CartController.to.verify,
                ),
              ],
            ),
          ),
          fallbackBuilder: (context) => const SizedBox(),
        ),
      ),
    );
  }
}
