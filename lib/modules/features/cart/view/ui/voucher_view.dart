import 'package:flutter/material.dart';
import 'package:flutter_conditional_rendering/flutter_conditional_rendering.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:java_code_onboarding/config/routes/routes.dart';
import 'package:java_code_onboarding/config/themes/colors.dart';
import 'package:java_code_onboarding/constant/core/asset_const.dart';
import 'package:java_code_onboarding/modules/features/cart/controllers/cart_controller.dart';
import 'package:java_code_onboarding/modules/features/cart/view/components/voucher_card.dart';
import 'package:java_code_onboarding/shared/widgets/empty_data_list_view.dart';
import 'package:java_code_onboarding/shared/widgets/primary_button_with_title.dart';
import 'package:java_code_onboarding/shared/widgets/rect_shimmer.dart';
import 'package:java_code_onboarding/shared/widgets/rounded_custom_app_bar.dart';
import 'package:java_code_onboarding/shared/widgets/server_error_list_view.dart';
import 'package:java_code_onboarding/utils/enums/cart_voucher_state.dart';

class VoucherView extends StatelessWidget {
  const VoucherView({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: RoundedAppBar(
        title: 'Choose voucher'.tr,
        iconAssetUrl: AssetConstants.iconVoucher,
      ),
      body: RefreshIndicator(
        onRefresh: () async => CartController.to.getUserVouchers(),
        child: Obx(
          () => ConditionalSwitch.single<CartVoucherState>(
            context: context,
            valueBuilder: (context) => CartController.to.voucherState.value,
            caseBuilders: {
              CartVoucherState.Loading: (context) => ListView.separated(
                    padding: EdgeInsets.symmetric(
                      horizontal: 25.w,
                      vertical: 30.h,
                    ),
                    itemBuilder: (context, _) =>
                        RectShimmer(height: 216.h, radius: 15.r),
                    separatorBuilder: (context, _) => 17.verticalSpace,
                    itemCount: 5,
                  ),
              CartVoucherState.Error: (context) => const ServerErrorListView(),
              CartVoucherState.Empty: (context) => const EmptyDataListView(),
            },
            fallbackBuilder: (context) => ListView.separated(
              padding: EdgeInsets.symmetric(horizontal: 25.w, vertical: 29.h),
              itemBuilder: (context, index) => VoucherCard(
                voucher: CartController.to.vouchers[index],
                isSelected: CartController.to.vouchers[index] ==
                    CartController.to.selectedVoucher.value,
                onTap: () => Get.toNamed(
                  '${AppRoutes.voucherView}/${CartController.to.vouchers[index].idVoucher}',
                  arguments: CartController.to.vouchers[index],
                ),
                onChanged: (value) {
                  if (value == true) {
                    CartController.to
                        .setVoucher(CartController.to.vouchers[index]);
                    return;
                  }
                  CartController.to.setVoucher(null);
                },
              ),
              separatorBuilder: (context, index) => 20.verticalSpace,
              itemCount: CartController.to.vouchers.length,
            ),
          ),
        ),
      ),
      bottomNavigationBar: Container(
        padding: EdgeInsets.symmetric(vertical: 10.r, horizontal: 22.r),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.vertical(top: Radius.circular(30.r)),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.2),
              blurRadius: 20,
              spreadRadius: -1,
              offset: const Offset(0, 1),
            ),
          ],
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            5.verticalSpace,
            Row(
              children: [
                10.horizontalSpace,
                Icon(Icons.check_circle_outline,
                    color: AppColor.kBlueColor, size: 24.r),
                10.horizontalSpace,
                Expanded(
                  child: RichText(
                    textAlign: TextAlign.left,
                    text: TextSpan(
                      children: [
                        TextSpan(
                          text:
                              'The use of vouchers cannot be combined with a discount'
                                  .tr,
                          style: Get.textTheme.bodySmall,
                        ),
                        TextSpan(
                          text: ' ${'employee reward program'.tr}',
                          style: Get.textTheme.labelLarge!
                              .copyWith(color: AppColor.kBlueColor),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            10.verticalSpace,
            SizedBox(
              width: 1.sw,
              height: 42.h,
              child: PrimaryButtonWithTitle(
                title: 'Okay'.tr,
                onPressed: () => Get.back(),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
