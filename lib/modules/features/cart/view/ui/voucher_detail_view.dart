import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_conditional_rendering/conditional.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:java_code_onboarding/config/routes/routes.dart';
import 'package:java_code_onboarding/config/themes/colors.dart';
import 'package:java_code_onboarding/constant/core/asset_const.dart';
import 'package:java_code_onboarding/modules/features/cart/controllers/cart_controller.dart';
import 'package:java_code_onboarding/modules/models/voucher.dart';
import 'package:java_code_onboarding/shared/widgets/outlined_title_button.dart';
import 'package:java_code_onboarding/shared/widgets/primary_button_with_title.dart';
import 'package:java_code_onboarding/shared/widgets/rounded_custom_app_bar.dart';
import 'package:java_code_onboarding/shared/widgets/tile_option.dart';

class VoucherDetailView extends StatelessWidget {
  const VoucherDetailView({super.key});

  @override
  Widget build(BuildContext context) {
    final voucher = Get.arguments as Voucher;
    return Scaffold(
      backgroundColor: AppColor.kLightColor2,
      appBar: RoundedAppBar(title: 'Detail voucher'.tr),
      body: Column(
        children: [
          Container(
            alignment: Alignment.center,
            height: 220.h,
            child: Hero(
              tag: 'voucher-image-${voucher.idVoucher}',
              child: CachedNetworkImage(
                imageUrl: voucher.infoVoucher,
                fit: BoxFit.fitWidth,
              ),
            ),
          ),
          Expanded(
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 45.h, horizontal: 25.w),
              decoration: BoxDecoration(
                color: AppColor.kLightColor,
                borderRadius: BorderRadius.vertical(top: Radius.circular(30.r)),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    voucher.nama,
                    style: Get.textTheme.titleMedium!
                        .copyWith(color: AppColor.kBlueColor),
                  ),
                  Conditional.single(
                    context: context,
                    conditionBuilder: (context) => voucher.catatan != null,
                    widgetBuilder: (context) => Html(
                      data: voucher.catatan,
                      style: {
                        '*': Style.fromTextStyle(
                          Get.textTheme.labelMedium!,
                        ),
                        'body': Style(
                          margin: Margins.zero,
                          padding: EdgeInsets.zero,
                        ),
                      },
                    ),
                    fallbackBuilder: (context) => const SizedBox(),
                  ),
                  40.verticalSpace,
                  const Divider(color: AppColor.kDarkColor5),
                  TileOption(
                    icon: AssetConstants.iconDate,
                    title: 'Valid date'.tr,
                    message:
                        '${DateFormat('dd/MM/yyyy').format(voucher.periodeMulai)} - ${DateFormat('dd/MM/yyyy').format(voucher.periodeSelesai)}',
                    titleStyle: Get.textTheme.headlineSmall,
                  ),
                  const Divider(color: AppColor.kDarkColor5),
                ],
              ),
            ),
          ),
        ],
      ),
      bottomNavigationBar: Container(
        color: AppColor.kLightColor,
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 10.h, horizontal: 22.w),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.vertical(top: Radius.circular(30.r)),
            color: AppColor.kLightColor,
            boxShadow: const [
              BoxShadow(
                color: AppColor.kDarkColor5,
                blurRadius: 20,
                spreadRadius: -1,
                offset: Offset(0, 1),
              ),
            ],
          ),
          child: Conditional.single(
            context: context,
            conditionBuilder: (context) =>
                voucher != CartController.to.selectedVoucher.value,
            widgetBuilder: (context) => PrimaryButtonWithTitle(
              title: 'Use voucher'.tr,
              onPressed: () {
                CartController.to.setVoucher(voucher);
                Get.until(ModalRoute.withName(AppRoutes.cartView));
              },
            ),
            fallbackBuilder: (context) => OutlinedTitleButton(
              text: 'Use voucher later'.tr,
              onPressed: () {
                CartController.to.setVoucher(null);
                Get.until(ModalRoute.withName(AppRoutes.cartView));
              },
            ),
          ),
        ),
      ),
    );
  }
}
