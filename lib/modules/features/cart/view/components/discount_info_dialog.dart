import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:java_code_onboarding/modules/models/discount.dart';
import 'package:java_code_onboarding/shared/widgets/primary_button_with_title.dart';
import 'package:java_code_onboarding/utils/extensions/string_ext.dart';

class DiscountInfoDialog extends StatelessWidget {
  final List<Discount> discounts;

  const DiscountInfoDialog({Key? key, required this.discounts})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        // Title
        Text(
          'Info discount'.tr,
          style: Get.textTheme.headlineMedium!.copyWith(
            color: Get.theme.colorScheme.primary,
          ),
        ),
        30.verticalSpace,

        // discount info list
        ...discounts.map<Widget>(
          (discount) => Padding(
            padding: EdgeInsets.fromLTRB(15.r, 0.r, 15.r, 7.r),
            child: Row(
              children: [
                // discount name
                Expanded(
                  child: Text(
                    discount.nama.toTitleCase,
                    style: Get.textTheme.bodyLarge,
                  ),
                ),

                // discount amount
                Text('${discount.nominal}%', style: Get.textTheme.titleSmall),
              ],
            ),
          ),
        ),
        30.verticalSpacingRadius,

        // confirm button
        SizedBox(
          width: 168.r,
          child: PrimaryButtonWithTitle(
              title: 'Okay'.tr, onPressed: () => Get.back()),
        ),
      ],
    );
  }
}
