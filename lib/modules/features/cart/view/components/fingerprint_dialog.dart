import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:java_code_onboarding/config/themes/colors.dart';
import 'package:java_code_onboarding/constant/common/constants.dart';
import 'package:java_code_onboarding/constant/core/asset_const.dart';
import 'package:java_code_onboarding/shared/widgets/horizontal_divider.dart';

class FingerprintDialog extends StatelessWidget {
  const FingerprintDialog({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 15.w),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          // title
          Text(
            'Verify order'.tr,
            style: Get.textTheme.headlineMedium,
          ),

          // subtitle
          Text(
            'Press your fingerprint'.tr,
            style:
                Get.textTheme.bodySmall!.copyWith(color: AppColor.kDarkColor7),
          ),
          30.verticalSpacingRadius,

          // fingerprint icon
          GestureDetector(
            child: SvgPicture.asset(AssetConstants.iconFingerprint),
            onTap: () => Get.back<String>(result: AppConstants.fingerprint),
          ),
          30.verticalSpacingRadius,
          const HorizontalOrDivider(),

          // verify using pin code
          TextButton(
            onPressed: () => Get.back<String>(result: AppConstants.pin),
            child: Text(
              'Verify using PIN code'.tr,
              style: Get.textTheme.titleSmall!
                  .copyWith(color: AppColor.kBlueColor),
            ),
          ),
        ],
      ),
    );
  }
}
