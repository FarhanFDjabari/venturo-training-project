import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:java_code_onboarding/config/themes/colors.dart';
import 'package:java_code_onboarding/constant/core/asset_const.dart';
import 'package:java_code_onboarding/modules/features/cart/controllers/cart_controller.dart';
import 'package:java_code_onboarding/shared/widgets/primary_button_with_title.dart';
import 'package:java_code_onboarding/utils/enums/cart_view_state.dart';

class CartOrderBottomBar extends StatelessWidget {
  const CartOrderBottomBar({
    super.key,
    this.onOrderButtonPressed,
    required this.totalPrice,
  });

  final VoidCallback? onOrderButtonPressed;
  final String totalPrice;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 75.h,
      padding: EdgeInsets.symmetric(
        vertical: 10.h,
        horizontal: 22.w,
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(30.r),
        ),
        color: AppColor.kLightColor,
        boxShadow: const [
          BoxShadow(
            color: AppColor.kDarkColor5,
            blurRadius: 20,
            spreadRadius: -1,
            offset: Offset(0, 1),
          ),
        ],
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          SvgPicture.asset(
            AssetConstants.iconCart,
            width: 35.w,
            height: 35.h,
          ),
          9.horizontalSpace,
          Expanded(
            flex: 5,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Total payment'.tr,
                  style: Get.textTheme.labelMedium,
                ),
                Text(
                  totalPrice,
                  style: Get.textTheme.titleMedium!
                      .copyWith(color: AppColor.kBlueColor),
                ),
              ],
            ),
          ),
          Expanded(
            flex: 4,
            child: Obx(() => PrimaryButtonWithTitle(
                  title: 'Order now'.tr,
                  onPressed: onOrderButtonPressed,
                  isLoading: CartController.to.cartViewState.value ==
                      CartViewState.Loading,
                )),
          ),
        ],
      ),
    );
  }
}
