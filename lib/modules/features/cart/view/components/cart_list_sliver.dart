import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:java_code_onboarding/config/routes/routes.dart';
import 'package:java_code_onboarding/modules/features/cart/controllers/cart_controller.dart';
import 'package:java_code_onboarding/modules/features/home/view/components/menu_card.dart';
import 'package:java_code_onboarding/modules/models/cart_item.dart';

class CartListSliver extends StatelessWidget {
  const CartListSliver({
    super.key,
    required this.carts,
  });

  final List<CartItem> carts;

  @override
  Widget build(BuildContext context) {
    return SliverFixedExtentList(
      delegate: SliverChildBuilderDelegate(
        (context, index) {
          return Padding(
            padding: EdgeInsets.symmetric(vertical: 8.5.h),
            child: MenuCard(
              menu: carts[index].menu,
              onTap: () {
                FocusManager.instance.primaryFocus?.unfocus();
                Get.toNamed(
                  '${AppRoutes.menuView}/${carts[index].menu.idMenu}',
                );
              },
              onIncrement: () => CartController.to.increaseQty(carts[index]),
              onDecrement: () => CartController.to.decreaseQty(carts[index]),
              price: carts[index].price,
              quantity: carts[index].quantity,
              note: carts[index].note,
              onNoteChanged: (value) => CartController.to.noteUpdate(
                carts[index],
                value,
              ),
            ),
          );
        },
        childCount: carts.length,
      ),
      itemExtent: 112.h,
    );
  }
}
