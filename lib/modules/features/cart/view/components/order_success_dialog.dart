import 'package:flutter/material.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:java_code_onboarding/config/themes/colors.dart';
import 'package:java_code_onboarding/constant/core/asset_const.dart';
import 'package:java_code_onboarding/shared/widgets/primary_button_with_title.dart';

class OrderSuccessDialog extends StatelessWidget {
  const OrderSuccessDialog({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 15.r),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          28.verticalSpace,
          SvgPicture.asset(AssetConstants.iconOrderSuccess),
          28.verticalSpace,
          Text(
            'Order is being prepared'.tr,
            style: Get.textTheme.headlineMedium?.copyWith(
              fontWeight: FontWeight.w400,
            ),
            textAlign: TextAlign.center,
          ),
          14.verticalSpace,
          Text.rich(
            TextSpan(children: [
              TextSpan(
                text: 'You can track your order in'.tr,
                style: Get.textTheme.bodySmall!.copyWith(
                  color: AppColor.kDarkColor6,
                ),
              ),
              TextSpan(
                text: ' ${'Order history'.tr}',
                style: Get.textTheme.bodySmall!.copyWith(
                  fontWeight: FontWeight.w800,
                  color: AppColor.kDarkColor6,
                ),
              ),
            ]),
            textAlign: TextAlign.center,
          ),
          14.verticalSpace,
          SizedBox(
            width: 168.w,
            child: PrimaryButtonWithTitle(
              onPressed: () => Get.back(),
              title: 'Okay'.tr,
            ),
          ),
        ],
      ),
    );
  }
}
