import 'package:flutter/material.dart';
import 'package:flutter_conditional_rendering/flutter_conditional_rendering.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:java_code_onboarding/config/themes/colors.dart';
import 'package:pinput/pinput.dart';

class PinDialog extends StatefulWidget {
  final String pin;

  const PinDialog({
    Key? key,
    required this.pin,
  }) : super(key: key);

  @override
  State<PinDialog> createState() => _PinDialogState();
}

class _PinDialogState extends State<PinDialog> {
  final RxBool obscure = RxBool(true);
  final RxnString errorText = RxnString();
  final TextEditingController controller = TextEditingController();
  int tries = 0;

  Future<void> processPin(String? pin) async {
    await Future.delayed(const Duration(milliseconds: 500));

    if (pin == widget.pin) {
      // if pin is correct close the dialog
      Get.back<bool>(result: true);
    } else {
      // if pin incorrect, type again
      tries++;

      if (tries >= 3) {
        // if tries more than 3, close the dialog
        Get.back<bool>(result: false);
      } else {
        // show how many tries user have left
        controller.clear();
        errorText.value = 'PIN wrong! n chances left.'.trParams({
          'n': (3 - tries).toString(),
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    final defaultPinTheme = PinTheme(
      width: 34.w,
      height: 50.h,
      textStyle: Get.textTheme.titleLarge,
      margin: EdgeInsets.symmetric(horizontal: 3.w),
      decoration: BoxDecoration(
        border: Border.all(color: AppColor.kBlueColor),
        borderRadius: BorderRadius.circular(10.r),
      ),
    );

    return Padding(
      padding: EdgeInsets.symmetric(vertical: 15.h, horizontal: 6.w),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          // title
          Text(
            'Verify order'.tr,
            style: Get.textTheme.headlineMedium,
          ),

          // subtitle
          Text(
            'Enter PIN code'.tr,
            style:
                Get.textTheme.bodySmall!.copyWith(color: AppColor.kDarkColor7),
          ),
          24.verticalSpacingRadius,
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Obx(
                () => Expanded(
                  // pin input
                  child: Pinput(
                    controller: controller,
                    showCursor: false,
                    length: 6,
                    autofocus: true,
                    separator: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 3.w),
                      child: ColoredBox(
                        color: AppColor.kDarkColor5,
                        child: SizedBox(width: 9.w, height: 2.h),
                      ),
                    ),
                    separatorPositions: const [2, 4],
                    closeKeyboardWhenCompleted: false,
                    defaultPinTheme: defaultPinTheme,
                    obscureText: obscure.value,
                    onSubmitted: processPin,
                    onCompleted: processPin,
                  ),
                ),
              ),
              10.horizontalSpace,
              // show pin button
              Obx(
                () => InkWell(
                  radius: 24.r,
                  child: Icon(
                    obscure.value ? Icons.visibility : Icons.visibility_off,
                    color: AppColor.kBlueColor,
                    size: 20.r,
                  ),
                  onTap: () {
                    obscure.value = !obscure.value;
                  },
                ),
              ),
            ],
          ),

          /// Pesan error
          Obx(
            () => Conditional.single(
              context: context,
              conditionBuilder: (context) => errorText.value != null,
              widgetBuilder: (context) => Padding(
                padding: EdgeInsets.only(left: 15.r, right: 15.r, top: 10.r),
                child: Text(
                  errorText.value!,
                  style: Get.textTheme.bodySmall!
                      .copyWith(color: AppColor.kRedColor),
                  textAlign: TextAlign.center,
                ),
              ),
              fallbackBuilder: (context) => const SizedBox(),
            ),
          ),
        ],
      ),
    );
  }
}
