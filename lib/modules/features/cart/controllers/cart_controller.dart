import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:java_code_onboarding/config/routes/routes.dart';
import 'package:java_code_onboarding/constant/common/constants.dart';
import 'package:java_code_onboarding/modules/features/cart/repositories/cart_order_repository.dart';
import 'package:java_code_onboarding/modules/features/cart/repositories/discount_repository.dart';
import 'package:java_code_onboarding/modules/features/cart/repositories/voucher_repository.dart';
import 'package:java_code_onboarding/shared/widgets/confirmation_dialog.dart';
import 'package:java_code_onboarding/modules/features/cart/view/components/discount_info_dialog.dart';
import 'package:java_code_onboarding/modules/features/cart/view/components/fingerprint_dialog.dart';
import 'package:java_code_onboarding/modules/features/cart/view/components/order_success_dialog.dart';
import 'package:java_code_onboarding/modules/features/cart/view/components/pin_dialog.dart';
import 'package:java_code_onboarding/modules/features/dashboard/controllers/dashboard_controller.dart';
import 'package:java_code_onboarding/modules/features/home/controllers/home_controller.dart';
import 'package:java_code_onboarding/modules/features/order/controllers/order_controller.dart';
import 'package:java_code_onboarding/modules/models/cart_item.dart';
import 'package:java_code_onboarding/modules/models/discount.dart';
import 'package:java_code_onboarding/modules/models/user.dart';
import 'package:java_code_onboarding/modules/models/voucher.dart';
import 'package:java_code_onboarding/shared/customs/error_snackbar.dart';
import 'package:java_code_onboarding/utils/enums/cart_discount_state.dart';
import 'package:java_code_onboarding/utils/enums/cart_view_state.dart';
import 'package:java_code_onboarding/utils/enums/cart_voucher_state.dart';
import 'package:java_code_onboarding/utils/services/local_db_services.dart';
import 'package:local_auth/local_auth.dart';

class CartController extends GetxController {
  static CartController get to => Get.find<CartController>();

  @override
  void onInit() {
    super.onInit();

    getUserDiscounts();
    getUserVouchers();
  }

  RxList<CartItem> cart = <CartItem>[].obs;
  RxList<Discount> discounts = <Discount>[].obs;
  RxList<Voucher> vouchers = <Voucher>[].obs;
  Rx<CartVoucherState> voucherState = CartVoucherState.Loading.obs;
  Rx<CartDiscountState> discountState = CartDiscountState.Loading.obs;
  Rx<CartViewState> cartViewState = CartViewState.Success.obs;

  Rxn<Voucher> selectedVoucher = Rxn<Voucher>();

  void addToCart(CartItem item) {
    if (cart.contains(item)) {
      removeFromCart(item);
    }
    cart.add(item);
    HomeController.to.menuStateReload();
  }

  void removeFromCart(CartItem item) {
    cart.remove(item);
  }

  void increaseQty(CartItem item) {
    item.quantity++;
    cart.refresh();
  }

  void decreaseQty(CartItem item) async {
    if (item.quantity > 1) {
      item.quantity--;
      cart.refresh();
    } else {
      showDeleteItemCartDialog(item);
    }
  }

  void showDeleteItemCartDialog(CartItem item) async {
    final result = await Get.defaultDialog(
      title: '',
      titleStyle: const TextStyle(fontSize: 0),
      content: ConfirmationDialog(
          title: 'Are you sure want to delete this menu from cart?'.tr),
    );

    if (result == true) {
      cart.remove(item);
    }
  }

  void noteUpdate(CartItem item, String note) {
    item.note = note;
    cart.refresh();
    HomeController.to.menuStateReload();
  }

  // Get food items
  List<CartItem> get foodItems => cart.where((e) => e.isFood).toList();

  /// Get drink items
  List<CartItem> get drinkItems => cart.where((e) => e.isDrink).toList();

  // get discounts
  Future<void> getUserDiscounts() async {
    discountState(CartDiscountState.Loading);
    final result = await DiscountRepository.getAllDiscount();

    if (result.statusCode >= 200 && result.statusCode < 300) {
      if (result.data == null) {
        discountState(CartDiscountState.Empty);
      } else {
        discounts(result.data);
        discountState(CartDiscountState.Success);
      }
    } else {
      discountState(CartDiscountState.Error);
    }
  }

  // get vouchers
  Future<void> getUserVouchers() async {
    voucherState(CartVoucherState.Loading);
    final result = await VoucherRepository.getAllVoucher();

    if (result.statusCode >= 200 && result.statusCode < 300) {
      if (result.data == null) {
        voucherState(CartVoucherState.Empty);
      } else {
        vouchers(result.data);
        voucherState(CartVoucherState.Success);
      }
    } else {
      voucherState(CartVoucherState.Error);
    }
  }

  void showDiscountDialog() {
    Get.defaultDialog(
      title: '',
      titleStyle: const TextStyle(fontSize: 0),
      content: DiscountInfoDialog(discounts: discounts),
    );
  }

  void showVoucherDialog() {
    if (vouchers.isEmpty) getUserVouchers();
    Get.toNamed(AppRoutes.voucherView);
  }

  void setVoucher(Voucher? voucher) {
    selectedVoucher.value = voucher;
    vouchers.refresh();
  }

  // calculate total price of all item
  int get totalPrice =>
      cart.fold(0, (prevTotal, item) => prevTotal + item.totalPrice);

  // calculate total discount
  int get totalDiscount =>
      discounts.fold(0, (prevTotal, discount) => prevTotal + discount.nominal);

  // calculate discount price
  int get discountPrice =>
      selectedVoucher.value == null ? totalPrice * totalDiscount ~/ 100 : 0;

  // get voucher amount
  int get voucherAmount => selectedVoucher.value!.nominal;

  // calculate final price
  int get grandTotalPrice => selectedVoucher.value != null
      ? totalPrice - voucherAmount
      : totalPrice - discountPrice;

  Future<void> addOrder() async {
    Get.until(ModalRoute.withName(AppRoutes.cartView));
    cartViewState(CartViewState.Loading);

    final userData = await LocalDBServices.getCurrentUser();

    final requestData = CartReq(
      user: userData as User,
      cart: cart,
      discounts: discounts.isEmpty ? null : discounts.toList(),
      voucher: selectedVoucher.value,
      discountPrice: totalPrice - grandTotalPrice,
      totalPrice: grandTotalPrice,
    );

    final result = await CartOrderRepository.addOrder(requestData);

    if (result != null &&
        result.statusCode == 200 &&
        result.data['status_code'] == 200) {
      cartViewState(CartViewState.Success);
      showOrderSuccessDialog();
    } else {
      cartViewState(CartViewState.Error);
      Get.showSnackbar(ErrorSnackBar(
        title: 'Error'.tr,
        message: result?.data['message'] ?? 'Server error'.tr,
      ));
    }
  }

  Future<void> verify() async {
    // check supported auth type in device
    final LocalAuthentication localAuth = LocalAuthentication();
    final bool canCheckBiometrics = await localAuth.canCheckBiometrics;
    final bool isBiometricSupported = await localAuth.isDeviceSupported();

    if (canCheckBiometrics && isBiometricSupported) {
      // open fingerprint dialog if supported
      final String? authType = await showFingerprintDialog();

      if (authType == AppConstants.fingerprint) {
        // fingerprint auth flow
        final bool authenticated = await localAuth.authenticate(
          localizedReason: 'Please authenticate to confirm order'.tr,
          options: const AuthenticationOptions(
            biometricOnly: true,
          ),
        );

        // if succeed, order cart
        if (authenticated) {
          await addOrder();
        }
      } else if (authType == AppConstants.pin) {
        // pin auth flow
        await showPinDialog();
      }
    } else {
      await showPinDialog();
    }
  }

  Future<String?> showFingerprintDialog() async {
    // ensure all modal is closed before show fingerprint dialog
    Get.until(ModalRoute.withName(AppRoutes.cartView));
    final result = await Get.defaultDialog(
      title: '',
      titleStyle: const TextStyle(fontSize: 0),
      content: const FingerprintDialog(),
    );

    return result;
  }

  Future<void> showPinDialog() async {
    // ensure all modal is closed before show pin dialog
    Get.until(ModalRoute.withName(AppRoutes.cartView));

    final userData = await LocalDBServices.getCurrentUser();

    final bool? authenticated = await Get.defaultDialog(
      title: '',
      titleStyle: const TextStyle(fontSize: 0),
      content: PinDialog(pin: userData?.pin ?? ''),
    );

    if (authenticated == true) {
      // if succeed, order cart
      await addOrder();
    } else if (authenticated != null) {
      // if failed 3 times, show order failed dialog
      Get.until(ModalRoute.withName(AppRoutes.cartView));
      Get.showSnackbar(ErrorSnackBar(
        title: 'Error'.tr,
        message: 'PIN already wrong 3 times. Please try again later.'.tr,
      ));
    }
  }

  Future<void> showOrderSuccessDialog() async {
    Get.until(ModalRoute.withName(AppRoutes.cartView));
    await Get.defaultDialog(
      title: '',
      titleStyle: const TextStyle(fontSize: 0),
      content: const OrderSuccessDialog(),
    );

    DashboardController.to.tabIndex.value = 1;
    OrderController.to.getOngoingOrders();

    Get.back();

    cart.clear();
    selectedVoucher.value = null;
  }
}
