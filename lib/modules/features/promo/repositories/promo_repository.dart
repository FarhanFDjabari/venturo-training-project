import 'package:dio/dio.dart';
import 'package:get/get.dart';
import 'package:java_code_onboarding/constant/core/api_const.dart';
import 'package:java_code_onboarding/modules/models/promo.dart';
import 'package:java_code_onboarding/utils/services/api_services.dart';
import 'package:java_code_onboarding/utils/services/local_db_services.dart';

class PromoRepository {
  PromoRepository._();

  // get promo by id
  static Future<PromoRes> getPromoById(int id) async {
    try {
      final userToken = await LocalDBServices.getUserToken();
      final dio = ApiServices.dioCall(token: userToken);
      final response = await dio.get('${ApiConstants.promoDetail}/$id');

      return PromoRes.fromJson(response.data);
    } on DioError {
      return PromoRes(statusCode: 500, message: 'Server error'.tr);
    }
  }
}
