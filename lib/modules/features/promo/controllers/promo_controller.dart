import 'package:get/get.dart';
import 'package:java_code_onboarding/modules/features/promo/repositories/promo_repository.dart';
import 'package:java_code_onboarding/modules/models/promo.dart';
import 'package:java_code_onboarding/utils/enums/promo_detail_state.dart';

class PromoController extends GetxController {
  static PromoController get to => Get.find<PromoController>();

  Rx<PromoDetailState> promoDetailState = PromoDetailState.Loading.obs;
  RxString errorMessage = ''.obs;
  Rxn<Promo> promoDetail = Rxn<Promo>();

  @override
  void onInit() {
    super.onInit();

    getPromoDetail(int.parse(Get.parameters['promoId'] as String));
  }

  Future<void> getPromoDetail(int promoId) async {
    promoDetailState(PromoDetailState.Loading);

    final result = await PromoRepository.getPromoById(promoId);

    if (result.statusCode >= 200 && result.statusCode < 300) {
      if (result.data != null) {
        promoDetailState(PromoDetailState.Success);
        promoDetail(result.data);
      } else {
        promoDetailState(PromoDetailState.Empty);
      }
    } else {
      promoDetailState(PromoDetailState.Error);
      errorMessage(result.message ?? 'Unknown error'.tr);
    }
  }
}
