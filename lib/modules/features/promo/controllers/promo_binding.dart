import 'package:get/get.dart';
import 'package:java_code_onboarding/modules/features/promo/controllers/promo_controller.dart';

class PromoBinding extends Bindings {
  @override
  void dependencies() {
    Get.put<PromoController>(PromoController());
  }
}
