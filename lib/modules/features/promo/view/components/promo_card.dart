import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:java_code_onboarding/config/themes/colors.dart';
import 'package:java_code_onboarding/constant/core/asset_const.dart';
import 'package:java_code_onboarding/modules/models/promo.dart';
import 'package:java_code_onboarding/utils/extensions/currency_ext.dart';
import 'package:java_code_onboarding/utils/extensions/string_ext.dart';

class PromoCard extends StatelessWidget {
  const PromoCard({
    super.key,
    this.enableShadow,
    required this.promo,
    this.width,
    this.onTap,
  });

  final bool? enableShadow;
  final Promo promo;
  final double? width;
  final ValueChanged<Promo>? onTap;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => onTap?.call(promo),
      borderRadius: BorderRadius.circular(15.r),
      child: Hero(
        tag: 'promo-${promo.idPromo}',
        child: Container(
          width: width ?? 282.w,
          height: 188.h,
          decoration: BoxDecoration(
            color: AppColor.kBlueColor2,
            borderRadius: BorderRadius.circular(15.r),
            image: DecorationImage(
                image: promo.foto != null && promo.foto?.isNotEmpty == true
                    ? CachedNetworkImageProvider(
                        promo.foto ?? '',
                      ) as ImageProvider<Object>
                    : const AssetImage(
                        AssetConstants.bgPromo,
                      ),
                fit: BoxFit.cover,
                colorFilter: const ColorFilter.mode(
                  AppColor.kBlueColor2,
                  BlendMode.srcATop,
                )),
            boxShadow: [
              if (enableShadow == true)
                const BoxShadow(
                  color: AppColor.kDarkColor6,
                  offset: Offset(0, 2),
                  blurRadius: 8,
                ),
            ],
          ),
          child: Padding(
            padding: EdgeInsets.all(16.w),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text.rich(
                  softWrap: true,
                  textAlign: TextAlign.center,
                  TextSpan(
                    text: promo.type == 'diskon' ? 'Diskon'.tr : 'Voucher'.tr,
                    style: Get.textTheme.titleMedium?.copyWith(
                      fontWeight: FontWeight.w800,
                      color: AppColor.kLightColor,
                    ),
                    children: [
                      TextSpan(
                        text: promo.type == 'diskon'
                            ? ' ${promo.diskon} %'
                            : '\n${promo.nominal?.toRupiah}',
                        style: Get.textTheme.displayLarge?.copyWith(
                          fontWeight: FontWeight.w800,
                          foreground: Paint()
                            ..style = PaintingStyle.stroke
                            ..strokeWidth = 0.1
                            ..color = AppColor.kLightColor,
                        ),
                      ),
                    ],
                  ),
                ),
                Text(
                  promo.nama.toTitleCase,
                  textAlign: TextAlign.center,
                  style: Get.textTheme.labelMedium?.copyWith(
                    color: AppColor.kLightColor,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
