import 'package:flutter/material.dart';
import 'package:flutter_conditional_rendering/conditional.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:java_code_onboarding/config/themes/colors.dart';
import 'package:java_code_onboarding/constant/core/asset_const.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:java_code_onboarding/modules/features/promo/controllers/promo_controller.dart';
import 'package:java_code_onboarding/modules/features/promo/view/components/promo_card.dart';
import 'package:java_code_onboarding/shared/widgets/rect_shimmer.dart';
import 'package:java_code_onboarding/shared/widgets/rounded_custom_app_bar.dart';
import 'package:java_code_onboarding/utils/enums/promo_detail_state.dart';

class PromoView extends StatelessWidget {
  const PromoView({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.kLightColor2,
      appBar: RoundedAppBar(
        title: 'Promo'.tr,
        iconAssetUrl: AssetConstants.iconPromo,
      ),
      body: Column(
        children: [
          Container(
            width: 1.sw,
            height: 231.h,
            padding: EdgeInsets.all(25.r),
            child: Center(
              child: Obx(() => Conditional.single(
                    context: context,
                    conditionBuilder: (context) =>
                        PromoController.to.promoDetailState.value ==
                        PromoDetailState.Success,
                    widgetBuilder: (context) => PromoCard(
                      width: 1.sw,
                      enableShadow: false,
                      promo: PromoController.to.promoDetail.value!,
                    ),
                    fallbackBuilder: (context) => RectShimmer(
                      height: 160.h,
                      radius: 15.r,
                    ),
                  )),
            ),
          ),
          Expanded(
            child: Container(
              width: 1.sw,
              padding: EdgeInsets.fromLTRB(22.w, 46.h, 22.w, 5.h),
              decoration: BoxDecoration(
                color: AppColor.kLightColor,
                borderRadius: BorderRadius.vertical(
                  top: Radius.circular(30.r),
                ),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Promo name'.tr,
                    style: Get.textTheme.titleSmall,
                  ),
                  Obx(() => Conditional.single(
                        context: context,
                        conditionBuilder: (context) =>
                            PromoController.to.promoDetailState.value ==
                            PromoDetailState.Success,
                        widgetBuilder: (context) {
                          return Text(
                            PromoController.to.promoDetail.value?.nama ?? '',
                            style: Get.textTheme.titleMedium?.copyWith(
                              color: AppColor.kBlueColor,
                              fontWeight: FontWeight.w700,
                            ),
                          );
                        },
                        fallbackBuilder: (context) => RectShimmer(height: 20.h),
                      )),
                  10.verticalSpace,
                  const Divider(
                    color: AppColor.kDarkColor5,
                    thickness: 1,
                  ),
                  10.verticalSpace,
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      const Icon(
                        Icons.list,
                        color: AppColor.kBlueColor,
                      ),
                      10.horizontalSpace,
                      Text(
                        'Terms and conditions'.tr,
                        style: Get.textTheme.titleSmall,
                      ),
                    ],
                  ),
                  10.horizontalSpace,
                  Obx(() => Conditional.single(
                        context: context,
                        conditionBuilder: (context) =>
                            PromoController.to.promoDetailState.value ==
                            PromoDetailState.Success,
                        widgetBuilder: (context) => Html(
                          data: PromoController
                                  .to.promoDetail.value?.syaratKetentuan ??
                              '',
                          style: {
                            '*': Style.fromTextStyle(
                              Get.textTheme.labelMedium!,
                            ),
                            'body': Style(
                              margin: Margins.zero,
                              padding: EdgeInsets.zero,
                            ),
                          },
                        ),
                        fallbackBuilder: (context) =>
                            RectShimmer(height: 100.h),
                      )),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
