import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:java_code_onboarding/config/themes/colors.dart';
import 'package:java_code_onboarding/constant/common/constants.dart';
import 'package:java_code_onboarding/constant/core/asset_const.dart';
import 'package:java_code_onboarding/modules/features/home/view/components/quantity_counter.dart';
import 'package:java_code_onboarding/modules/models/menu.dart';
import 'package:java_code_onboarding/utils/extensions/currency_ext.dart';

class MenuCard extends StatelessWidget {
  final Menu menu;
  final int? price;
  final int quantity;
  final String? note;
  final void Function()? onTap;
  final void Function()? onIncrement;
  final void Function()? onDecrement;
  final void Function(String)? onNoteChanged;

  const MenuCard({
    Key? key,
    required this.menu,
    this.price,
    this.quantity = 0,
    this.note,
    this.onTap,
    this.onIncrement,
    this.onDecrement,
    this.onNoteChanged,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      borderRadius: BorderRadius.circular(10.r),
      child: Ink(
        padding: EdgeInsets.all(7.r),
        decoration: BoxDecoration(
          color: AppColor.kLightColor,
          borderRadius: BorderRadius.circular(10.r),
          boxShadow: const [
            BoxShadow(
              color: AppColor.kDarkColor5,
              offset: Offset(0, 2),
              blurRadius: 8,
              spreadRadius: -1,
            ),
          ],
        ),
        child: Row(
          children: [
            // menu image
            Container(
              height: 90.h,
              width: 90.w,
              margin: EdgeInsets.only(right: 12.r),
              padding: EdgeInsets.all(5.r),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10.r),
                color: AppColor.kLightColor3,
              ),
              child: Hero(
                tag: 'menu-${menu.idMenu}',
                child: CachedNetworkImage(
                  imageUrl: menu.foto,
                  useOldImageOnUrlChange: true,
                  fit: BoxFit.contain,
                  errorWidget: (context, _, __) => CachedNetworkImage(
                    imageUrl: AppConstants.defaultMenuPhoto,
                    fit: BoxFit.contain,
                  ),
                ),
              ),
            ),

            // menu info
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    menu.nama,
                    style: Get.textTheme.titleMedium,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 1,
                  ),
                  Text(
                    (price ?? menu.harga).toRupiah,
                    style: Get.textTheme.bodyMedium!.copyWith(
                        color: AppColor.kBlueColor,
                        fontWeight: FontWeight.bold),
                  ),
                  5.verticalSpace,
                  if (quantity > 0)
                    Row(
                      children: [
                        SvgPicture.asset(AssetConstants.iconEdit, height: 12.r),
                        7.horizontalSpaceRadius,
                        Expanded(
                          child: TextFormField(
                            initialValue: note,
                            style: Get.textTheme.labelMedium,
                            decoration: InputDecoration.collapsed(
                              hintText: 'Add note'.tr,
                              border: InputBorder.none,
                            ),
                            onChanged: onNoteChanged,
                          ),
                        ),
                      ],
                    ),
                ],
              ),
            ),

            // qty counter
            Container(
              height: 75.r,
              padding: EdgeInsets.only(left: 12.r, right: 5.r),
              child: InkWell(
                onTap: () {},
                splashFactory: NoSplash.splashFactory,
                child: QuantityCounter(
                  quantity: quantity,
                  onIncrement: onIncrement,
                  onDecrement: onDecrement,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
