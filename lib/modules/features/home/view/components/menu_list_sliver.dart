import 'package:flutter/material.dart';
import 'package:flutter_conditional_rendering/conditional.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:java_code_onboarding/config/routes/routes.dart';
import 'package:java_code_onboarding/modules/features/home/controllers/home_controller.dart';
import 'package:java_code_onboarding/modules/features/home/view/components/menu_card.dart';
import 'package:java_code_onboarding/modules/models/menu.dart';
import 'package:java_code_onboarding/shared/widgets/empty_data_view.dart';

class MenuListSliver extends StatelessWidget {
  const MenuListSliver({
    super.key,
    required this.menus,
  });

  final List<Menu> menus;

  @override
  Widget build(BuildContext context) {
    return Conditional.single(
      context: context,
      conditionBuilder: (context) => menus.isNotEmpty,
      widgetBuilder: (context) => SliverFixedExtentList(
        delegate: SliverChildBuilderDelegate(
          (context, index) {
            return Padding(
              padding: EdgeInsets.symmetric(vertical: 8.5.h),
              child: Obx(() => MenuCard(
                    menu: menus[index],
                    onTap: () {
                      FocusManager.instance.primaryFocus?.unfocus();
                      Get.toNamed(
                          '${AppRoutes.menuView}/${menus[index].idMenu}');
                    },
                    onIncrement: () =>
                        HomeController.to.addQtyCartItem.call(menus[index]),
                    onDecrement: () => HomeController.to.decrementQtyCartItem
                        .call(menus[index]),
                    price: menus[index].harga,
                    quantity: HomeController.to
                            .getMenuFromCart(menus[index].idMenu)
                            ?.quantity ??
                        0,
                    note: HomeController.to
                            .getMenuFromCart(menus[index].idMenu)
                            ?.note ??
                        '',
                    onNoteChanged: (value) => HomeController
                        .to.addNoteToCartItem
                        .call(menus[index], value),
                  )),
            );
          },
          childCount: menus.length,
        ),
        itemExtent: 112.h,
      ),
      fallbackBuilder: (context) => SliverToBoxAdapter(
        child: EmptyDataView(width: 100.w),
      ),
    );
  }
}
