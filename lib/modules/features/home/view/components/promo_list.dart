import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:java_code_onboarding/config/routes/routes.dart';
import 'package:java_code_onboarding/modules/features/promo/view/components/promo_card.dart';
import 'package:java_code_onboarding/modules/models/promo.dart';

class PromoList extends StatelessWidget {
  const PromoList({
    super.key,
    required this.promos,
  });

  final List<Promo> promos;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 1.sw,
      height: 188.h,
      child: ListView.separated(
        scrollDirection: Axis.horizontal,
        shrinkWrap: true,
        padding: EdgeInsets.symmetric(horizontal: 25.w),
        itemBuilder: (context, index) {
          return PromoCard(
            enableShadow: false,
            promo: promos[index],
            onTap: (value) {
              FocusScope.of(context).unfocus();
              Get.toNamed('${AppRoutes.promoView}/${promos[index].idPromo}');
            },
          );
        },
        separatorBuilder: (context, index) => 26.horizontalSpace,
        itemCount: promos.length,
      ),
    );
  }
}
