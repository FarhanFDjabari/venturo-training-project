import 'package:flutter/material.dart';
import 'package:flutter_conditional_rendering/conditional.dart';
import 'package:flutter_conditional_rendering/conditional_switch.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:get/get.dart';
import 'package:java_code_onboarding/config/routes/routes.dart';
import 'package:java_code_onboarding/config/themes/colors.dart';
import 'package:java_code_onboarding/constant/core/asset_const.dart';
import 'package:java_code_onboarding/modules/features/cart/controllers/cart_controller.dart';
import 'package:java_code_onboarding/modules/features/home/controllers/home_controller.dart';

import 'package:java_code_onboarding/modules/features/home/view/components/menu_chip.dart';
import 'package:java_code_onboarding/modules/features/home/view/components/menu_list_shimmer.dart';
import 'package:java_code_onboarding/modules/features/home/view/components/menu_list_sliver.dart';
import 'package:java_code_onboarding/modules/features/home/view/components/promo_list.dart';
import 'package:java_code_onboarding/modules/features/home/view/components/search_app_bar.dart';
import 'package:java_code_onboarding/shared/widgets/section_header.dart';
import 'package:java_code_onboarding/shared/widgets/custom_error_view.dart';
import 'package:java_code_onboarding/shared/widgets/empty_data_view.dart';
import 'package:java_code_onboarding/shared/widgets/rect_shimmer.dart';
import 'package:java_code_onboarding/utils/enums/menu_category.dart';
import 'package:java_code_onboarding/utils/enums/menu_state.dart';
import 'package:java_code_onboarding/utils/enums/promo_state.dart';
import 'package:badges/badges.dart' as badge;

class HomeView extends StatelessWidget {
  final TextEditingController _searchController = TextEditingController();
  HomeView({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: SearchAppBar(
        searchController: _searchController,
        onChange: HomeController.to.setQueryMenu,
      ),
      body: CustomScrollView(
        physics: const ClampingScrollPhysics(),
        slivers: [
          SliverToBoxAdapter(child: 22.verticalSpace),
          SliverToBoxAdapter(
            child: SectionHeader(
              iconAssetUrl: AssetConstants.iconPromo,
              title: 'Available promo'.tr,
            ),
          ),
          SliverToBoxAdapter(child: 22.verticalSpace),
          Obx(
            () => SliverToBoxAdapter(
              child: ConditionalSwitch.single<PromoState>(
                context: context,
                valueBuilder: (context) => HomeController.to.promoState.value,
                caseBuilders: {
                  PromoState.Loading: (context) => SizedBox(
                        height: 188.h,
                        child: ListView.separated(
                          padding: EdgeInsets.symmetric(
                            horizontal: 25.w,
                            vertical: 15.h,
                          ),
                          scrollDirection: Axis.horizontal,
                          itemBuilder: (context, _) => RectShimmer(
                            width: 282.w,
                            height: 158.h,
                            radius: 15.r,
                          ),
                          itemCount: 5,
                          separatorBuilder: (context, _) =>
                              25.horizontalSpaceRadius,
                        ),
                      ),
                  PromoState.Error: (context) => CustomErrorView(
                        message: HomeController.to.promoErrorMessage.value,
                      ),
                  PromoState.Empty: (context) => Padding(
                        padding: EdgeInsets.only(bottom: 15.h),
                        child: EmptyDataView(width: 100.w),
                      ),
                },
                fallbackBuilder: (context) =>
                    PromoList(promos: HomeController.to.promoList),
              ),
            ),
          ),
          SliverToBoxAdapter(child: 22.verticalSpace),
          SliverToBoxAdapter(
            child: SizedBox(
              width: 1.sw,
              height: 45.h,
              child: ListView.separated(
                scrollDirection: Axis.horizontal,
                padding: EdgeInsets.symmetric(horizontal: 25.w),
                itemBuilder: (context, index) {
                  return Obx(
                    () => MenuChip(
                      text: HomeController.to.filters[index]['name'],
                      iconPath: HomeController.to.filters[index]['icon'],
                      isSelected: HomeController.to.menuCategory.value ==
                          HomeController.to.filters[index]['value'],
                      onTap: () {
                        HomeController.to.setMenuCategory(
                          HomeController.to.filters[index]['value'],
                        );
                      },
                    ),
                  );
                },
                separatorBuilder: (context, index) => 13.horizontalSpace,
                itemCount: HomeController.to.filters.length,
              ),
            ),
          ),
          SliverToBoxAdapter(child: 22.verticalSpace),
          Obx(
            () => SliverToBoxAdapter(
              child: Conditional.single(
                context: context,
                conditionBuilder: (context) =>
                    HomeController.to.menuCategory.value != MenuCategory.Drink,
                widgetBuilder: (context) => SectionHeader(
                  iconAssetUrl: AssetConstants.iconFood,
                  title: 'Food'.tr,
                  color: AppColor.kBlueColor,
                ),
                fallbackBuilder: (context) => const SizedBox(),
              ),
            ),
          ),
          Obx(
            () {
              if (HomeController.to.menuCategory.value != MenuCategory.Drink) {
                return ConditionalSwitch.single<MenuState>(
                  context: context,
                  valueBuilder: (context) => HomeController.to.menuState.value,
                  caseBuilders: {
                    MenuState.Loading: (context) =>
                        const SliverToBoxAdapter(child: MenuListShimmer()),
                    MenuState.Error: (context) => SliverToBoxAdapter(
                          child: CustomErrorView(
                            message: HomeController.to.menuErrorMessage.value,
                          ),
                        ),
                    MenuState.Empty: (context) => SliverPadding(
                          padding: EdgeInsets.only(bottom: 15.h),
                          sliver: SliverToBoxAdapter(
                              child: EmptyDataView(width: 100.w)),
                        ),
                  },
                  fallbackBuilder: (context) => SliverPadding(
                    padding:
                        EdgeInsets.symmetric(horizontal: 25.w, vertical: 8.5.h),
                    sliver: MenuListSliver(
                      menus: HomeController.to.foodList,
                    ),
                  ),
                );
              } else {
                return const SliverToBoxAdapter();
              }
            },
          ),
          Obx(
            () => SliverToBoxAdapter(
              child: SizedBox(
                height:
                    HomeController.to.menuCategory.value != MenuCategory.Drink
                        ? 13.5.h
                        : 0.h,
              ),
            ),
          ),
          Obx(
            () => SliverToBoxAdapter(
              child: Conditional.single(
                context: context,
                conditionBuilder: (context) =>
                    HomeController.to.menuCategory.value != MenuCategory.Food,
                widgetBuilder: (context) => SectionHeader(
                  iconAssetUrl: AssetConstants.iconDrink,
                  title: 'Drink'.tr,
                  color: AppColor.kBlueColor,
                ),
                fallbackBuilder: (context) => const SizedBox(),
              ),
            ),
          ),
          Obx(
            () {
              if (HomeController.to.menuCategory.value != MenuCategory.Food) {
                return ConditionalSwitch.single<MenuState>(
                  context: context,
                  valueBuilder: (context) => HomeController.to.menuState.value,
                  caseBuilders: {
                    MenuState.Loading: (context) =>
                        const SliverToBoxAdapter(child: MenuListShimmer()),
                    MenuState.Error: (context) => SliverToBoxAdapter(
                          child: CustomErrorView(
                            message: HomeController.to.menuErrorMessage.value,
                          ),
                        ),
                    MenuState.Empty: (context) => SliverToBoxAdapter(
                          child: EmptyDataView(width: 100.w),
                        ),
                  },
                  fallbackBuilder: (context) => SliverPadding(
                    padding:
                        EdgeInsets.symmetric(horizontal: 25.w, vertical: 8.5.h),
                    sliver: MenuListSliver(
                      menus: HomeController.to.drinkList,
                    ),
                  ),
                );
              } else {
                return const SliverToBoxAdapter();
              }
            },
          ),
        ],
      ),
      floatingActionButton: Obx(
        () => Conditional.single(
          context: context,
          conditionBuilder: (context) => CartController.to.cart.isNotEmpty,
          widgetBuilder: (context) => FloatingActionButton(
            onPressed: () => Get.toNamed(AppRoutes.cartView),
            backgroundColor: AppColor.kBlueColor,
            child: badge.Badge(
              showBadge: CartController.to.cart.isNotEmpty,
              badgeStyle: const badge.BadgeStyle(
                badgeColor: AppColor.kLightColor,
                borderSide: BorderSide(
                  color: AppColor.kBlueColor,
                ),
              ),
              badgeContent: Text(
                CartController.to.cart.length.toString(),
                style: Get.textTheme.labelMedium!.copyWith(
                  color: AppColor.kBlueColor,
                  fontWeight: FontWeight.w700,
                ),
              ),
              child: Icon(
                Icons.shopping_cart,
                size: 30.r,
                color: AppColor.kLightColor,
              ),
            ),
          ),
          fallbackBuilder: (context) => const SizedBox(),
        ),
      ),
    );
  }
}
