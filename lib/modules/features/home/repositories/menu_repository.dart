import 'package:dio/dio.dart';
import 'package:get/get.dart';
import 'package:java_code_onboarding/constant/core/api_const.dart';
import 'package:java_code_onboarding/modules/models/menu.dart';
import 'package:java_code_onboarding/utils/services/api_services.dart';
import 'package:java_code_onboarding/utils/services/local_db_services.dart';

class MenuRepository {
  MenuRepository._();

  // get all menu
  static Future<ListMenuRes> getAllMenu() async {
    try {
      final userToken = await LocalDBServices.getUserToken();
      final dio = ApiServices.dioCall(token: userToken);
      final response = await dio.get(ApiConstants.menu);

      return ListMenuRes.fromJson(response.data);
    } on DioError {
      return ListMenuRes(statusCode: 500, message: 'Server error'.tr);
    }
  }

  static Future<ListMenuRes> getMenuByCategory(String category) async {
    try {
      final userToken = await LocalDBServices.getUserToken();
      final dio = ApiServices.dioCall(token: userToken);
      final response =
          await dio.get('${ApiConstants.menuByCategory}/$category');

      return ListMenuRes.fromJson(response.data);
    } on DioError {
      return ListMenuRes(statusCode: 500, message: 'Server error'.tr);
    }
  }
}
