import 'package:dio/dio.dart';
import 'package:get/get.dart';
import 'package:java_code_onboarding/constant/core/api_const.dart';
import 'package:java_code_onboarding/modules/models/promo.dart';
import 'package:java_code_onboarding/utils/services/api_services.dart';
import 'package:java_code_onboarding/utils/services/local_db_services.dart';

class PromoListRepository {
  PromoListRepository._();

  // get all promo for user
  static Future<ListPromoRes> getPromoList() async {
    try {
      final userToken = await LocalDBServices.getUserToken();
      final dio = ApiServices.dioCall(token: userToken);
      final result = await dio.get(ApiConstants.promo);

      return ListPromoRes.fromJson(result.data);
    } on DioError {
      return ListPromoRes(statusCode: 500, message: 'Server error'.tr);
    }
  }
}
