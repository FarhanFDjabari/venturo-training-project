import 'package:get/get.dart';
import 'package:get/get_rx/src/rx_workers/utils/debouncer.dart';
import 'package:java_code_onboarding/constant/core/asset_const.dart';
import 'package:java_code_onboarding/modules/features/cart/controllers/cart_controller.dart';
import 'package:java_code_onboarding/modules/features/home/repositories/menu_repository.dart';
import 'package:java_code_onboarding/modules/features/home/repositories/promo_list_repository.dart';
import 'package:java_code_onboarding/modules/models/cart_item.dart';
import 'package:java_code_onboarding/modules/models/menu.dart';
import 'package:java_code_onboarding/modules/models/promo.dart';
import 'package:java_code_onboarding/utils/enums/menu_category.dart';
import 'package:java_code_onboarding/utils/enums/menu_state.dart';
import 'package:java_code_onboarding/utils/enums/promo_state.dart';

class HomeController extends GetxController {
  static HomeController get to => Get.find<HomeController>();

  late Debouncer debouncer;
  // promo
  Rx<PromoState> promoState = PromoState.Loading.obs;
  RxString promoErrorMessage = ''.obs;
  RxList<Promo> promoList = <Promo>[].obs;

  // menu
  Rx<MenuCategory> menuCategory = MenuCategory.All.obs;
  Rx<MenuState> menuState = MenuState.Loading.obs;
  RxList<Menu> listMenu = <Menu>[].obs;
  RxString menuErrorMessage = ''.obs;
  RxString queryMenu = ''.obs;

  List<Map<String, dynamic>> get filters => [
        {
          'name': 'All menu'.tr,
          'value': MenuCategory.All,
          'icon': AssetConstants.iconList
        },
        {
          'name': 'Food'.tr,
          'value': MenuCategory.Food,
          'icon': AssetConstants.iconFood
        },
        {
          'name': 'Drink'.tr,
          'value': MenuCategory.Drink,
          'icon': AssetConstants.iconDrink
        },
      ];

  @override
  void onInit() {
    super.onInit();
    debouncer = Debouncer(delay: const Duration(milliseconds: 500));

    reload();
  }

  @override
  void onClose() {
    debouncer.cancel();
    super.onClose();
  }

  Future<void> reload() async {
    await Future.any([
      getMenuList(),
      getPromoList(),
    ]);
  }

  void menuStateReload() async {
    menuState(MenuState.Loading);
    await Future.delayed(const Duration(milliseconds: 50));
    menuState(MenuState.Success);
  }

  Future<void> getPromoList() async {
    promoState(PromoState.Loading);
    final promoListData = await PromoListRepository.getPromoList();

    if (promoListData.statusCode >= 200 && promoListData.statusCode < 300) {
      // if success then show data in view
      if (promoListData.data?.isNotEmpty == true) {
        promoState(PromoState.Success);
        promoList(promoListData.data);
      } else {
        promoState(PromoState.Empty);
      }
    } else {
      promoState(PromoState.Error);
      promoErrorMessage(promoListData.message ?? 'Unknown error'.tr);
    }
  }

  Future<void> setMenuCategory(MenuCategory category) async {
    menuCategory(category);
  }

  Future<void> setQueryMenu(String query) async {
    debouncer.call(() {
      queryMenu(query);
    });
  }

  Future<void> getMenuList() async {
    menuState(MenuState.Loading);
    final result = await MenuRepository.getAllMenu();

    if (result.statusCode >= 200 && result.statusCode < 300) {
      // if success then show data in view
      if (result.data?.isNotEmpty == true) {
        menuState(MenuState.Success);

        listMenu(result.data);
      } else {
        menuState(MenuState.Empty);
      }
    } else {
      menuState(MenuState.Error);
      menuErrorMessage(result.message ?? 'Unknown error'.tr);
    }
  }

  List<Menu> get foodList => listMenu
      .where((element) =>
          element.isFood &&
          element.nama.toLowerCase().contains(queryMenu.value.toLowerCase()))
      .toList();

  List<Menu> get drinkList => listMenu
      .where((element) =>
          element.isDrink &&
          element.nama.toLowerCase().contains(queryMenu.value.toLowerCase()))
      .toList();

  // get menu data from cart
  CartItem? getMenuFromCart(int idMenu) {
    return CartController.to.cart
        .firstWhereOrNull((element) => element.menu.idMenu == idMenu);
  }

  void addNoteToCartItem(Menu menu, String note) {
    if (note.isEmpty) return;
    final cartItem = getMenuFromCart(menu.idMenu);

    if (cartItem != null) {
      CartController.to.cart
          .elementAt(CartController.to.cart.indexOf(cartItem))
          .note = note;
      CartController.to.cart.refresh();
    }
  }

  void addQtyCartItem(Menu menu) {
    final cartItem = getMenuFromCart(menu.idMenu);

    if (cartItem != null) {
      CartController.to.increaseQty(cartItem);
    } else {
      CartController.to.cart.add(CartItem(
        menu: menu,
        quantity: 1,
        level: null,
        note: '',
        toppings: const [],
      ));
    }
  }

  void decrementQtyCartItem(Menu menu) {
    final cartItem = getMenuFromCart(menu.idMenu);

    if (cartItem != null) {
      if (cartItem.quantity == 1) {
        CartController.to.cart.remove(cartItem);
        return;
      }
      CartController.to.decreaseQty(cartItem);
    }
  }
}
