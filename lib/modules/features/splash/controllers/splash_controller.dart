import 'package:get/get.dart';
import 'package:java_code_onboarding/config/localization/localization.dart';
import 'package:java_code_onboarding/config/routes/routes.dart';
import 'package:java_code_onboarding/modules/global_controllers/global_controller.dart';
import 'package:java_code_onboarding/utils/enums/connection_status.dart';
import 'package:java_code_onboarding/utils/services/local_db_services.dart';

class SplashController extends GetxController {
  static SplashController get to => Get.find<SplashController>();

  @override
  void onReady() async {
    // connection state check
    await checkConnectionState();

    // check language
    await languageCheck();

    // check user session
    await checkUserSession();
  }
}

Future<void> languageCheck() async {
  final language = await LocalDBServices.getLanguage();
  if (language != null) {
    Localization.changeLocale(language);
  }
}

Future<void> checkConnectionState() async {
  if (GlobalController.to.connectionStatus.value != ConnectionStatus.Online) {
    await GlobalController.to.showConnectionErrorAlert();
  }
}

Future<void> checkUserSession() async {
  final user = await LocalDBServices.getCurrentUser();
  final token = await LocalDBServices.getUserToken();

  // if there is session data
  if (user != null && token != null) {
    // to home page
    await Get.offAllNamed(AppRoutes.dashboardView);
  } else {
    // to login page
    await Get.offAllNamed(AppRoutes.loginView);
  }
}
