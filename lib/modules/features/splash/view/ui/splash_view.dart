import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:java_code_onboarding/constant/core/asset_const.dart';

class SplashView extends StatelessWidget {
  const SplashView({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Hero(
          tag: AssetConstants.iconJavaCode,
          child: Image.asset(
            AssetConstants.iconJavaCode,
            width: 0.8.sw,
          ),
        ),
      ),
    );
  }
}
